﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class giftBoxManager : MonoBehaviour
{
    public static giftBoxManager instance;
    public Animator boxAnim;
    public Animator itemAnim;
    public ParticleSystem confetti;
    public ParticleSystem backGlow;
    public GameObject item;
    public Transform itemHolder;
    GameObject itemLive;


    public SpriteRenderer spriteRenderer;

    bool start;
    public GameObject rootItem;
    // Start is called before the first frame update
    void Awake()
    {
        instance = this;
        giftBoxManager.ActivateToolGiftBox(false);
    }

    public static void ActivateToolGiftBox(bool enabled)
    {
        instance.rootItem.SetActive(enabled);
        //instance.showHand = enabled;
    }

    public GameObject handObject;
    public Animator handAnim;
    float lastDisableTime = 0;
    const float activeTime = 2.5f;
    const float downTime = 3;
    bool handIsEnabled = false;
    //public bool showHand = false;
    private void OnEnable()
    {
        lastDisableTime = Time.time;
        handIsEnabled = false;
        HandDisable();
    }
    void HandEnable()
    {
        handIsEnabled = true;
        handObject.SetActive(true);
        UniBliss.Centralizer.Add_DelayedMonoAct(this, ()=>handAnim.enabled =true, 0.5f);
    }
    void HandDisable()
    {
        lastDisableTime = Time.time;
        handIsEnabled = false;
        handObject.SetActive(false); handAnim.enabled = false;
    }
    void FixedUpdate()
    {
        if (Input.GetMouseButton(0)) HandDisable();

        //if (!showHand && handIsEnabled) HandDisable();
        if (!handIsEnabled && Time.time > lastDisableTime + downTime)
        {
            HandEnable();
        }
        if (handIsEnabled && Time.time > lastDisableTime + downTime + activeTime)
        {
            HandDisable();
        }


#if UNITY_EDITOR
        if (Input.GetKeyDown(KeyCode.Space) && start)
        {
            OpenBox(item);
        }

        if (Input.GetKeyDown(KeyCode.R))
        {
            ResetGiftBox();
        }

        if (Input.GetKeyDown(KeyCode.S))
        {
            StartOpenBox();
        }
#endif

    }

    public void StartOpenBox()
    {
        if (start) return;
        start = true;
        boxAnim.SetTrigger("Start");
    }

    public void SetSprite(Sprite sprite)
    {
        spriteRenderer.sprite = sprite;
    }
    public void OpenBox(GameObject item)
    {
        if (!start) return;
        if (item != null) this.item = item;

        //itemLive = Instantiate(item, itemHolder.position , itemHolder.rotation);
        //itemLive.transform.SetParent(itemHolder);
        //itemLive.transform.localScale = new Vector3(1,1,1);
        boxAnim.SetTrigger("OpenBox");
        itemAnim.SetTrigger("Reveal");
        confetti.Play();
        backGlow.Play();
    }

    public void ResetGiftBox()
    {
        if (!start) return;
        boxAnim.SetTrigger("Reset");
        itemAnim.SetTrigger("Reset");

        //Destroy(itemLive);
        backGlow.Stop();
        start = false;

    }
}
