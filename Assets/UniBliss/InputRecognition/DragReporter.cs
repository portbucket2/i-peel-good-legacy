﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragManager : MonoBehaviour
{
    const float defWidth = 900;
    public float defPixCount = 15;
    public event System.Action<Vector2> OnDrag;
    float minimalPixelCount;

    public static DragManager instance;
    Drag currentDrag = null;
    private void Start()
    {
        instance = this;
        minimalPixelCount =  (defPixCount/defWidth)* Screen.width;
    }
    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            currentDrag = new Drag(new Vector2(Input.mousePosition.x, Input.mousePosition.y), minimalPixelCount);
        }
        else if (Input.GetMouseButtonUp(0))
        {
            currentDrag = null;
        }

        if (currentDrag != null)
        {
            OnDrag?.Invoke(currentDrag.GetDragAmount(new Vector2(Input.mousePosition.x, Input.mousePosition.y)));
        }
    }
}

public class Drag
{
    Vector2 startPos;
    Vector2 lastPos;
    public Vector2 travelVec { get { return lastPos - startPos; } }
    float startTime;
    float minPixel = 100;
    public Drag(Vector2 startPosition, float minPix)
    {
        this.minPixel = minPix;
        startTime = Time.time;
        startPos = startPosition;
        lastPos = startPosition;
    }

    public Vector2 GetDragAmount(Vector2 currentPosition)
    {

        float dist = (currentPosition - startPos).magnitude;

        if (dist >= minPixel)
        {
            return travelVec;
        }
        else return Vector2.zero;
    }
}
