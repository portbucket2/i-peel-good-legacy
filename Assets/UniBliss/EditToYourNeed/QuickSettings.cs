﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuickSettings : ScriptableObject
{
    public bool showEjectLog = false;
    public bool drawDebugRays = false;
}
