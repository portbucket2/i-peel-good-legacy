﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CSVEditTemp : MonoBehaviour
{


    public TextAsset asset;


    CSVRow[] rows;

    public List<CSVLoadedData> loadedDatas;
    private void Start()
    {
        rows  = CSVReader.ReadCSVAsset(asset, ',');

        loadedDatas = new List<CSVLoadedData>();
        foreach (CSVRow item in rows)
        {
            string type = item.fields[1];

            string levelNo = item.fields[0];
            long lvlNo = long.Parse( levelNo.Split('(', ')')[1]);
            long userCount = long.Parse(item.fields[4]);



            CSVLoadedData loaded = Get(type);
            if (loaded == null)
            {
                loaded = new CSVLoadedData();
                loaded.type = type;
                loadedDatas.Add(loaded);
            }

            loaded.Add(userCount,lvlNo);

        }
    }
    CSVLoadedData Get(string type)
    {
        for (int i = 0; i < loadedDatas.Count; i++)
        {
            if (loadedDatas[i].type == type) return loadedDatas[i];
        }
        return null;
    }


}
[System.Serializable]
public class CSVLoadedData
{
    public string type;
    public long total;
    public long totalCount;
    public float avg;

    public void Add(long count, long value)
    {
        total += (count*value);
        totalCount += count;
        avg = total / totalCount;
    }
}
