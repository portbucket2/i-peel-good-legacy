﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
namespace UniBliss
{

    public class EditorUtilities
    {
        [UnityEditor.MenuItem("Clean/All Player Prefs")]
        public static void ClearPP()
        {
            PlayerPrefs.DeleteAll();
        }

        [UnityEditor.MenuItem("Clean/BMAD PData/Goals")]
        public static void ClearGoalProgress()

        {
            PlayerPrefs.DeleteKey("GOALSET_INDEX");
            PlayerPrefs.DeleteKey("GOAL_STATE_0");
            PlayerPrefs.DeleteKey("GOAL_STATE_1");
            PlayerPrefs.DeleteKey("GOAL_STATE_2");
        }

        [UnityEditor.MenuItem("Clean/Daily Reward Timer")]
        public static void ClearDailyRewardTimer()

        {
            PlayerPrefs.DeleteKey("LAST_CLAIM_DATE_DAILYREWARD");
        }

#if UNITY_EDITOR

        [UnityEditor.MenuItem("Clean/ReImport Level FBX")]
        public static void FBXImport()

        {
            Debug.Log(Selection.activeContext);
            string[] basePaths = { "Assets/GameAsset/AllLevelFruits" };
            string[] guids = AssetDatabase.FindAssets("t:Model", basePaths);
            for (int i = 0; i < guids.Length; i++)
            {
                //if (i == 0) continue;

                string path = AssetDatabase.GUIDToAssetPath(guids[i]);
                GameObject mi = AssetDatabase.LoadAssetAtPath<GameObject>(path);
                Debug.LogFormat("Will Reimport: {0}", mi.name);
                AssetDatabase.ImportAsset(path, ImportAssetOptions.ForceUpdate);
            }

        }

        [UnityEditor.MenuItem("Clean/Rebuild Prefabs")]
        public static void PrefabBuild()
        {
            for (int i = 1; i <= 151; i++)
            {
                RebuildPeelablePrefab(i);
            }

            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
        }

        public static void RebuildPeelablePrefab(int id)
        {

            string path = string.Format("Assets/Resources/Area 1/Level {0}.prefab", id);
            GameObject mi = AssetDatabase.LoadAssetAtPath<GameObject>(path);
            try
            {
                PeelableController pc = mi.transform.GetComponentInChildren<PeelableController>();
                pc.EditorInit(false);
                EditorUtility.SetDirty(pc);
                Debug.LogFormat("Rebuilding: {0}", pc);
            }
            catch (System.Exception e)
            {
                Debug.LogError(path);
            }



        }

#endif
    }
}