﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace UniBliss
{

    public class QuickButton
    {
        [UnityEditor.MenuItem("Assets/UniBliss/DoIt")]
        public static void Create()
        {
            

            foreach (GameObject go in Selection.objects)
            {
               go.transform.GetComponentInChildren<PeelableController>().EditorInit();
            }
        }
    }
}