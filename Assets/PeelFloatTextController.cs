﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class PeelFloatTextController : MonoBehaviour
{

    public Text text;
    public float lifeTime = 2;

    public void Init()
    {
        UniBliss.Centralizer.Add_DelayedMonoAct(this, () =>
        {
           if(this.gameObject.activeSelf) UniBliss.Pool.Destroy(this.gameObject);
        }, lifeTime);
    }

    public void DestroyNow()
    {
        UniBliss.Pool.Destroy(this.gameObject);
    }
}
