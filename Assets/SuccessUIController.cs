﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UniBliss;
public class SuccessUIController : MonoBehaviour
{
    #region editor fields
    [Header("Transition Buttons")]
    public Button levels2Button;
    public Button replayButton;
    public Button nextButton;
    public Button stepButton;
    public Button skipBoxButton;
    public Button openBoxButton;
    public Button claimBoxButton;
    //public Text openButtonText;
    public Text giftBoxTitleText;
    public Button getRewardButton;


    [Header("Completion Data")]
    public List<Animator> stars;
    public Text levelNumericNameText;
    public Text levelNameTextSuccess;
    public Text levelNameTextSuccess_2step;
    public Text triviaText;
    public Text peelLengthText;
    public Text peelLengthText2;
    public Animator peelTextAnimator;


    [Header("Flying Stars")]
    public GameObject prfeabStar;
    public RectTransform targetPoint;
    public Text flyingStarTargetText;


    [Header("State Toggles")]
    public Animator nextButtonAnim;
    public Animator nextButtonAnim_2step;
    public GameObject[] step1_Objects;
    public GameObject[] step2_Objects;
    public GameObject newToolDisplay;


    [Header("Tool Progress")]
    public Image initialProgressBar;
    public Image achievedProgressBar;
    public Image rewardedProgressBar;
    #endregion

    LevelPrefabManager levelObject;

    public Image toolToUnlockImage;

    //public void Awake()
    //{
    //    rewardADButtonOriginalParent = getRewardButton.transform.parent;
    //}
    public void ForceDisable()
    {
        this.gameObject.SetActive(false);
        SetHandAnimState(false);
    }

    int savedStarCounts;
    int textStarCount;
    public void SetStars(int starCount)
    {
        textStarCount = StarGazer.StarsAvailable;
        flyingStarTargetText.text = textStarCount.ToString();
        savedStarCounts = starCount;
        StarGazer.AddStarsForLevelCompletion(starCount);
    }
    public void DisplayStars()
    {
        for (int i = 0; i < stars.Count; i++)
        {

            stars[i].SetTrigger("reset");

        }

        UniBliss.Centralizer.Add_DelayedMonoAct(this, () =>
        {
            for (int i = 0; i < stars.Count; i++)
            {

                stars[i].SetTrigger("reset");
                int index = i;
                if (savedStarCounts > index) Centralizer.Add_DelayedMonoAct(this, () =>
                {
                    if (this!=null && this.gameObject !=null && this.gameObject.activeInHierarchy)
                    {

                        stars[index].SetTrigger("go");
                        StartCoroutine(Flier(stars[index].transform as RectTransform, 0.5f));
                    }
                }, 0.5f * index);

            }
            //if (savedStarCounts > 0) UniBliss.Centralizer.Add_DelayedMonoAct(this, () => 
            //{
            //    stars[0].SetTrigger("go"); 
            //}, 0.0f);
            //if (savedStarCounts > 1) UniBliss.Centralizer.Add_DelayedMonoAct(this, () => 
            //{ 
            //    stars[1].SetTrigger("go"); 
            //}, 0.5f);
            //if (savedStarCounts > 2) UniBliss.Centralizer.Add_DelayedMonoAct(this, () => 
            //{ 
            //    stars[2].SetTrigger("go");
            //    StartCoroutine(Flier(stars[2].transform, 0.5f));
            //}, 1.0f);
        }, 0);

        
    }

    IEnumerator Flier(RectTransform startPoint, float tSpan)
    {
        yield return new WaitForSeconds(0.5f);
        GameObject go = Pool.Instantiate(prfeabStar);
        go.transform.parent = this.transform;
        go.transform.position = startPoint.position;

        PooledItem pitem = go.GetComponent<PooledItem>();
        flierList.Add(pitem);
        //Debug.LogError("A");
        RectTransform trect = go.transform as RectTransform;

        float startTime = Time.time;

        Vector3 initialPos = trect.position;

        while (Time.time < startTime + tSpan)
        {
            float prog = Mathf.Clamp01((Time.time - startTime) / tSpan);

            trect.position = Vector3.Lerp(initialPos, targetPoint.position, prog);

            yield return null;
        }
        textStarCount++;
        flyingStarTargetText.text = textStarCount.ToString();
        flierList.Remove(pitem);
        //Debug.LogError("B");
        Pool.Destroy(go);
    }
    List<PooledItem> flierList = new List<PooledItem>();
    void ResetFlierProcesses()
    {
        StopAllCoroutines();
        for (int i = flierList.Count-1; i >= 0; i--)
        {

            //Debug.LogError(flierList[i].alive);
            if (flierList[i].alive)
            {
                Pool.Destroy(flierList[i].gameObject);
            }
            flierList.RemoveAt(i);
        }
    }

    public void OnShow(LevelPrefabManager levelObject)
    {
        ResetFlierProcesses();
        SetHandAnimState(false);
        this.gameObject.SetActive(true);

        this.levelObject = levelObject;


        triviaText.text = string.Format("<color=#253B71><size=60>Did you know?</size></color>\n\n{0}", levelObject.levelDefinition.detail);
        peelLengthText.text = string.Format("{0:0.0} inches", (MeshPeeler.instance.finalHighestPeelLength));
        peelLengthText2.text = string.Format("{0:0.0} inches", (MeshPeeler.instance.finalHighestPeelLength));
        levelNameTextSuccess.text = levelObject.levelDefinition.title;
        levelNameTextSuccess_2step.text = levelObject.levelDefinition.title;
        //levelNumericNameText.text = "LEVEL " + levelObject.cumulitiveLevelNo.ToString();

        switch (levelObject.loadType)
        {
            default:
            case LoadType.NORMAL:
                levelNumericNameText.text = string.Format("LEVEL {0}", levelObject.cumulitiveLevelNo);
                break;
            case LoadType.DAILY:
                levelNumericNameText.text = string.Format("DAILY LEVEL {0}", levelObject.levelI + 1);
                break;
        }


        nextButton.onClick.RemoveAllListeners();
        nextButton.onClick.AddListener(OnNext);
        replayButton.onClick.RemoveAllListeners();
        replayButton.onClick.AddListener(levelObject.OnReset);
        levels2Button.onClick.RemoveAllListeners();
        levels2Button.onClick.AddListener(levelObject.OnLevels);
        stepButton.onClick.RemoveAllListeners();
        stepButton.onClick.AddListener(OnStep);



        SetStateList(step1_Objects, true);
        SetStateList(step2_Objects, false);
    }
    private void OnNext()
    {
        if (forcePromptToGetRewardComplete)
        {
            levelObject.OnNext();
        }
    }
    public void OnStep()
    {
        SetStateList(step2_Objects, true);
        SetStateList(step1_Objects, false);
        nextButtonAnim_2step.SetTrigger("go");
        DisplayStars();
        ToolProfile targetTool = ToolProfileManager.GetTargetToolProfile();
        if (targetTool)
        {
            toolToUnlockImage.sprite = targetTool.proPic2;
            float initialProgress = ToolProfileManager.GetProgressToTarget();
            float achievedProgress = ToolProfileManager.AddProgressToTarget(MeshPeeler.instance.finalHighestPeelLength);

            //Debug.LogErrorFormat("{0} - {1}/{2}....{3}=>{4}", targetTool, targetTool.peelCost, MeshPeeler.instance.highestPeelLength, initialProgress, achievedProgress);
            //Debug.Log(initialProgress+" "+achievedProgress);
            initialProgressBar.fillAmount = initialProgress;
            achievedProgressBar.fillAmount = initialProgress;
            rewardedProgressBar.fillAmount = initialProgress;
            float secondaryP = achievedProgress;
            StopAllCoroutines();
            numsteps = 0;
            peelLengthText.text = peeltext(numsteps);
            StartCoroutine(AchievedProgressAnimate(achievedProgressBar, secondaryP < 1, 1, initialProgress, secondaryP, targetTool));
        }
        else
        {
            toolToUnlockImage.gameObject.SetActive(false);
            initialProgressBar.fillAmount = 0;
            achievedProgressBar.fillAmount = 1;
            rewardedProgressBar.fillAmount = 0;
            StopAllCoroutines();
            peelLengthText.text =  (Mathf.RoundToInt(MeshPeeler.instance.finalHighestPeelLength*10)/10.0f).ToString();
        }





    }
    static bool forcePromptToGetRewardComplete
    {
        get
        {
            if (_forcePromptForRewardButtonCompleteHD == null) _forcePromptForRewardButtonCompleteHD = new UniBliss.HardData<bool>("FORCE_REWARD_COMPLETE", false);
            return _forcePromptForRewardButtonCompleteHD.value;
        }
        set
        {
            if (_forcePromptForRewardButtonCompleteHD == null) _forcePromptForRewardButtonCompleteHD = new UniBliss.HardData<bool>("FORCE_REWARD_COMPLETE", false);
            _forcePromptForRewardButtonCompleteHD.value = value;
        }
    }
    static UniBliss.HardData<bool> _forcePromptForRewardButtonCompleteHD;

    int numsteps;
    string peeltext ( int i )
    {
        return ( (int) ( i / 10 ) ).ToString () + "." + ( (int) ( i % 10 ) ).ToString () + " inches";
    }

    IEnumerator AchievedProgressAnimate(Image bar,bool rewardButtonActivate, float animateTime, float initialValue, float finalValue, ToolProfile profile)
    {
        //peelLengthText.text = "0.0" + " inches";
        yield return new WaitForSeconds ( 0.5f );
        int numberofsteps = (int)(10 * MeshPeeler.instance.finalHighestPeelLength);

        float startTime = Time.time;
        float animationProgress = 0;
        peelTextAnimator.SetTrigger("go");
        while (Time.time<startTime+animateTime)
        {
            animationProgress = Mathf.Clamp01((Time.time-startTime)/ animateTime);
            bar.fillAmount = Mathf.Lerp(initialValue,finalValue,animationProgress);
            
            int intermediatenumberofsteps = (int) ( numberofsteps * (1- Mathf.Cos ( Mathf.PI * animationProgress )  ) / 2 );
            peelLengthText.text = peeltext(numsteps + intermediatenumberofsteps);
            yield return null;
        }
        numsteps += numberofsteps;
        //peelLengthText.text = string.Format ( "{0:0.0} inches", ( MeshPeeler.instance.highestPeelLength ) );
        peelLengthText.text = peeltext ( numsteps );
        bar.fillAmount = finalValue;
        if (finalValue >= 1)
        {
            newToolDisplay.SetActive(true);
            giftBoxManager.ActivateToolGiftBox(true);
            giftBoxManager.instance.SetSprite(profile.proPic);
            giftBoxManager.instance.StartOpenBox();

            openBoxButton.gameObject.SetActive(true);
            skipBoxButton.gameObject.SetActive(false);
            claimBoxButton.transform.parent.gameObject.SetActive(false);
            //openButtonText.text = "Tap to Open!";
            giftBoxTitleText.text = "";


            openBoxButton.onClick.RemoveAllListeners();
            openBoxButton.onClick.AddListener(() =>
            {

                giftBoxTitleText.text = profile.title;
                giftBoxManager.instance.OpenBox(null);

                openBoxButton.gameObject.SetActive(false);
                skipBoxButton.gameObject.SetActive(true);
                claimBoxButton.transform.parent.gameObject.SetActive(true);


                skipBoxButton.onClick.RemoveAllListeners();
                skipBoxButton.onClick.AddListener(() =>
                {
                    //Debug.LogError("skipped");
                    giftBoxManager.instance.ResetGiftBox();
                    newToolDisplay.SetActive(false);
                    giftBoxManager.ActivateToolGiftBox(false);
                });
                claimBoxButton.onClick.RemoveAllListeners();
                claimBoxButton.onClick.AddListener(() =>
                {

                    //Debug.LogError("claimed");
                    if (Application.isEditor)
                    {
                        giftBoxManager.instance.ResetGiftBox();
                        newToolDisplay.SetActive(false);
                        giftBoxManager.ActivateToolGiftBox(false);

                        profile.ReduceRemainingCost(profile.directCostRemainingToUnlock);
                        ToolSelectionManager.mode = profile.type;
                    }
                    else
                    {
                        claimBoxButton.interactable = false;
                        skipBoxButton.interactable = false;
                        Portbliss.Ad.AdController.ShowRewardedVideoAd((bool succes) =>
                        {
                            claimBoxButton.interactable = true;
                            skipBoxButton.interactable = true;
                            if (succes)
                            {
                                giftBoxManager.instance.ResetGiftBox();
                                newToolDisplay.SetActive(false);
                                giftBoxManager.ActivateToolGiftBox(false);

                                profile.ReduceRemainingCost(profile.directCostRemainingToUnlock);
                                ToolSelectionManager.mode = profile.type;

                            }
                        });
                    }



                });
            });
        }
        else if(rewardButtonActivate) ActivateRewardAdButton();
        if (newToolDisplay.activeSelf)
        {
            forcePromptToGetRewardComplete = true;
            getRewardButton.interactable = false;
        }
        else if (!forcePromptToGetRewardComplete)
        {
            SetHandAnimState(true);
        }
    }

    public void ActivateRewardAdButton()
    {
        getRewardButton.interactable = true;
        getRewardButton.onClick.RemoveAllListeners();
        getRewardButton.onClick.AddListener(OnRewardAdClick);
    }
    public void OnRewardAdClick()
    {
        getRewardButton.interactable = false;
        if (rewardTutorialRunning)
        {

            forcePromptToGetRewardComplete = true;
            SetHandAnimState(false);
            OnRewardAdCallback(true);
        }
        else if (Application.isEditor)
        {
            OnRewardAdCallback(true);
        }
        else
        {
            AnalyticsAssistant.RewardADClicked(LevelPrefabManager.currentLevel.cumulitiveLevelNo, LevelPrefabManager.currentLevel.LevelTypeInfo);
            AnalyticsAssistant.LogRewardedVideoAdStart(LevelPrefabManager.currentLevel.cumulitiveLevelNo);
            Portbliss.Ad.AdController.ShowRewardedVideoAd(OnRewardAdCallback);
        }

    }

    public void OnRewardAdCallback(bool success)
    {
        LevelPrefabManager.currentLevel.rewardedVideoAdPlayed = success;
        if (success)
        {
            float initialProgress = ToolProfileManager.GetProgressToTarget();
            float totalCost = ToolProfileManager.GetTargetToolProfile().peelCost;
            float rewardAmount = 0;
            if (initialProgress >= 0.5f)
            {
                rewardAmount = totalCost;
            }
            else
            {
                rewardAmount = (0.85f - initialProgress) * totalCost;
            }

            

            ToolProfile targetProfile = ToolProfileManager.GetTargetToolProfile();
            float achievedProgress = ToolProfileManager.AddProgressToTarget(rewardAmount);
            float secondaryP =  achievedProgress;
            StopAllCoroutines();
            StartCoroutine(AchievedProgressAnimate(rewardedProgressBar,false, 1, initialProgress, secondaryP, targetProfile));
            AnalyticsAssistant.LogRewardedVideoAdComplete(LevelPrefabManager.currentLevel.cumulitiveLevelNo);
        }
        else
        {
            getRewardButton.interactable = true;
        }
    }

    private void SetStateList(GameObject[] obs, bool state)
    {
        foreach (GameObject item in obs)
        {
            item.SetActive(state);
        }
    }


    public GameObject uiHandCanvas;
    public GameObject uiHandCamObject;
    public Animator uiHandAnimator;
    IEnumerator PointToRewardAdCo()
    {
        while (true)
        {
            uiHandAnimator.SetTrigger("reset");
            yield return new WaitForSeconds(2);
        }
    }

    Coroutine handShowRoutine;
    bool rewardTutorialRunning=true;
    //Transform rewardADButtonOriginalParent;
    void SetHandAnimState(bool enable)
    {
        if (rewardTutorialRunning == enable) return;
        rewardTutorialRunning = enable;
        if (enable)
        {
            uiHandCanvas.SetActive(true);
            uiHandCamObject.SetActive(true);
            //getRewardButton.transform.SetParent(uiHandCanvas.transform);
            Canvas cv = getRewardButton.gameObject.AddComponent<Canvas>();
            cv.overrideSorting = true;
            cv.sortingOrder = 2;
            getRewardButton.gameObject.AddComponent<GraphicRaycaster>();
            handShowRoutine = StartCoroutine(PointToRewardAdCo());
        }
        else
        {
            //getRewardButton.transform.SetParent(rewardADButtonOriginalParent);
            Destroy(getRewardButton.gameObject.GetComponent<GraphicRaycaster>());
            Destroy(getRewardButton.gameObject.GetComponent<Canvas>());
            if (handShowRoutine!=null) StopCoroutine(handShowRoutine);
            uiHandCanvas.SetActive(false);
            uiHandCamObject.SetActive(false);
        }
    }
}
