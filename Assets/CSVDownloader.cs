﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System.IO;
using UnityEditor;
public static class CSVDownloader
{
#if UNITY_EDITOR

    private const string head = "https://docs.google.com/spreadsheets/d/";
    private const string tail = "/export?format=csv";


    [MenuItem("Download/I Peel Good/Sequence Data")]
    public static void DownloadCSV_Sequence()
    {

        string url = head + "1_ckG-m83C-MHbeNGG7svSFphwbXXxbM5l8f7K0e8ZmM" + tail;
        string save_path = "Assets/Resources/CSV/CSV_Sequence.csv";

        DownloadCSV(url, save_path);
    }


    [MenuItem("Download/I Peel Good/Level Detail Data")]
    public static void DownloadCSV_Details()
    {

        string url = head + "1-CD1yq5PKWnxUOhEERFfUfURovGObTyxT3kR1PuEj7A" + tail;
        string save_path = "Assets/Resources/CSV/CSV_LevelDetail.csv";

        DownloadCSV(url, save_path);
    }


    [MenuItem("Download/I Peel Good/Tool Data")]
    public static void DownloadCSV_Tools()
    {

        string url = head + "15unP2hdtChkapa9DmbeFiHgV9w4DaOK9GW0ePvETpg8" + tail;
        string save_path = "Assets/Resources/CSV/CSV_ToolData.csv";

        DownloadCSV(url,save_path);
    }


    public static void DownloadCSV(string url, string save_path)
    {
        UnityWebRequest webRequest = UnityWebRequest.Get(url);

        webRequest.SendWebRequest().completed += (AsyncOperation asop) =>
        {
            if (webRequest.isNetworkError)
            {
                Debug.Log("Download Error: " + webRequest.error);
            }
            else
            {
                Debug.Log("Download success");
                Debug.Log("Data: " + webRequest.downloadHandler.text);

                File.WriteAllText(save_path, webRequest.downloadHandler.text);
                AssetDatabase.Refresh();
            }
        };
    }
#endif

}