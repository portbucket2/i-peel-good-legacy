﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PeelLengthDataGenerator : MonoBehaviour
{

    public static PeelLengthDataGenerator instance;

    private void Awake()
    {
        instance = this;
        LevelPrefabManager.newPrefabLoading += OnNewPrefabLoad;
    }
    private void OnDestroy()
    {

        LevelPrefabManager.newPrefabLoading -= OnNewPrefabLoad;
    }
    void OnNewPrefabLoad(Theme theme)
    {
        targetText.text = "";
    }

    public GameObject plus1Prefab;
    public float generationRate = 3;

    public Transform positioningTrans;

    
    float touchTime = 0;
    // Update is called once per frame
    void Update()
    {
        if (!MeshPeeler.instance || !MeshPeeler.instance.IsGeneratingPeelNow()) return;
        
        touchTime += Time.deltaTime;

        if (touchTime >= 1 / generationRate)
        {
            touchTime = 0;
            GameObject go = UniBliss.Pool.Instantiate(plus1Prefab);
            go.transform.parent = this.transform;
            go.GetComponent<PeelFloatTextController>().Init();
            go.transform.position = positioningTrans.position;
        }
    }

    public void OnPeelCompleted(float previousLenth, float freshLength)
    {
        //Debug.LogErrorFormat("{0}_{1}", previousLenth,freshLength);
        if(freshLength>previousLenth)
        StartCoroutine(Flier(1,previousLenth,freshLength));
    }

    IEnumerator Flier(float tSpan, float previousLenth, float freshLength)
    {
        float dispValue = ((int)freshLength * 10) / 10.0f;
        if (dispValue != 0)
        {
            GameObject go = UniBliss.Pool.Instantiate(finalValueObject);
            go.transform.parent = this.transform;
            PeelFloatTextController fptc = go.GetComponent<PeelFloatTextController>();
            fptc.Init();
            fptc.text.text = string.Format("{0} In.", ((int)freshLength * 10) / 10.0f);
            go.transform.position = positioningTrans.position;

            RectTransform trect = go.transform as RectTransform;

            float startTime = Time.time;

            Vector3 initialPos = trect.position;

            while (Time.time < startTime + tSpan)
            {
                float prog = Mathf.Clamp01((Time.time - startTime) / tSpan);

                trect.position = Vector3.Lerp(initialPos, target.position, prog);

                yield return null;
            }

            if (freshLength > previousLenth)
            {
                targetText.text = string.Format("{0} In.", ((int)freshLength * 10) / 10.0f);
            }
            fptc.DestroyNow();
        }


    }

    public GameObject finalValueObject;
    public RectTransform target;
    public UnityEngine.UI.Text targetText;


}
