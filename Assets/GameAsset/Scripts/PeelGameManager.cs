﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class PeelGameManager : MonoBehaviour
{
    public static PeelGameManager instance;
    float topLimit = 612;
    float botLimit = 17;

    public static bool GameInProgress
    {
        get; private set;
    }

    public InLevelCanvasManager inLevelCanvasMan;
    public Image progressSlider;
    public Text timeText;
    public Text starText;
    //bool firstStarReached;
    //bool secondStarReached;
    //bool allStarReached;

    //public int starCount = 0;


    public RectTransform m1;
    public RectTransform m2;
    public RectTransform m3;
    public Animator star1;
    public Animator star2;
    public Animator star3;
    public GameObject uiDisableOnWinItem_early;
    public GameObject uiDisableOnWinItem;


    public GameObject collidePrefab;


    public List<GameObject> initialDisableList;

    // Start is called before the first frame update

    //float startTime;


    public GameObject shareToSocialObject;
    public UnityEngine.UI.Button shareToSocialButton;

    public void OnGifRecordEnd()
    {
        shareToSocialObject.SetActive(true);
    }
    private void Awake()
    {
        shareToSocialButton.onClick.AddListener(() =>
        {
            //shareToSocialObject.SetActive(false);
            MeshPeeler.instance.OnGIFShareButton();
        });
        MeshPeeler.onGIFRecordEnd += OnGifRecordEnd;

        instance = this;

        float f3 = topLimit;
        float f2 = 0.85f * (topLimit - botLimit) + botLimit;
        float f1 = 0.70f * (topLimit - botLimit) + botLimit;

        float ypos = m1.anchoredPosition.y;
        m1.anchoredPosition = new Vector2(f1, ypos);
        m2.anchoredPosition = new Vector2(f2, ypos);
        m3.anchoredPosition = new Vector2(f3, ypos);
        LevelPrefabManager.newPrefabLoading += OnNewPrefabLoad;
    }
    private void Start()
    {

        inLevelCanvasMan.onMiniAction += OnMinimize;
    }
    private void OnDestroy()
    {
        MeshPeeler.onGIFRecordEnd -= OnGifRecordEnd;
        LevelPrefabManager.newPrefabLoading -= OnNewPrefabLoad;
        inLevelCanvasMan.onMiniAction -= OnMinimize;
    }
    private void OnNewPrefabLoad(Theme theme)
    {
        forceFinish = false;

        shareToSocialObject.SetActive(false);
        uiDisableOnWinItem_early.SetActive(true);
        LevelPrefabManager.currentLevel.suspendedExternally = false;
        //PhaseManager.instance.forceUseDefaultCamTrans = false;
        progressSlider.fillAmount = 0;
        uiDisableOnWinItem.SetActive(true);
        timeText.text = "";
        starText.text = "3";
        //starCount = 0;
        progressComplete = false;
        RotationSpeedManager.instance.BreakRotation(0.75f, true);
        levelStartTime = -1;
        //Debug.Log("xxxxxxxxxxxxx: "+ LevelLoader.instance);

        int initialStars = LevelPrefabManager.currentLevel.GetStarData();
        //Debug.Log("xxxxxxxxxxxxx: " + initialStars);
        star1.ResetTrigger("go");
        star2.ResetTrigger("go");
        star3.ResetTrigger("go");
        star1.SetTrigger("reset");
        star2.SetTrigger("reset");
        star3.SetTrigger("reset");
        lastTimeStarUpdated = 3;

        UniBliss.Centralizer.Add_DelayedMonoAct(this, () =>
        {

            star1.SetTrigger("reset");
            star2.SetTrigger("reset");
            star3.SetTrigger("reset");
        }, 0, true);
        GameInProgress = true;
    }



    void OnMinimize()
    {
        if (!uiDisableOnWinItem) Debug.Log(this.GetInstanceID());
        uiDisableOnWinItem.SetActive(true);
        RotationSpeedManager.instance.BreakRotation(2.5f, true);
    }
    //bool timeRanOut = false;
    //bool completed= false;

    float levelStartTime = -1;
    float levelCompletionTime = -1;
    bool progressComplete = false;

    int timingStars
    {
        get
        {
            if (levelStartTime < 0) return 3;

            if (progressComplete)
            {
                return StarGazer.StarEarnableForTiming(LevelPrefabManager.currentLevel.levelDefinition, levelCompletionTime);
            }
            else
            {

                return StarGazer.StarEarnableForTiming(LevelPrefabManager.currentLevel.levelDefinition, Time.time - levelStartTime);
            }
        }
    }


    string timeString
    {
        get
        {
            switch (timingStars)
            {
                case 3:
                    return "Excellent!";
                case 2:
                    return "Awesome!";
                case 1:
                    return "Great!";
                default:
                    return "";
            }
        }
    }


    float timeSpent
    {
        get
        {
            if (progressComplete)
            {
                return Mathf.FloorToInt(levelCompletionTime);
            }
            else
            {

                return Mathf.FloorToInt(Time.time - levelStartTime);
            }
        }
    }

    public bool forceFinish;
    public void SetProgressToComplete() {

        forceFinish = true;
    }

    int lastTimeStarUpdated;
    void Update()
    {
        if (MainGameManager.state != GameState.GAME) return;


        //timeText.text = timeString;
        //if (timeLeft > 0)
        //{
        float progress = MeshPeeler.instance.currentProgress / PeelableController.instance.targetCount;
        if (forceFinish || Input.GetKeyDown(KeyCode.End)) {
            progress = 1;
        }
        forceFinish = false;
        progressSlider.fillAmount = progress;

        int timeStar = timingStars;
        if (progress >= .01f)
        {

            timeText.text = string.Format("{0} sec ", timeSpent);

            starText.text = string.Format("{0}", timeStar);
            if (levelStartTime<=0) levelStartTime = Time.time;

            if (!progressComplete)
            {
                if (timeStar != lastTimeStarUpdated)
                {
                    //Debug.LogError(timeStar);
                    switch (timeStar)
                    {
                        case 2:
                            star3.SetTrigger("go");
                            break;
                        case 1:
                            star2.SetTrigger("go");
                            break;
                        case 0:
                            star1.SetTrigger("go");
                            break;
                    }
                }

                lastTimeStarUpdated = timeStar;
            }

            

        }

        if (progress >= 1 && !progressComplete)
        {
            progressComplete = true;
            levelCompletionTime = Time.time-levelStartTime;
            levelStartTime = -1;
            //starCount++;
            MainGameManager.instance.runningLevelManager.AddStar();
            OnCompletionRequested();
            //star3.SetTrigger("go");
            //StarGazer.AddStarsForLevelCompletion(timeStar);
            AnalyticsAssistant.LevelCompleted(LevelPrefabManager.currentLevel.cumulitiveLevelNo, LevelPrefabManager.currentLevel.levelDefinition.title, LevelPrefabManager.currentLevel.LevelTypeInfo);
        }

        //if (progress >= 1 && starCount < 3)
        //{

        //    starCount++;
        //    MainGameManager.instance.runningLevelManager.AddStar ();
        //    OnCompletionRequested ();
        //    star3.SetTrigger ( "go" );

        //    AnalyticsAssistant.LevelCompleted ( LevelPrefabManager.currentLevel.cumulitiveLevelNo, LevelPrefabManager.currentLevel.levelDefinition.title, LevelPrefabManager.currentLevel.LevelTypeInfo );
        //}
        //else if (progress > .85f && starCount < 2)
        //{
        //    starCount++;
        //    MainGameManager.instance.runningLevelManager.AddStar();
        //    MainGameManager.instance.runningLevelManager.ReportEarlyCompletion(2);
        //    star2.SetTrigger("go");
        //}
        //else if (progress > .7f && starCount < 1)
        //{
        //    starCount++;
        //    MainGameManager.instance.runningLevelManager.AddStar();
        //    MainGameManager.instance.runningLevelManager.ReportEarlyCompletion(1);
        //    star1.SetTrigger("go");

        //    //uiDisableOnWinItem_early.SetActive ( false );
        //    //MainGameManager.instance.runningLevelManager.canvMan.LoadContinue ( inLevelCanvasMan.AllowSkip );
        //}

    }


    public void OnCompletionRequested ()
    {
        if ( !GameInProgress )
            return;
        GameInProgress = false;
        LevelPrefabManager.currentLevel.suspendedExternally = true;
        MainGameManager.instance.runningLevelManager.canvMan.LoadContinue ( false );
        MeshPeeler.instance.Eject ( "Game Over", true );

       AnalyticsAssistant.SpeedChanged ( RotationSpeedManager.SpeedMultiplierName () );


        UniBliss.Centralizer.Add_DelayedMonoAct ( this, () => {
            LevelPrefabManager.currentLevel.suspendedExternally = false;
            uiDisableOnWinItem.SetActive ( false );
            MainGameManager.instance.runningLevelManager.OnComplete ( true,timingStars );
            MainGameManager.instance.runningLevelManager.AnimateNextLevelButton ();
        }, 2.75f, true );
        RotationSpeedManager.instance.BreakRotation ( 2.5f, false );
        ConfettiManager.Play ();

        if ( CameraAndToolController.VibrationStatus.value )
        {
#if UNITY_IOS || UNITY_ANDROID
            Handheld.Vibrate ();
#endif

        }
    }

}