﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(PeelableController))]
public class PeelableControllerEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        PeelableController myScript = (PeelableController)target;
        if (GUILayout.Button("Build Object", GUILayout.Height(50)))
        {
            Undo.RecordObject(target,"PeelableController");
            myScript.EditorInit();
        }
    }

}
