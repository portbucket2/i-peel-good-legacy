﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PeelUnit
{
    public const float MAX_PEEL_LENGTH = 0.01f;
    public const float DEGREE_PER_LENGHT = 1000;


    public Vector3 startPoint;
    public Vector3 startNormal;

    public RaycastHit lastRCH;

    public Transform axis;


    public float Travel { get { return Vector3.Distance(startPoint, lastRCH.point); } }
    public Vector3 PosPoint { get { return (startPoint + lastRCH.point) / 2; } }
    public Vector3 LookAtPoint
    {
        get
        {
            return PosPoint + (startNormal + lastRCH.normal);
        }
    }

    public Transform mainTransform;
    public Transform peelVisTrans;

    public PeelUnit(RaycastHit rch, GameObject prefab, Color peelColor, PeelUnit lastPeel = null)
    {
        startPoint = rch.point;
        startNormal = rch.normal;
        mainTransform = new GameObject("PeelMain").transform;
        peelVisTrans = MonoBehaviour.Instantiate(prefab).transform;
        peelVisTrans.SetParent(mainTransform);
        peelVisTrans.localPosition = Vector3.zero;
        Update(rch, false, lastPeel);



        MeshRenderer meshr = peelVisTrans.GetComponent<MeshRenderer>();
        Material m = meshr.material;
        meshr.material = new Material(m);
        meshr.material.color = peelColor;
        //if (lastPeel != null) SetFollower(lastPeel);
    }

    public void Update(RaycastHit rch, bool finalUpdate = false, PeelUnit seedPeel = null)
    {
        lastRCH = rch;

        mainTransform.position = PosPoint;
        //peelVisTrans.localScale = new Vector3(Travel + (finalUpdate ? 0.03f : .075f), .18f, .001f);
        peelVisTrans.localScale = new Vector3(Travel, .18f, .001f);
        mainTransform.LookAt(LookAtPoint, seedPeel != null ? seedPeel.mainTransform.up : Vector3.Cross(rch.point - startPoint, rch.normal).normalized);
        if (axis) axis.localPosition = followerOffset + new Vector3(Travel / 2 * followerOffset.x / Mathf.Abs(followerOffset.x), 0, 0);
    }

    Vector3 followerOffset;
    public void SetFollower(PeelUnit followerPeel)
    {
        //follower = followerPeel.mainTransform;
        //followerPeel.mainTransform.SetParent(mainTransform);
        //followerOffset = followerPeel.mainTransform.localPosition;

        //follower = followerPeel.mainTransform;
        //follower.SetParent(mainTransform);
        //followerOffset = followerPeel.mainTransform.localPosition;

        axis = new GameObject("axis").transform;
        axis.SetParent(peelVisTrans);
        axis.localRotation = Quaternion.identity;
        axis.localPosition = new Vector3(.5f, 0, 0);

        axis.SetParent(mainTransform, true);
        axis.localScale = Vector3.one;
        followerPeel.mainTransform.SetParent(axis);
        //follower = axisTR;

        //bool positivity = false;// Vector3.Angle(mainTransform.up, Vector3.up) < 90;
        //Debug.Log(positivity);
        axis.localRotation = Quaternion.Euler(0, -MAX_PEEL_LENGTH * DEGREE_PER_LENGHT, 0);
        //axisTR.Rotate(axisTR.up, 5);// (positivity ? -1 : +1)* MAX_PEEL_LENGTH*DEGREE_PER_LENGHT);
        followerOffset = axis.localPosition;
    }

}
