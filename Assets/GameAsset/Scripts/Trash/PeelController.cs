﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PeelController : MonoBehaviour
{
    public Camera cam;
    // Start is called before the first frame update


    public Material mat;
    public Texture2D tex0;


    void Start()
    {
        Texture2D texNew = new Texture2D(tex0.width, tex0.height);

        Graphics.CopyTexture(tex0, texNew);

        mat.mainTexture = texNew;

    }
    public LayerMask hitLayer;

    public GameObject peelPrefab;

    PeelUnit currentPeel;
    PeelUnit oldPeel;
    PeelUnit olderPeel;



    void Update()
    {


        if (Input.GetMouseButton(0))
        {
            if (wasTouchedInLastFrame)
            {
                Vector3 mouseTravelVec = Input.mousePosition - lastMousePosition;//vec3 to vec2
                float mouseReTouchBudget = mouseTravelVec.magnitude;

                Vector3 lastretouchPoint = Input.mousePosition;
                retouchPoints.Clear();
                retouchPoints.Add(lastretouchPoint);
                while (mouseReTouchBudget > MIN_RETOUCH_DISTANCE)
                {
                    lastretouchPoint = lastretouchPoint - (mouseTravelVec.normalized) * MIN_RETOUCH_DISTANCE;
                    mouseReTouchBudget -= MIN_RETOUCH_DISTANCE;
                    retouchPoints.Add(lastretouchPoint);
                }

                for (int i = retouchPoints.Count - 1; i >= 0; i--)
                {
                    OnTouch(retouchPoints[i]);
                }
            }
            OnTouch(Input.mousePosition);

            wasTouchedInLastFrame = true;
            lastMousePosition = Input.mousePosition;
        }
        else
        {
            wasTouchedInLastFrame = false;
            Clean();
        }
    }
    const float MIN_RETOUCH_DISTANCE = 10;
    bool wasTouchedInLastFrame = false;
    Vector3 lastMousePosition;
    public List<Vector3> retouchPoints = new List<Vector3>(100);


    int deadMovementCounter;
    public void OnTouch(Vector3 touchPoint)
    {
        Ray ray = cam.ScreenPointToRay(touchPoint);

        RaycastHit rch;
        bool counterShouldBePreserved = false;

        if (Physics.Raycast(ray, out rch, 100, layerMask: hitLayer))
        {
            Color foundColor;
            float activity = PeelColorAtPoint(hit:rch, brushRadius:200,prevColorKeep: out foundColor);
            //bool noActivityPointFound = activity == 0;
            //Vector3 mouseTravelVec = Input.mousePosition - lastMousePosition;//vec3 to vec2
            //float mouseDist = mouseTravelVec.magnitude;

            if (activity > 0.1f)
            {
                if (currentPeel == null)
                {
                    currentPeel = new PeelUnit(rch, peelPrefab, foundColor);
                }
                else
                {
                    if (currentPeel.Travel > PeelUnit.MAX_PEEL_LENGTH)
                    {
                        currentPeel.Update(rch,true);

                        olderPeel = oldPeel;
                        oldPeel = currentPeel;
                        currentPeel = new PeelUnit(rch, peelPrefab, foundColor, currentPeel);
                        if (olderPeel != null)
                        {
                            oldPeel.SetFollower(olderPeel);
                        }
                    }
                    else
                    {
                        currentPeel.Update(rch);
                    }
                }

            }
            else if (currentPeel != null)
            {
                currentPeel.Update(rch);
                if (currentPeel.Travel > PeelUnit.MAX_PEEL_LENGTH)
                {   
                    //if (mouseDist > MIN_RETOUCH_DISTANCE && noActivityPointFound) Clean("clean speed");
                    //else 
                    if (activity == 0)
                    {
                        deadMovementCounter++;
                        Debug.LogFormat("{0} to {1}________{2}", lastMousePosition, Input.mousePosition, deadMovementCounter);
                        if (deadMovementCounter == 7)
                        {
                            Debug.LogFormat("cleaned!");
                            Clean();
                        }
                        else
                        {

                            counterShouldBePreserved = true;
                        }
                    } }
            }

        }
        else Clean();
        if (!counterShouldBePreserved)
        {

            deadMovementCounter = 0;
        }
    }
    public void Clean(string log=null)
    {
        if (log != null) Debug.Log(log);
        if (currentPeel != null)
        {
            if(oldPeel!=null)currentPeel.SetFollower(oldPeel);

            Rigidbody rgbd = currentPeel.mainTransform.gameObject.AddComponent<Rigidbody>();

            rgbd.AddForce(Vector3.forward * -1, ForceMode.Impulse);


        }

        currentPeel = null;
        oldPeel = null;
        olderPeel = null;
    }

    public Texture2D tex2;
    public float PeelColorAtPoint(RaycastHit hit,float brushRadius, out Color prevColorKeep)
    {
        Renderer rend = hit.transform.GetComponent<Renderer>();
        MeshCollider meshCollider = hit.collider as MeshCollider;
        //Debug.Log(meshCollider);
        //Debug.Log(rend);
        //Debug.Log(rend.sharedMaterial);
        //Debug.Log(rend.sharedMaterial.mainTexture);
        prevColorKeep = Color.clear;
        if (rend == null || rend.sharedMaterial == null || rend.sharedMaterial.mainTexture == null || meshCollider == null)
            return 0;

        Texture2D tex = rend.material.mainTexture as Texture2D;
        Vector2 pixelUV = hit.textureCoord;
        pixelUV.x *= tex.width;
        pixelUV.y *= tex.height;
        //Debug.Log(hit.textureCoord);

        int X = (int)pixelUV.x;
        int Y = (int)pixelUV.y;

        //col = tex.GetPixel(X,Y);

        float totalR = 0;
        float totalG = 0;
        float totalB = 0;
        float totalCount = 0;

        Color tempCol;
        float points=0;
        float validPoints=0;
        for (int i = (int)(X-brushRadius); i <= (int)(X+brushRadius); i++)
        {
            for (int j = (int)(Y - brushRadius); j <= (int)(Y + brushRadius); j++)
            {
                //tex.SetPixel(i, j, Color.clear);
                float dist = (i - X) * (i - X) + (j - Y) * (j - Y);
                if (dist <= brushRadius)
                {
                    tempCol = tex.GetPixel(i, j);
                    points++;
                    validPoints += tempCol.a;
                    if (tempCol.a > 0.1f)
                    {
                        totalR += tempCol.r;
                        totalG += tempCol.g;
                        totalB += tempCol.b;
                        totalCount++;

                        tex.SetPixel(i, j, Color.clear);
                    }
                }
            }
        }

        prevColorKeep = new Color(totalR/totalCount, totalG/totalCount, totalB/totalCount, 1);
        //tex.SetPixel((int)pixelUV.x, (int)pixelUV.y, Color.clear);
        tex.Apply();
        //Debug.LogFormat("{0}/{1}",validPoints,points);
        return validPoints/points;
    }
}

