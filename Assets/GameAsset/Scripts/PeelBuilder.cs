﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class PeelBuilder
{
    //public const float PEEL_WIDTH = 0.075f;
    public const float EXTRUDE_CONST = 0.002f;//.002f;
    public const float JUMP_LIMIT = 0.500f;

    public const int colliderInterval = 5;

    private float half_thickness = 0.0005f;
    private float rotationRatePerTravel = -720;
    private float totalRotationReduction = -630;
    public float travelLimit = 0.03f;
    public Transform root;
    public Rigidbody rgbd;
    public GameObject front;
    public GameObject back;
    public float frontThickness;
    public float backThickness;
    public float currentWindValue;
    public float currentWindEndTime;
    public float ejectYpos;
    public float ejectTime;

    public List<Transform> colliders=new List<Transform>();



    Mesh fiberFrontMesh;
    Mesh fiberBackMesh;

    public List<PeelFiber> fibers = new List<PeelFiber>();


    #region easy access functions
    public PeelFiber lastFiber_0
    {
        get
        {
            if (fibers.Count < 1) return null;
            else return fibers[fibers.Count - 1];
        }
    }
    public PeelFiber lastFiber_1
    {
        get
        {
            if (fibers.Count < 2) return null;
            else return fibers[fibers.Count - 2];
        }
    }
    public PeelFiber lastFiber_2
    {
        get
        {
            if (fibers.Count < 3) return null;
            else return fibers[fibers.Count - 3];
        }
    }
    public float GetTotalFiberLength()
    {
        float length = 0;
        for (int i = 0; i < fibers.Count; i++)
        {
            length += fibers[i].lastRecordedLength;
        }
        return length;
    }
    public Vector3 GetTravelVec()
    {
        return lastFiber_0.basePoint - lastFiber_1.basePoint;
    }
    public Vector3 GetTravelVec_1()
    {
        return lastFiber_1.basePoint - lastFiber_2.basePoint;
    }
    public Vector3 GetPotentialTravelVec(Vector3 pointInMeshSpace)
    {
        return pointInMeshSpace - lastFiber_1.basePoint;
    }
    public float GetLastInterFiberDistance()
    {
        return (lastFiber_0.basePoint - lastFiber_1.basePoint).magnitude;
    }
    public float GetPotentialStrechDistance(Vector3 worldPoint)
    {
        //root.InverseTransformPoint(lastFiber_0.point);
        return (worldPoint - root.TransformPoint(lastFiber_1.basePoint)).magnitude;
    }
    public bool WillExceedTravelLimit(Vector3 worldPoint)
    {
        //Debug.LogFormat("Travel: {0}, Count: {1}", GetCurrentTravelLength(), fibers.Count);
        return GetPotentialStrechDistance(worldPoint) > travelLimit;
    }
    public bool WillExceedJumpLimit(Vector3 worldPoint)
    {
        //Debug.LogFormat("Travel: {0}, Count: {1}", GetCurrentTravelLength(), fibers.Count);
        return GetPotentialStrechDistance(worldPoint) > JUMP_LIMIT;
    }
    #endregion

    public static int peelIDindex=0;
    public  int peelID=-1;
    public PeelBuilder(PeelableController peelable, RaycastHit hit, float rotationRatePerTravel, float totalRotationReduction, float travelLimit, int ARC_VxCount )
    {
        peelID = peelIDindex++;
        allRegisteredPeels.Add(this);
        this.ARC_N = ARC_VxCount;
        this.travelLimit = travelLimit;
        this.rotationRatePerTravel = rotationRatePerTravel;
        this.totalRotationReduction = totalRotationReduction;
        this.half_thickness = peelable.half_thickness;
        frontThickness = half_thickness*2* peelable.thicknessRatio;
        backThickness = half_thickness*2*(1- peelable.thicknessRatio);


        root = new GameObject("peel").transform;
        front = new GameObject("front");
        back = new GameObject("back");
        front.transform.SetParent(root);
        back.transform.SetParent(root);
        root.position = hit.point;
        root.rotation = Quaternion.identity;



        MeshFilter frontFilter = front.AddComponent<MeshFilter>();
        MeshRenderer frontRenderer = front.AddComponent<MeshRenderer>();
        frontRenderer.receiveShadows = false;
        frontRenderer.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.TwoSided;
        fiberFrontMesh = new Mesh();
        fiberFrontMesh.name = "fberFrontMesh";
        frontFilter.mesh = fiberFrontMesh;
        frontRenderer.material = peelable.skinMaterial0;

        MeshFilter backFilter = back.AddComponent<MeshFilter>();
        MeshRenderer backRenderer = back.AddComponent<MeshRenderer>();
        backRenderer.receiveShadows = false;
        backRenderer.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
        fiberBackMesh = new Mesh();
        fiberBackMesh.name = "fberBackMesh";
        backFilter.mesh = fiberBackMesh;
        backRenderer.material = peelable.skinMaterial1;
    }

    public static List<PeelBuilder> allRegisteredPeels = new List<PeelBuilder>();
    public static void OnNewLevelStarting()
    {
        for (int i = allRegisteredPeels.Count-1; i >=0; i--)
        {
            if(allRegisteredPeels[i]!=null) allRegisteredPeels[i].TrashItems();
            allRegisteredPeels.RemoveAt(i);
        }
    }

    public void AddFiber(RaycastHit hit, float halfPeelWidth, Vector3 rootOffset, Vector2 uvU, Vector2 uvD)
    {
        lastLength = 0;

        PeelFiber pf = new PeelFiber(hit, root, halfPeelWidth,rootOffset,half_thickness);

        if (fibers.Count >= 2)
        {
            //Vector3 axis = root.TransformDirection(fibers[fibers.Count - 1].upOffset);
            //Vector3 point = root.TransformPoint(fibers[fibers.Count - 1].basePoint);
            //float rotationAmount =  rotationRatePerTravel -  (1-(1/Mathf.Pow( GetTotalFiberLength(),1/16)))*totalRotationReduction;
            //if (GetTotalFiberLength() > 1)
            //{
            //    rotationAmount -= (1 - (1 / GetTotalFiberLength())) * totalRotationReduction;
            //}
            //root.RotateAround(point, axis, GetLastInterFiberDistance() * rotationAmount);
            //Debug.Log("shit");
            Vector3 travelDirection = fibers[fibers.Count - 1].basePoint - fibers[fibers.Count - 2].basePoint;
            //Debug.Log(travelDirection)
            pf.upOffset = Vector3.Cross(travelDirection.normalized, pf.normal).normalized * pf.halfPeelWidth;
        }

        fibers.Add(pf);
        if (fibers.Count%10 == 3)
        {
            Transform colTr = UniBliss.Pool.Instantiate(PeelGameManager.instance.collidePrefab,root).transform;
            colTr.position = root.TransformPoint(lastFiber_2.ShiftedPoint);
            Vector3 normal = root.TransformDirection(lastFiber_2.normal);
            colTr.LookAt(colTr.position + normal);
            colTr.localScale = new Vector3(travelLimit , lastFiber_2.halfPeelWidth, lastFiber_2.half_thickness)*2;
            colliders.Add(colTr);
        }
        UpdateLastFibers(hit,uvU,uvD);
    }

    float lastLength = 0;
    public void UpdateLastFibers(RaycastHit hit, Vector2 uvU, Vector2 uvD)
    {
        lastFiber_0.UpdateUV(uvU, uvD);
        if (fibers.Count < 2) return;


        lastFiber_0.basePoint = root.InverseTransformPoint(hit.point + hit.normal * EXTRUDE_CONST);
        Vector3 travelDirection = lastFiber_0.basePoint - lastFiber_1.basePoint;
        lastFiber_0.lastRecordedLength = travelDirection.magnitude;

        if (lastFiber_0.lastRecordedLength == 0) return;

        lastFiber_0.upOffset = Vector3.Cross(travelDirection.normalized, lastFiber_0.normal).normalized * lastFiber_0.halfPeelWidth;
        if (lastFiber_2 == null)
            lastFiber_1.upOffset = Vector3.Cross(travelDirection.normalized, lastFiber_1.normal).normalized * lastFiber_1.halfPeelWidth;
        else
        {
            Vector3 travelDirection2 = lastFiber_1.basePoint - lastFiber_2.basePoint;
            lastFiber_1.upOffset = Vector3.Cross((travelDirection.normalized + travelDirection2.normalized) / 2, lastFiber_1.normal).normalized * lastFiber_1.halfPeelWidth;

        }
        //Debug.Log(lastFiber_1.upOffset.magnitude);


        float currentLength = GetLastInterFiberDistance();
        float lengthIncrease = currentLength - lastLength;
        lastLength = GetLastInterFiberDistance();

        Vector3 axis = root.TransformDirection(fibers[fibers.Count - 1].upOffset);
        Vector3 point = root.TransformPoint(fibers[fibers.Count - 1].basePoint);
        float rotationAmount = rotationRatePerTravel - (1 - (1 / Mathf.Pow(GetTotalFiberLength(), 1 / 12))) * totalRotationReduction;


        if (GetTotalFiberLength() > 1)
        {
            rotationAmount -= (1 - (1 / GetTotalFiberLength())) * totalRotationReduction;
        }
        //root.RotateAround(point, axis, lengthIncrease * rotationAmount);
        float rotationInUse;
        float calculatedRot = lengthIncrease * rotationAmount / Time.deltaTime;

        if (lastNormalizedRotation == 0)
        {
            rotationInUse = calculatedRot;
        }
        else
        {
            rotationInUse = Mathf.Lerp(lastNormalizedRotation, calculatedRot, 0.15f);
        }
        lastNormalizedRotation = rotationInUse;
        root.RotateAround(point, axis, rotationInUse*Time.deltaTime);

        lastFiber_0.UpdateFiberExtendedElements();
        lastFiber_1.UpdateFiberExtendedElements();
    }
    float lastNormalizedRotation = 0;
    public float EjectPeel(bool disablePhysics, float ejectForce)
    {
        //HERE
        //transform.get_position
        if (root == null)
        {
            Debug.Log("<color='red'>root is null!</color>");
        }

        if (lastFiber_0 == null)
        {
            Debug.Log("<color='red'>lastFiber_0 is null!</color>");
        }

        ejectYpos = root.position.y;
        ejectTime = Time.time;
        if (!disablePhysics)
        {
            rgbd = root.gameObject.AddComponent<Rigidbody>();
            rgbd.AddForce( root.TransformDirection( lastFiber_0.normal)* ejectForce, ForceMode.Impulse);
            //GameObject go = root.gameObject;
            //UniBliss.Centralizer.Add_DelayedAct(() => { GameObject.DestroyImmediate(go); }, 1);
        }
        return GetTotalFiberLength();
        //if (fibers.Count > 2)
        //{
        //    MeshCollider mc = front.AddComponent<MeshCollider>();
        //    mc.convex = true;
        //}
        //Debug.Log(fibers.Count);
    }
    public void UnloadPoolColliders()
    {
        //Debug.Log(fibers.Count);
        //Debug.Log(colliders);
        for (int i = colliders.Count-1; i >=0; i--)
        {
            if(colliders[i]) UniBliss.Pool.Destroy(colliders[i].gameObject,true);
            //Debug.Log("x");
            colliders.RemoveAt(i);

        }

    }
    public void TrashItems()
    {

        //Debug.Log(peelID);
        UnloadPoolColliders();
        if(root && root.gameObject) GameObject.DestroyImmediate(root.gameObject);
        if(fiberFrontMesh) Mesh.Destroy(fiberFrontMesh);
        if (fiberBackMesh) Mesh.Destroy(fiberBackMesh);
    }

    #region draw
    int ARC_N = 24;

    static bool threadBusy;
    public void UpdateFiberMesh()
    {
        //UpdateFrontMesh();
        //UpdateBackMesh();
        if (threadBusy) return;
        threadBusy = true;
        PeelDrawThreader.PrepareForWork(fibers, ARC_N, half_thickness, frontThickness, backThickness);
        PeelDrawThreader.StartWork(MeshPeeler.instance, fiberFrontMesh, fiberBackMesh, () => { threadBusy = false; });
    }


    //public static Vector3[] vertices;
    //public static Vector3[] normals;
    //public static Vector2[] uv;
    //public static int[] triangles;
    public static void ArrayInit(int fiberN,int arcN)
    {
        //vertices = new Vector3[fiberN * 4 + arcN*4];
        //normals = new Vector3[fiberN *4 + arcN*4];
        //uv = new Vector2[fiberN * 4 + arcN * 4];
        //triangles = new int[3 * (fiberN - 1) * 6 + (arcN * 3 * 3 + 6) * 2];
    }
    private void UpdateFrontMesh()
    {
        int used_VXIndex = fibers.Count * 4;
        int used_TRIndex = 3 * (fibers.Count - 1) * 6;
        Vector3[] vertices = new Vector3[used_VXIndex + ARC_N * 4];
        Vector3[] normals = new Vector3[used_VXIndex + ARC_N * 4];
        Vector2[] uv = new Vector2[used_VXIndex + ARC_N * 4];
        int[] triangles = new int[used_TRIndex + (ARC_N *3*3+6)*2];


        for (int i = 0; i < fibers.Count; i++)
        {
            vertices[4 * i + 0] = fibers[i].DownPosFront;
            vertices[4 * i + 1] = fibers[i].UpPosFront;
            vertices[4 * i + 2] = fibers[i].DownPosFront - fibers[i].normal*frontThickness;
            vertices[4 * i + 3] = fibers[i].UpPosFront - fibers[i].normal *frontThickness;

            normals[4 * i + 0] = fibers[i].normal;
            normals[4 * i + 1] = fibers[i].normal;
            normals[4 * i + 2] = -fibers[i].upOffset;
            normals[4 * i + 3] = fibers[i].upOffset;

            uv[4 * i + 0] = fibers[i].uv_D;
            uv[4 * i + 1] = fibers[i].uv_U;
            uv[4 * i + 2] = fibers[i].uv_D;
            uv[4 * i + 3] = fibers[i].uv_U;


            if (i == fibers.Count - 1)
            {
                continue;
            }
            else
            {
                //face
                triangles[18 * i + 0] = 4 * i + 0;
                triangles[18 * i + 1] = 4 * i + 1;
                triangles[18 * i + 2] = 4 * i + 4;

                triangles[18 * i + 3] = 4 * i + 4;
                triangles[18 * i + 4] = 4 * i + 1;
                triangles[18 * i + 5] = 4 * i + 5;


                //downBorder
                triangles[18 * i + 6] = 4 * i + 6;
                triangles[18 * i + 7] = 4 * i + 2;
                triangles[18 * i + 8] = 4 * i + 0;

                triangles[18 * i + 9] = 4 * i + 6;
                triangles[18 * i + 10] = 4 * i + 0;
                triangles[18 * i + 11] = 4 * i + 4;

                //upBorder
                triangles[18 * i + 12] = 4 * i + 5;
                triangles[18 * i + 13] = 4 * i + 1;
                triangles[18 * i + 14] = 4 * i + 3;

                triangles[18 * i + 15] = 4 * i + 5;
                triangles[18 * i + 16] = 4 * i + 3;
                triangles[18 * i + 17] = 4 * i + 7;
            }
        }




        int index0, index1, index0x, index1x;




        //start-front
        index0x = 2;
        index0 = 0;
        index1 = 1;
        index1x = 3;

        //Debug.LogFormat("budget {0}, arc {1}",vertices.Length-used_VXIndex, ARC_N);
        for (int i = 0; i <= ARC_N; i++)
        {
            if (i < ARC_N)
            {
                Vector3 offset;
                offset = Quaternion.AngleAxis(-(180.0f * (i + 1)) / (ARC_N + 1), fibers[0].normal) * fibers[0].upOffset;
                Vector3 vert = fibers[0].ShiftedPoint + offset + fibers[0].normal * half_thickness;
                vertices[used_VXIndex + 2 * i + 0] = vert;
                vertices[used_VXIndex + 2 * i + 1] = vert - fibers[0].normal * frontThickness;
                normals[used_VXIndex + 2 * i + 0] = fibers[0].normal;
                normals[used_VXIndex + 2 * i + 1] = offset;
                uv[used_VXIndex + 2 * i + 0] = fibers[0].uv_D;
                uv[used_VXIndex + 2 * i + 1] = fibers[0].uv_D;
            }
            int innerIndex = used_VXIndex + 2 * i;
            int outerIndex = used_VXIndex + 2 * i + 1;
            int k = 0;
            if (i == ARC_N)
            {
                innerIndex = index0;
                outerIndex = index0x;
            }
            else
            {
                triangles[used_TRIndex + 9 * i + k++] = index1;
                triangles[used_TRIndex + 9 * i + k++] = index0;
                triangles[used_TRIndex + 9 * i + k++] = innerIndex;
            }


            triangles[used_TRIndex + 9 * i + k++] = index1x;
            triangles[used_TRIndex + 9 * i + k++] = index1;
            triangles[used_TRIndex + 9 * i + k++] = outerIndex;

            triangles[used_TRIndex + 9 * i + k++] = index1;
            triangles[used_TRIndex + 9 * i + k++] = innerIndex;
            triangles[used_TRIndex + 9 * i + k++] = outerIndex;

            index1 = innerIndex;
            index1x = outerIndex;

        }
        used_VXIndex += ARC_N * 2;
        used_TRIndex += (ARC_N * 9 + 6);




        //end-front
        index0x = fibers.Count * 4 - 2;
        index0 = fibers.Count * 4 - 4;
        index1 = fibers.Count * 4 - 3;
        index1x = fibers.Count * 4 - 1;

        //Debug.LogFormat("budget {0}, arc {1}",vertices.Length-used_VXIndex, ARC_N);
        for (int i = 0; i <= ARC_N; i++)
        {
            if (i < ARC_N)
            {
                Vector3 offset;
                offset = Quaternion.AngleAxis((180.0f * (i + 1)) / (ARC_N + 1), lastFiber_0.normal) * lastFiber_0.upOffset;
                Vector3 vert = lastFiber_0.ShiftedPoint + offset + lastFiber_0.normal * half_thickness;
                vertices[used_VXIndex + 2 * i + 0] = vert;
                vertices[used_VXIndex + 2 * i + 1] = vert - lastFiber_0.normal * frontThickness;
                normals[used_VXIndex + 2 * i + 0] = lastFiber_0.normal;
                normals[used_VXIndex + 2 * i + 1] = offset;
                uv[used_VXIndex + 2 * i + 0] = lastFiber_0.uv_D;
                uv[used_VXIndex + 2 * i + 1] = lastFiber_0.uv_D;
            }
            int innerIndex = used_VXIndex + 2 * i;
            int outerIndex = used_VXIndex + 2 * i + 1;
            int k = 0;
            if (i == ARC_N)
            {
                innerIndex = index0;
                outerIndex = index0x;
            }
            else
            {
                triangles[used_TRIndex + 9 * i + k++] = index0;
                triangles[used_TRIndex + 9 * i + k++] = index1;
                triangles[used_TRIndex + 9 * i + k++] = innerIndex;
            }


            triangles[used_TRIndex + 9 * i + k++] = index1;
            triangles[used_TRIndex + 9 * i + k++] = index1x;
            triangles[used_TRIndex + 9 * i + k++] = outerIndex;

            triangles[used_TRIndex + 9 * i + k++] = index1;
            triangles[used_TRIndex + 9 * i + k++] = outerIndex;
            triangles[used_TRIndex + 9 * i + k++] = innerIndex;

            index1 = innerIndex;
            index1x = outerIndex;

        }
        used_VXIndex += ARC_N*2;
        used_TRIndex += (ARC_N * 9+6);




        fiberFrontMesh.vertices = vertices;
        fiberFrontMesh.normals = normals;
        fiberFrontMesh.triangles = triangles;
        fiberFrontMesh.uv = uv;
    }
    private void UpdateBackMesh()
    {
        int used_VXIndex = fibers.Count * 4;
        int used_TRIndex = 3 * (fibers.Count - 1) * 6;
        Vector3[] vertices = new Vector3[used_VXIndex + ARC_N * 4];
        Vector3[] normals = new Vector3[used_VXIndex + ARC_N * 4];
        Vector2[] uv = new Vector2[used_VXIndex + ARC_N * 4];
        int[] triangles = new int[used_TRIndex + (ARC_N * 3 * 3 + 6) * 2];



        for (int i = 0; i < fibers.Count; i++)
        {
            vertices[4 * i + 0] = fibers[i].DownPosBack;
            vertices[4 * i + 1] = fibers[i].UpPosBack;
            vertices[4 * i + 2] = fibers[i].DownPosBack + fibers[i].normal * backThickness;
            vertices[4 * i + 3] = fibers[i].UpPosBack + fibers[i].normal * backThickness;

            normals[4 * i + 0] = -fibers[i].normal;
            normals[4 * i + 1] = -fibers[i].normal;
            normals[4 * i + 2] = -fibers[i].upOffset;
            normals[4 * i + 3] = fibers[i].upOffset;

            uv[4 * i + 0] = fibers[i].uv_D;
            uv[4 * i + 1] = fibers[i].uv_U;
            uv[4 * i + 2] = fibers[i].uv_D;
            uv[4 * i + 3] = fibers[i].uv_U;


            if (i == fibers.Count - 1)
            {
                continue;
            }
            else
            {
                //face
                triangles[18 * i + 0] = 4 * i + 0;
                triangles[18 * i + 1] = 4 * i + 4;
                triangles[18 * i + 2] = 4 * i + 1;

                triangles[18 * i + 3] = 4 * i + 1;
                triangles[18 * i + 4] = 4 * i + 4;
                triangles[18 * i + 5] = 4 * i + 5;


                //downBorder
                triangles[18 * i + 6] = 4 * i + 2;
                triangles[18 * i + 7] = 4 * i + 6;
                triangles[18 * i + 8] = 4 * i + 0;

                triangles[18 * i + 9] = 4 * i + 0;
                triangles[18 * i + 10] = 4 * i + 6;
                triangles[18 * i + 11] = 4 * i + 4;

                //upBorder
                triangles[18 * i + 12] = 4 * i + 1;
                triangles[18 * i + 13] = 4 * i + 5;
                triangles[18 * i + 14] = 4 * i + 3;

                triangles[18 * i + 15] = 4 * i + 3;
                triangles[18 * i + 16] = 4 * i + 5;
                triangles[18 * i + 17] = 4 * i + 7;
            }
        }


        int index0, index1, index0x, index1x;
        //start-back
        index0x = 2;
        index0 = 0;
        index1 = 1;
        index1x = 3;

        //Debug.LogFormat("budget {0}, arc {1}",vertices.Length-used_VXIndex, ARC_N);
        for (int i = 0; i <= ARC_N; i++)
        {
            if (i < ARC_N)
            {
                Vector3 offset;
                offset = Quaternion.AngleAxis(-(180.0f * (i + 1)) / (ARC_N + 1), fibers[0].normal) * fibers[0].upOffset;
                Vector3 vert = fibers[0].ShiftedPoint + offset - fibers[0].normal * half_thickness;
                vertices[used_VXIndex + 2 * i + 0] = vert;
                vertices[used_VXIndex + 2 * i + 1] = vert + fibers[0].normal * frontThickness;
                normals[used_VXIndex + 2 * i + 0] = -fibers[0].normal;
                normals[used_VXIndex + 2 * i + 1] = offset;
                uv[used_VXIndex + 2 * i + 0] = fibers[0].uv_D;
                uv[used_VXIndex + 2 * i + 1] = fibers[0].uv_D;
            }
            int innerIndex = used_VXIndex + 2 * i;
            int outerIndex = used_VXIndex + 2 * i + 1;
            int k = 0;
            if (i == ARC_N)
            {
                innerIndex = index0;
                outerIndex = index0x;
            }
            else
            {
                triangles[used_TRIndex + 9 * i + k++] = index0;
                triangles[used_TRIndex + 9 * i + k++] = index1;
                triangles[used_TRIndex + 9 * i + k++] = innerIndex;
            }


            triangles[used_TRIndex + 9 * i + k++] = index1;
            triangles[used_TRIndex + 9 * i + k++] = index1x;
            triangles[used_TRIndex + 9 * i + k++] = outerIndex;

            triangles[used_TRIndex + 9 * i + k++] = index1;
            triangles[used_TRIndex + 9 * i + k++] = outerIndex;
            triangles[used_TRIndex + 9 * i + k++] = innerIndex;

            index1 = innerIndex;
            index1x = outerIndex;

        }
        used_VXIndex += ARC_N * 2;
        used_TRIndex += (ARC_N * 9 + 6);




        //end-back
        index0x = fibers.Count * 4 - 2;
        index0 = fibers.Count * 4 - 4;
        index1 = fibers.Count * 4 - 3;
        index1x = fibers.Count * 4 - 1;

        //Debug.LogFormat("budget {0}, arc {1}",vertices.Length-used_VXIndex, ARC_N);
        for (int i = 0; i <= ARC_N; i++)
        {
            if (i < ARC_N)
            {
                Vector3 offset;
                offset = Quaternion.AngleAxis((180.0f * (i + 1)) / (ARC_N + 1), lastFiber_0.normal) * lastFiber_0.upOffset;
                Vector3 vert = lastFiber_0.ShiftedPoint + offset - lastFiber_0.normal * half_thickness;
                vertices[used_VXIndex + 2 * i + 0] = vert;
                vertices[used_VXIndex + 2 * i + 1] = vert + lastFiber_0.normal * frontThickness;
                normals[used_VXIndex + 2 * i + 0] = -lastFiber_0.normal;
                normals[used_VXIndex + 2 * i + 1] = offset;
                uv[used_VXIndex + 2 * i + 0] = lastFiber_0.uv_D;
                uv[used_VXIndex + 2 * i + 1] = lastFiber_0.uv_D;
            }
            int innerIndex = used_VXIndex + 2 * i;
            int outerIndex = used_VXIndex + 2 * i + 1;
            int k = 0;
            if (i == ARC_N)
            {
                innerIndex = index0;
                outerIndex = index0x;
            }
            else
            {
                triangles[used_TRIndex + 9 * i + k++] = index1;
                triangles[used_TRIndex + 9 * i + k++] = index0;
                triangles[used_TRIndex + 9 * i + k++] = innerIndex;
            }


            triangles[used_TRIndex + 9 * i + k++] = index1x;
            triangles[used_TRIndex + 9 * i + k++] = index1;
            triangles[used_TRIndex + 9 * i + k++] = outerIndex;

            triangles[used_TRIndex + 9 * i + k++] = index1;
            triangles[used_TRIndex + 9 * i + k++] = innerIndex;
            triangles[used_TRIndex + 9 * i + k++] = outerIndex;

            index1 = innerIndex;
            index1x = outerIndex;

        }
        used_VXIndex += ARC_N * 2;
        used_TRIndex += (ARC_N * 9 + 6);





        fiberBackMesh.vertices = vertices;
        fiberBackMesh.normals = normals;
        fiberBackMesh.triangles = triangles;
        fiberBackMesh.uv = uv;
    }
    #endregion


}