﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UniBliss;

public class ToolProfile : MonoBehaviour
{
    public string title;
    public ToolType type;
    //public ToolBehaviourType behaviour;
    //private float cost;
    public Sprite proPic;
    public Sprite proPic2;
    //public GameObject prefab;

    public bool videoLock = false;


    public float peelCost;
    public UnlockType unlockType;
    public int directCost;


    public string sheet_id;

    public bool IsUnlocked { get { return directCostRemainingToUnlock == 0; } }

    public int CostPending { get { return directCostRemainingToUnlock; } }

    public HardData<int> directCostRemainingToUnlock;
    public void Init( CSVRow sheetData)
    {
        int index = int.Parse( sheetData.fields[0]);
        sheet_id = sheetData.fields[1];
        unlockType = (UnlockType)int.Parse(sheetData.fields[2]);
        directCost = int.Parse(sheetData.fields[3]);
        peelCost = float.Parse(sheetData.fields[4]);

        directCostRemainingToUnlock = new HardData<int>(string.Format("{0}_direct_cost_pending",sheet_id), index==0?0:directCost);

    }

    public void ReduceRemainingCost(int reduction)
    {
        directCostRemainingToUnlock.value-=reduction;
    }

    //public void SetUnlocked()
    //{
    //    isUnlocked.value = true;
    //}

    //public static float GetCost(int index)
    //{
    //    if (LevelLoader.instance && LevelLoader.instance.allToolsUnlocked) return 0;


    //    if (index == 0) return 0;
    //    if (index == 1) return 12;

    //    //int formulaIndex = index - 1;

    //    float baseCost = 16f;
    //    float gainIncrementRatio = 1.08f;

    //    float cost = baseCost*Mathf.Pow(gainIncrementRatio,index-1);
    //    //for (int i = 1; i <= formulaIndex; i++)
    //    //{
    //    //    cost += baseGain * Mathf.Pow(gainIncrementRatio, i - 1);
    //    //}
    //    return cost;
    //}




//#if UNITY_EDITOR
//    [UnityEditor.MenuItem("Assets/Scriptable/ToolProfile")]
//    public static void Create()
//    {
//        ToolProfile so = ScriptableObject.CreateInstance<ToolProfile>();
//        UnityEditor.AssetDatabase.CreateAsset(so, "Assets/ToolProfile.asset");
//        UnityEditor.AssetDatabase.SaveAssets();
//        UnityEditor.EditorUtility.FocusProjectWindow();
//        UnityEditor.Selection.activeObject = so;
//    }
//#endif
}
public enum UnlockType
{
    STAR = 0,
    VIDEO = 1,
}