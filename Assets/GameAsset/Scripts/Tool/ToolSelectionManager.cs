﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UniBliss;

public class ToolSelectionManager : MonoBehaviour
{
    public static event System.Action<ToolProfile> onToolSelectionChanged;

    public static ToolProfile currentProfile
    {
        get
        {
            return ToolProfileManager.GetSelectedToolProfile();
        }
    }
    public static ToolType mode
    {
        get
        {
            return currentProfile.type;
        }
        set
        {
            if (value != mode)
            {
                ToolProfileManager.SetSelectedTool(value);
                onToolSelectionChanged?.Invoke(currentProfile);
            }
            AnalyticsAssistant.ObjectSelected(value.ToString());
        }
    }

    public static ToolBehaviourType behaviourType
    {
        get
        {
            switch (mode)
            {
                default:
                case ToolType.Knife:
                case ToolType.Axe:
                case ToolType.Cleaver:
                case ToolType.LightSaber:
                case ToolType.Machete:
                case ToolType.Sabre:
                case ToolType.Sword:
                    return ToolBehaviourType.SingleEdge;
                case ToolType.Peeler:
                case ToolType.Rake:
                case ToolType.Scissors:
                case ToolType.Bottle:
                    return ToolBehaviourType.Clawing;

                case ToolType.Pen:
                case ToolType.Scalpel:
                case ToolType.ChainSaw:
                    return ToolBehaviourType.Vertical;
            }

        }
    }


    
    public Button openMenuButton;
    public Button exitMenuButton;
    public GameObject selectionMenu;
    public GameObject selectionItemPrefab;
    public GameObject unlockableNotiObj;
    public RectTransform contentPanel;
    public bool centreObjectIfNecessaryFor3ItemColumn = true;


    public Image progressImage;
    public Image targetToolImage;

    private List<ToolSelectionUIItem> selectionItems = new List<ToolSelectionUIItem>();
    void Start()
    {
        openMenuButton.onClick.RemoveAllListeners();
        openMenuButton.onClick.AddListener(Load);
        exitMenuButton.onClick.RemoveAllListeners();
        exitMenuButton.onClick.AddListener(Exit);
        selectionMenu.SetActive(false);

        StarGazer.onStarCountChanged += OnStarCountChanged;
        OnStarCountChanged(StarGazer.StarsAvailable);
    }
    private void OnDestroy()
    {
        StarGazer.onStarCountChanged -= OnStarCountChanged;
    }
    void OnStarCountChanged(int count)
    {
        bool shouldNotify = false;
        for (int i = 0; i < ToolProfileManager.ProfileCount; i++)
        {
            ToolProfile profile = ToolProfileManager.GetProfile(i);

            if (!profile.IsUnlocked && profile.unlockType == UnlockType.STAR && profile.directCostRemainingToUnlock.value<=count)
            {
                shouldNotify = true;
                break;
            }
        }
        unlockableNotiObj.SetActive(shouldNotify);


    }
    void Load()
    {
        selectionMenu.SetActive(true);

        ToolProfile targetToolNow = ToolProfileManager.GetTargetToolProfile();

        if (targetToolNow)
        {
            targetToolImage.sprite = targetToolNow.proPic2;
        }

        progressImage.fillAmount = ToolProfileManager.GetProgressToTarget();

        for (int i = selectionItems.Count - 1; i >= 0; i--)
        {
            Pool.Destroy(selectionItems[i].gameObject);
            selectionItems.RemoveAt(i);
        }
        int N = ToolProfileManager.ProfileCount ;
        int m = N % 3;
        bool addOne = centreObjectIfNecessaryFor3ItemColumn && m == 1;

        for (int i = 0; i < N + (addOne ? 1 : 0); i++)
        {
            Transform tr = Pool.Instantiate(selectionItemPrefab).transform;
            tr.SetParent(contentPanel);
            tr.localScale = Vector3.one;
            tr.rotation = Quaternion.identity;

            selectionItems.Add(tr.GetComponent<ToolSelectionUIItem>());
        }
        Refresh(addOne);
    }

    void Refresh(bool hasExtra)
    {
        bool isUnlocked;
        bool isSelected;
        ToolProfile tp;
        if (hasExtra)
        {
            //Debug.Log("B");
            for (int i = 0; i < selectionItems.Count; i++)
            {
                isUnlocked = false;
                isSelected = false;
                tp = null;
                if (i == selectionItems.Count - 1)
                {
                    int ti = i - 1;
                    isUnlocked = ToolProfileManager.GetProfile(ti).IsUnlocked;
                    isSelected = (ti == ToolProfileManager.SelectedIndex);
                    tp = ToolProfileManager.GetProfile(ti);
                }
                else if(i != selectionItems.Count - 2)
                {
                    isUnlocked = ToolProfileManager.GetProfile(i).IsUnlocked;
                    isSelected = (i == ToolProfileManager.SelectedIndex);
                    tp = ToolProfileManager.GetProfile(i);
                }
                selectionItems[i].Load(tp,isSelected, Exit);
            }
        }
        else
        {
            //Debug.Log("A");
            for (int i = 0; i < selectionItems.Count; i++)
            {
                isUnlocked = ToolProfileManager.GetProfile(i).IsUnlocked;
                isSelected = (i == ToolProfileManager.SelectedIndex);
                tp = ToolProfileManager.GetProfile(i);
                selectionItems[i].Load(tp, isSelected, Exit);
            }
        }
    }
    
    void Exit()
    {
        selectionMenu.SetActive(false);
    }

}
public enum ToolType
{
    Peeler=0,
    Knife=1,
    Rake=2,
    Axe=3,
    Cleaver =4,
    Pen = 5,
    LightSaber = 6,
    Machete = 7,
    Sword = 8,
    Sabre = 9,
    Scalpel = 10,
    ChainSaw =11,
    Scissors = 12,
    Bottle = 13
}
public enum ToolBehaviourType
{
    SingleEdge = 0,//movement oriented motion
    Vertical = 1,//vertical
    Clawing = 2,//clawing motion
}