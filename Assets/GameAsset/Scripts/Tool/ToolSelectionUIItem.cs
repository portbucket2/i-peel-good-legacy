﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class ToolSelectionUIItem : MonoBehaviour
{
    public GameObject mainObject;
    public Text levelName;
    public Image profilePicImage;
    public Button selectButton;
    public GameObject isSelectedObject;
    public GameObject isLockedObject;
    public Image bgImage;
    public Color selectedColor;
    public Color normalColor;



    public GameObject isStarLockObject;
    public GameObject isVideoLockObject;

    public Text remainingDirectCostText;
    //public void Load(ToolProfile profile,bool isUnlocked, bool isSelected, System.Action onClick)
    //{
        
    //    if (profile == null)
    //    {
    //        LoadAsDummy();
    //        return;
    //    }
    //    mainObject.SetActive(true);
    //    levelName.text = profile.title;
    //    isSelectedObject.SetActive(isSelected);
    //    isLockedObject.SetActive(!isUnlocked);
    //    bgImage.color = isSelected ? selectedColor : normalColor;
    //    //profilePicImage.gameObject.SetActive(isUnlocked);
    //    profilePicImage.sprite = profile.proPic;

    //    //selectButton.interactable = isUnlocked;
    //    selectButton.onClick.RemoveAllListeners();
    //    selectButton.onClick.AddListener(()=> 
    //    {
    //        ToolSelectionManager.mode = profile.type;
    //        onClick?.Invoke();
    //    });
    //}

    public void Load(ToolProfile profile, bool isSelected, System.Action onNewToolChosen)
    {
        if (profile == null)
        {
            LoadAsDummy();
            return;
        }

        mainObject.SetActive(true);
        levelName.text = profile.title;
        bgImage.color = isSelected ? selectedColor : normalColor;
        profilePicImage.sprite = profile.proPic;


        bool isUnlocked = profile.IsUnlocked;

        isLockedObject.SetActive(!isUnlocked);
        isSelectedObject.SetActive(isSelected);
        isStarLockObject.SetActive(profile.unlockType == UnlockType.STAR);
        isVideoLockObject.SetActive(profile.unlockType == UnlockType.VIDEO);

        remainingDirectCostText.text = profile.directCostRemainingToUnlock.value.ToString();



        selectButton.interactable = true;

        //switch (profile.unlockType)
        //{
        //    case UnlockType.STAR:
        //        {
        //            selectButton.interactable = StarGazer.StarsAvailable>=profile.directCostRemainingToUnlock;
        //        }
        //        break;
        //    case UnlockType.VIDEO:
        //        {
        //            selectButton.interactable = true;
        //        }
        //        break;
        //}


        selectButton.onClick.RemoveAllListeners();
        selectButton.onClick.AddListener(() =>
        {
            if (isUnlocked)
            {
                ToolSelectionManager.mode = profile.type;
                onNewToolChosen?.Invoke();
            }
            else switch (profile.unlockType)
            {
                case UnlockType.STAR:
                        {

                            if (StarGazer.StarsAvailable < profile.directCostRemainingToUnlock)
                            {
                                DialogueAcceptReject.instance.Load_1Choice("Not enough stars!", "Finishing levels quickly will unlock more stars!", null);
                            }
                            else
                            {
                                //Debug.LogError("DialogureCalled");
                                DialogueAcceptReject.instance.Load_2Choice(
                                    titleText: "Unlock Tool",
                                    bodyText: string.Format("Unlock this item for {0} stars?", profile.directCostRemainingToUnlock.value),
                                    onAccept: () =>
                                    {
                                        StarGazer.ConsumeStars(profile.directCostRemainingToUnlock);
                                        profile.ReduceRemainingCost(profile.directCostRemainingToUnlock);
                                        ToolSelectionManager.mode = profile.type;
                                        onNewToolChosen?.Invoke();

                                    });
                            }

                        }
                    break;
                case UnlockType.VIDEO:
                        {
                            if (Application.isEditor)
                            {
                                profile.ReduceRemainingCost(1);
                                if (profile.directCostRemainingToUnlock > 0)
                                {
                                    Load(profile, isSelected, onNewToolChosen);
                                }
                                else
                                {
                                    ToolSelectionManager.mode = profile.type;
                                    onNewToolChosen?.Invoke();
                                }
                            }
                            else
                            {
                                selectButton.interactable = false;
                                Portbliss.Ad.AdController.ShowRewardedVideoAd((bool success) => {
                                    if (success)
                                    {
                                        profile.ReduceRemainingCost(1);
                                        if (profile.directCostRemainingToUnlock > 0)
                                        {
                                            Load(profile, isSelected, onNewToolChosen);
                                        }
                                        else
                                        {
                                            ToolSelectionManager.mode = profile.type;
                                            onNewToolChosen?.Invoke();
                                        }
                                    }
                                    else
                                    {
                                        selectButton.interactable = true;
                                    }

                                });
                            }
                            
                        }
                    break;
            }
            
        });


    }


    private void LoadAsDummy()
    {
        mainObject.SetActive(false);
    }
}
