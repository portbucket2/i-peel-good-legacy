﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniBliss;

public class ToolProfileManager : MonoBehaviour
{
    private static ToolProfileManager instance;


    [SerializeField] private ToolProfile[] profiles;
    [SerializeField] private TextAsset csvToolData;


    static HardData<int> selectedIndex;
    static HardData<int> currentPeelUnlockTarget;
    static HardData<float> currentUnlockProgress;





    void Awake()
    {
        if (instance)
        {
            Destroy(this.gameObject);
            return;
            
        }
        else
        {
            DontDestroyOnLoad(this.gameObject);
            instance = this;
            selectedIndex = new HardData<int>("SELECTED_TOOLINDEX", 0);
            currentPeelUnlockTarget = new HardData<int>("UNLOCK_TARGET_INDEX", 1);
            currentUnlockProgress = new HardData<float>("PROGRESS_TO_TARGET", 0);

            CSVRow[] rows = CSVReader.ReadCSVAsset(csvToolData, '|');

            for (int i = 0; i < profiles.Length; i++)
            {
                profiles[i].Init(rows[i]);
            }
        }
        
    }


    public static int SelectedIndex { get { return selectedIndex; } }
    public static int TargetIndex { get { return currentPeelUnlockTarget; } }
    public static int ProfileCount { get { return instance.profiles.Length; } }
    public static ToolProfile GetProfile(int index)
    {
        if (index < instance.profiles.Length && index >= 0)
            return instance.profiles[index];
        else
            return null;
    }
    public static ToolProfile GetTargetToolProfile()
    {
        ToolProfile profile = GetProfile(TargetIndex);
        while (profile != null && profile.IsUnlocked)
        {
            currentPeelUnlockTarget.value++;
            profile = GetProfile(TargetIndex);
        }
        return profile;
    }
    public static ToolProfile GetSelectedToolProfile()
    {
        return GetProfile(selectedIndex);
    }
    public static void SetSelectedTool(ToolType type)
    {
        for (int i = 0; i <= instance.profiles.Length; i++)
        {
            if (instance.profiles[i].type == type)
            {
                selectedIndex.value = i;
                return;
            }
        }
        Debug.LogError("Invalid Tool Selection");
    }

    public static float GetProgressToTarget()
    {
        ToolProfile targetprofile = GetTargetToolProfile();

        if (targetprofile == null) return 1;
        else return currentUnlockProgress.value / targetprofile.peelCost;
    }

    public static float AddProgressToTarget(float additionalPeel)
    {
        currentUnlockProgress.value += additionalPeel;
        if (GetProgressToTarget() > 1)
        {
            currentUnlockProgress.value = 0;
            currentPeelUnlockTarget.value++;
            return 1;
        }
        else return GetProgressToTarget();
    }


    //public static bool SubmitProgressAndCheckNewUnlock(float submittedAmount)
    //{
    //    int oldUnlockedIndex = unlockedIndex;

    //    totalUnlockProgress.value += submittedAmount;

    //    for (int i = oldUnlockedIndex+1; i <= UnlockedIndex; i++)
    //    {
    //        AnalyticsAssistant.ToolUnlocked(i,ToolProfileManager.GetProfile(i).name,LevelPrefabManager.TotalCompletedLevelCount);
    //    }

    //    if (unlockedIndex > oldUnlockedIndex) return true;
    //    else return false;
    //}


    //public static float NextUpgradeProgress()
    //{
    //    float cumulative = 0;
    //    float cumulativePrev = 0;
    //    for (int i = 0; i < instance.profiles.Length; i++)
    //    {
    //        cumulative += ToolProfile.GetCost(i);
    //        if (cumulative > totalUnlockProgress)
    //        {
    //            return (totalUnlockProgress - cumulativePrev) / (cumulative - cumulativePrev);
    //        }
    //        else if(i==instance.profiles.Length-1)
    //        {
    //            return 1;
    //        }
    //        cumulativePrev += ToolProfile.GetCost(i);
    //    }
    //    return -1;
    //}


}

