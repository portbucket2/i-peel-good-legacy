﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniBliss;

public class TrashManager : MonoBehaviour
{
    public static TrashManager instance;

    [SerializeField] bool dontEjectWithPhysics = false;
    float ejectForce = 0.75f;
    [SerializeField] Vector3 targetRelativeSpeed = Vector3.zero;
    [SerializeField] Transform floor;
    [SerializeField] Transform trashDiscardPoint;
    //[SerializeField] float windForce = 1;


    [Header("Wind Settings")]
    //[SerializeField] float windGap = 2.5f;
    [SerializeField] Transform windDirectionKeeper;
    public WindSetting[] windSettings;
    public WindSetting finalWindSettings;


    private void Awake()
    {
        instance = this ;
        StartCoroutine(SuckIn());
    }

    void Update()
    {
        for (int i = oldPeels.Count - 1; i >= 0; i--)
        {
            if (trashDiscardPoint && oldPeels[i].root.position.y < trashDiscardPoint.position.y)
            {
                oldPeels[i].TrashItems();
                oldPeels.RemoveAt(i);
            }
        }
        this.transform.Rotate( (RotationSpeedManager.instance.ChosenSpeed- targetRelativeSpeed) * Time.deltaTime);
    }




    IEnumerator SuckIn()
    {
        while (true)
        {
            foreach (var item in oldPeels)
            {
                //if (!item.root) continue;
                if (Time.time < item.ejectTime + 1.5f)
                {
                    float maxY = item.ejectYpos - floor.position.y;
                    float y = item.root.transform.position.y - floor.position.y;

                    float scaleM = Mathf.Lerp(0.6f, 1f, Mathf.Clamp01(y / maxY));
                    if (item.root.localScale.sqrMagnitude > scaleM * scaleM)
                        item.root.localScale = Vector3.one * scaleM;
                }
                else if(Time.time> item.ejectTime+2)
                {

                    if (!item.rgbd.isKinematic) {
                        item.rgbd.isKinematic = true;
                        item.UnloadPoolColliders();
                    }

                    float suckRate = Mathf.Lerp(0.02f, 0.2f, Mathf.Clamp01(item.fibers.Count / 350));
                    item.root.localPosition += new Vector3(0, -suckRate * Time.deltaTime, 0);
                }

            }
            yield return null;
        }
    }

    
    List<PeelBuilder> oldPeels = new List<PeelBuilder>();
    public void AddPeelToTrash(PeelBuilder peelBuilder)
    {
        //HERE
        peelBuilder.EjectPeel(disablePhysics: dontEjectWithPhysics,ejectForce: ejectForce);
        peelBuilder.root.SetParent(transform);

        oldPeels.Add(peelBuilder);
    }
    private void OnDestroy()
    {
        foreach (var peel in oldPeels)
        {
            peel.TrashItems();
        }
    }

}
public enum WindType
{
    BREEZE,
    LIGHT,
    HEAVY,
    GUST,
}

[System.Serializable]
public class WindSetting
{
    public string name { get { return type.ToString(); } }
    public WindType type;
    [Range(0, 1)] public float chance = 1;
    public float forceStrength = 1;
    [Range(0, 100)] public float strengthDeviation = 0;
    public float duration = 0.5f;
    [Range(0, 100)]  public float durationDeviation = 20;
    public float GetNextForceStrength()
    {
        return Handy.Deviate(forceStrength,strengthDeviation/100);
    }
    public float GetNextDuration()
    {
        return Handy.Deviate(duration, durationDeviation / 100);
    }
    //public float GetMaxPossibleDuration()
    //{
    //    return Handy.Deviate(duration, durationDeviation / 100);
    //}
}