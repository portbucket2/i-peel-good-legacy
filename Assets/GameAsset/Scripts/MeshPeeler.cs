﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MeshPeeler : MonoBehaviour, IThemeScript
{
    public static MeshPeeler instance;

    [Header("UX theme")]
    [SerializeField] Theme levelTheme = Theme.BLUE;
   
    [Header("Sound Settings")]
    [SerializeField]public SoundTrackType soundType = SoundTrackType.BANANA;
    [Range(0.01f,2)]
    [SerializeField]public float soundPitch = 1;


    [Header("Brush Characterstics")]
    [SerializeField] float brushRadius = 0.07f;
    //[SerializeField] float boostBrush_UpAngle = 5;
    //[SerializeField] float boostBrush_DownAngle = 15;
    //[SerializeField] float boostBrush_RadiusUpMult = 2;
    //[SerializeField] float boostBrush_RadiusDownMult = 2.5f;



    //[SerializeField] public int targetCount = 45000;
    //[SerializeField] public int targetTime = 60;

    [Header("Peel Characterstics")]
    [SerializeField] float peelDivisionLength = 0.1f;
    [SerializeField] float rotationPerTravelUnit = -500;
    [SerializeField] float rotationReductionMax = -240;
    [SerializeField] float extrusion = 0.002f;
    [SerializeField] int arcVertices = 24;


    [Header("Smoothness")]
    [SerializeField] float widthSmoothingLerpRate = 20;
    [SerializeField] float positionSmoothingLerpRate = 10f;

    [Header("Cut Off Limits")]
    [SerializeField] int fiberCountCutOffLimit = 1000;
    [SerializeField] float lengthCutOffLimit = 10000;
    [SerializeField] float lengthCutOff_deviation = 0.25f;
    [SerializeField] float angleCutOffLimit = 45;
    [SerializeField] float dirtyThreshold_new = 0.2f;
    [SerializeField] float dirtyThreshold_old =  0.01f;

    [Header("Settings")]

    [Range(0, 1)]
    [SerializeField] float innerRad = 0.98f;

    [SerializeField] float vertexImpressionDistance;


    public static float standardHalfPeelWidth;

    public float currentProgress = 0;
    private float r2;
    private float r1;
    private MeshFilter meshFilter;

    float peelDepth;
    Vector3[] gravityPoints;
    int[] gravityPointIndices;
    Vector3[] depressedvertices;

    //public LayerMask hitLayer;
    //MeshRenderer meshRenderer;



    List<VertexGroup> viGroup;
    float viScale;
    float groupCheckDistance;

    private bool gifRecordCompleted;
    public static System.Action onGIFRecordStart;
    public static System.Action onGIFRecordEnd;
    public static System.Action onGIFShareButton;
    public const float RECORD_ENDPOINT_PROGRESS = 0.50f;
    void CompleteRecording()
    {
        if (gifRecordCompleted) return;
        gifRecordCompleted = true;
        onGIFRecordEnd?.Invoke();
    }
    public void OnGIFShareButton()
    {
        onGIFShareButton?.Invoke();
    }
    private void Awake()
    {
        instance = this;
        
    }
    IEnumerator Start()
    {

        //onGIFRecordStart += () => 
        //{
        //    Debug.Log("<color='yellow'>Started Recording GIF</color>");
        //};
        //onGIFRecordEnd += () => 
        //{
        //    Debug.Log("<color='yellow'>Ended Recording GIF</color>");
        //};
        //onGIFShareButton += () =>
        //{
        //    Debug.Log("<color='yellow'>Shared GIF</color>");
        //};
        onGIFRecordStart?.Invoke();
        gifRecordCompleted = false;

        if (PeelableController.instance == null) throw new System.Exception("peelable not found");
        PeelBuilder.ArrayInit(fiberCountCutOffLimit,arcVertices);
        peelDepth = PeelableController.instance.half_thickness*2;
        viGroup = PeelableController.instance.vertexGroupList;
        viScale = PeelableController.instance.gridDivisionUnit;

        meshFilter = PeelableController.instance.meshFilter;
        currentProgress = 0;
        standardHalfPeelWidth =  brushRadius * 1.07f;
        //Debug.LogFormat("brush {0} peel {1}",brushRadius,peelWidth);
        r2 = brushRadius * brushRadius;
        r1 = innerRad* innerRad * r2;

        groupCheckDistance =  Mathf.Pow( Mathf.Sqrt( r2) + 0.87f * PeelableController.unitLength,2);

        Color[] colors = new Color[meshFilter.mesh.vertices.Length];
        for (int i = 0; i < colors.Length; i++)
        {
            colors[i] = Color.white;
        }
        meshFilter.mesh.colors = colors;
        //Debug.Log(vcolors.Length);

        //dummyMat = meshRenderer.material;

        mesh = meshFilter.mesh;
        //Debug.LogErrorFormat("target ratio========================== {0 }", (PeelableController.instance.targetCount / 0.96f) / mesh.vertexCount);
        //triangles = mesh.triangles;
        vertices = mesh.vertices;
        normals = mesh.normals;
        vcolors = mesh.colors;
        uvs = mesh.uv;

        _highestPeelLength = 0;
        gravityPoints = new Vector3[PeelableController.gravityPoints.Length];
        for (int i = 0; i < PeelableController.gravityPoints.Length; i++)
        {
            gravityPoints[i] = meshFilter.transform.InverseTransformPoint(PeelableController.gravityPoints[i]);
        }
        gravityPointIndices = new int[vertices.Length];
        depressedvertices = new Vector3[vertices.Length];
        int closestIndex = -1;
        float closestDistance = float.MaxValue;
        float distance;
        for (int v = 0; v < vertices.Length; v++)
        {
            for (int g = 0; g < gravityPoints.Length; g++)
            {
                distance = Vector3.Distance(vertices[v], gravityPoints[g]);
                if (distance < closestDistance)
                {
                    closestIndex = g;
                    closestDistance = distance;
                }
            }
            gravityPointIndices[v] = closestIndex;
            closestDistance = float.MaxValue;
            depressedvertices[v] = vertices[v] - vertexImpressionDistance * normals[v];
        }
       // Debug.Log("B");



        DecideNextCutOffLength();
        yield return null;

        PeelBuilder.OnNewLevelStarting();
    }
    Mesh mesh;
    //int[] triangles;
    Vector3[] vertices;
    Vector3[] normals;
    Color[] vcolors;
    Vector2[] uvs;


    private float runningLastCentreOffset;
    private float lastWidth;

    #region vertex loop data carriers
    float closestSqrDist;
    float farthestSqrDist;
    Vector2 uvUp;
    Vector2 uvDown;
    int total;
    float dirty;
    float distCumulitive;
    #endregion

    #region vertex loop

    public void VertexOperation_Sphere_Thin(Vector3 rchPoint, Vector3 travelVec, Vector3 travelOrigin)
    {
        //HERE
        VertexGroup vg;
        int[] vindices;
        

        int v;
        float d = 0;
        float tempSqrDist = 0;
        float temp_a;
        Vector3 tempVec;
        for (int i = 0; i < viGroup.Count; i++)
        {
            vg = viGroup[i];
            if (vg == null)
            {
                Debug.Log("<color='red'>vg is null</color>");
            }
            if ((vg.groupPostion - rchPoint).sqrMagnitude < currentRG)
            {
                vindices = vg.vertexIndices;
                if (vindices == null || vindices.Length == 0)
                {
                    Debug.Log("<color='red'>vindices is invalid</color>");
                }
                for (int vi = 0; vi < vindices.Length; vi++)
                {
                    v = vindices[vi];
                    tempVec.x = vertices[v].x - rchPoint.x;
                    tempVec.y = vertices[v].y - rchPoint.y;
                    tempVec.z = vertices[v].z - rchPoint.z;
                    d = tempVec.x * tempVec.x + tempVec.y * tempVec.y + tempVec.z * tempVec.z;
                    if (d < currentR2)
                    {
                        total++;
                        if (vcolors[v].a > 0)
                        {
                            dirty++;
                            if (d > currentR1)
                            {
                                temp_a = (currentR2 - d) / (currentR2 - currentR1);
                                if (temp_a < vcolors[v].a)
                                {
                                    currentProgress += vcolors[v].a - temp_a;
                                    vcolors[v].a = temp_a;
                                }
                            }
                            else if (vcolors[v].a > 0)
                            {
                                currentProgress += vcolors[v].a;
                                vcolors[v].a = 0;
                            }

                            tempSqrDist = Vector3.Cross(travelVec, vertices[v] - travelOrigin).sqrMagnitude;
                            distCumulitive += tempSqrDist;
                            if (tempSqrDist > farthestSqrDist)
                            {
                                uvUp = uvs[v];
                                farthestSqrDist = tempSqrDist;
                            }
                            if (tempSqrDist < closestSqrDist)
                            {
                                uvDown = uvs[v];
                                closestSqrDist = tempSqrDist;
                            }

                        }
                    }

                }
            }

        }
    }
    public void VertexOperation_Sphere_Thick(Vector3 rchPoint, Vector3 travelVec, Vector3 travelOrigin)
    {
        VertexGroup vg;
        int[] vindices;
        int v;
        float d = 0;
        float tempSqrDist = 0;
        Vector3 tempVec;
        for (int i = 0; i < viGroup.Count; i++)
        {
            vg = viGroup[i];
            if ((vg.groupPostion - rchPoint).sqrMagnitude < currentRG)
            {
                vindices = vg.vertexIndices;
                for (int vi = 0; vi < vindices.Length; vi++)
                {
                    v = vindices[vi];
                    tempVec.x = vertices[v].x - rchPoint.x;
                    tempVec.y = vertices[v].y - rchPoint.y;
                    tempVec.z = vertices[v].z - rchPoint.z;
                    d = tempVec.x * tempVec.x + tempVec.y * tempVec.y + tempVec.z * tempVec.z;
                    if (d < currentR2)
                    {
                        total++;
                        placeholderfunction ( v, tempSqrDist, travelVec, travelOrigin );
                        //if (vcolors[v].a > 0)
                        //{
                        //    dirty++;
                        //    currentProgress += 1;
                        //    vcolors[v].a = 0;

                        //    tempSqrDist = Vector3.Cross(travelVec, vertices[v] - travelOrigin).sqrMagnitude;
                        //    distCumulitive += tempSqrDist;
                        //    if (tempSqrDist > farthestSqrDist)
                        //    {
                        //        uvUp = uvs[v];
                        //        farthestSqrDist = tempSqrDist;
                        //    }
                        //    if (tempSqrDist < closestSqrDist)
                        //    {
                        //        uvDown = uvs[v];
                        //        closestSqrDist = tempSqrDist;
                        //    }
                        //    vertices[v] =  gravityPoints[gravityPointIndices[v]];
                        //}
                    }

                }
            }

        }
    }
    public void VertexOperation_Tunnel_Thin(RaycastHit hit, Ray ray, Vector3 travelVec, Vector3 travelOrigin)
    {
        VertexGroup vg;
        int[] vindices;
        int v;
        //float d = 0;
        float tempSqrDist;
        Vector3 tempVec;
        float temp_a;
        Vector3 rayDir = hit.transform.InverseTransformDirection(ray.direction).normalized;
        Vector3 rayOrigin = hit.transform.InverseTransformPoint(ray.origin);

        Vector3 A;
        Vector3 B;
        float x, y, z;


        for (int i = 0; i < viGroup.Count; i++)
        {
            vg = viGroup[i];
            tempVec = (vg.groupPostion - rayOrigin);
            tempSqrDist = Vector3.Cross(rayDir, tempVec).sqrMagnitude;

            if (tempSqrDist < currentRG)
            {
                vindices = vg.vertexIndices;
                for (int vi = 0; vi < vindices.Length; vi++)
                {
                    v = vindices[vi];

                    tempVec.x = vertices[v].x - rayOrigin.x;
                    tempVec.y = vertices[v].y - rayOrigin.y;
                    tempVec.z = vertices[v].z - rayOrigin.z;
                    A = rayDir;
                    B = tempVec;
                    x = (A.y * B.z - A.z * B.y);
                    y = (A.z * B.x - A.x * B.z);
                    z = (A.x * B.y - A.y * B.x);
                    tempSqrDist = x * x + y * y + z * z;// Vector3.Cross(rayDir, tempVec).sqrMagnitude;

                    //d = (vertices[v] - rchPoint).sqrMagnitude;
                    if (tempSqrDist < currentR2)
                    {
                        total++;
                        if (vcolors[v].a > 0)
                        {
                            dirty++;
                            if (tempSqrDist > currentR1)
                            {
                                temp_a = (currentR2 - tempSqrDist) / (currentR2 - currentR1);
                                if (temp_a < vcolors[v].a)
                                {
                                    currentProgress += vcolors[v].a - temp_a;
                                    vcolors[v].a = temp_a;
                                }
                            }
                            else if (vcolors[v].a > 0)
                            {
                                currentProgress += vcolors[v].a;
                                vcolors[v].a = 0;
                            }

                            tempSqrDist = Vector3.Cross(travelVec, vertices[v] - travelOrigin).sqrMagnitude;
                            distCumulitive += tempSqrDist;
                            if (tempSqrDist > farthestSqrDist)
                            {
                                uvUp = uvs[v];
                                farthestSqrDist = tempSqrDist;
                            }
                            if (tempSqrDist < closestSqrDist)
                            {
                                uvDown = uvs[v];
                                closestSqrDist = tempSqrDist;
                            }
                            //vertices[v] = peelableCentre_meshSpace;
                        }
                    }

                }
            }

        }
    }
    public void VertexOperation_Tunnel_Thick(RaycastHit hit, Ray ray, Vector3 travelVec, Vector3 travelOrigin)
    {
        VertexGroup vg;
        int[] vindices;
        int v;
        //float d = 0;
        float tempSqrDist;
        Vector3 tempVec;
        Vector3 rayDir = hit.transform.InverseTransformDirection(ray.direction).normalized;
        Vector3 rayOrigin = hit.transform.InverseTransformPoint(ray.origin);

        Vector3 A;
        Vector3 B;
        float x, y, z;


        for (int i = 0; i < viGroup.Count; i++)
        {
            vg = viGroup[i];
            tempVec = (vg.groupPostion - rayOrigin);
            tempSqrDist = Vector3.Cross(rayDir, tempVec).sqrMagnitude;

            if (tempSqrDist < currentRG)
            {
                vindices = vg.vertexIndices;
                for (int vi = 0; vi < vindices.Length; vi++)
                {
                    v = vindices[vi];

                    tempVec.x = vertices[v].x - rayOrigin.x;
                    tempVec.y = vertices[v].y - rayOrigin.y;
                    tempVec.z = vertices[v].z - rayOrigin.z;
                    A = rayDir;
                    B = tempVec;
                    x= (A.y * B.z - A.z * B.y);
                    y= (A.z * B.x - A.x * B.z);
                    z= (A.x * B.y - A.y * B.x);
                    tempSqrDist =   x*x + y*y + z*z;// Vector3.Cross(rayDir, tempVec).sqrMagnitude;

                    //d = (vertices[v] - rchPoint).sqrMagnitude;
                    if (tempSqrDist < currentR2)
                    {
                        total++;
                        placeholderfunction (v,tempSqrDist,travelVec,travelOrigin);
                        //if (vcolors[v].a > 0)
                        //{
                        //    dirty++;
                        //    currentProgress += 1;
                        //    vcolors[v].a = 0;

                        //    tempSqrDist = Vector3.Cross(travelVec, vertices[v] - travelOrigin).sqrMagnitude;
                        //    distCumulitive += tempSqrDist;
                        //    if (tempSqrDist > farthestSqrDist)
                        //    {
                        //        uvUp = uvs[v];
                        //        farthestSqrDist = tempSqrDist;
                        //    }
                        //    if (tempSqrDist < closestSqrDist)
                        //    {
                        //        uvDown = uvs[v];
                        //        closestSqrDist = tempSqrDist;
                        //    }
                        //    vertices[v] = gravityPoints[gravityPointIndices[v]];
                        //}
                    }

                }
            }

        }
    }

    void placeholderfunction (int v, float tempSqrDist, Vector3 travelVec, Vector3 travelOrigin )
    {
        if ( vcolors[v].a > 0 )
        {
            dirty++;
            currentProgress += 1;
            vcolors[v].a = 0;

            tempSqrDist = Vector3.Cross ( travelVec, vertices[v] - travelOrigin ).sqrMagnitude;
            distCumulitive += tempSqrDist;
            if ( tempSqrDist > farthestSqrDist )
            {
                uvUp = uvs[v];
                farthestSqrDist = tempSqrDist;
            }
            if ( tempSqrDist < closestSqrDist )
            {
                uvDown = uvs[v];
                closestSqrDist = tempSqrDist;
            }
            if ( vertexImpressionDistance > 0 )
            {
                //vertices[v] -= vertexImpressionDistance * normals[v];

                vertices[v] = depressedvertices[v];

                //if ( Vector3.Angle ( normals[v], vertices[v] - gravityPoints[gravityPointIndices[v]] ) <70 )
                //{
                //    vertices[v] -= vertexImpressionDistance * normals[v];
                //}
                //else
                //{
                //    vertices[v] = gravityPoints[gravityPointIndices[v]];
                //}
            }
            else
            {
                vertices[v] = gravityPoints[gravityPointIndices[v]];
            }
            
        }
    }
    #endregion

    float currentR2;
    float currentR1;
    float currentRG;
    public void OnKnifeUpdate(RaycastHit hit, VertexOpMode opMode, Ray ray)
    {
        // OnBladeTouch(touchPoint);
        hit.point = hit.point + hit.normal * extrusion;

        if (MainGameManager.settings.drawDebugRays) Debug.DrawRay(hit.point, hit.normal*2, Color.yellow, 1);
        float angle = Vector3.Angle(hit.normal,Vector3.up);
        currentR2 = r2;
        currentR1 = r1;
        currentRG = groupCheckDistance;
        //Debug.LogFormat("Mult choice angle {0}",angle);
        //if (angle < boostBrush_UpAngle)
        //{
        //    Debug.Log("upMultused");
        //    currentR1 *= boostBrush_RadiusUpMult;
        //    currentR2 *= boostBrush_RadiusUpMult;
        //    currentRG *= boostBrush_RadiusUpMult;
        //}
        //else if (angle > 180 - boostBrush_DownAngle)
        //{
        //    Debug.Log("downMultused");
        //    currentR1 *= boostBrush_RadiusDownMult;
        //    currentR2 *= boostBrush_RadiusDownMult;
        //    currentRG *= boostBrush_RadiusDownMult;
        //}

        Vector3 rchPoint = hit.transform.InverseTransformPoint(hit.point);
        total = 0;
        dirty = 0;
        distCumulitive = 0;
        Vector3 travelVec;
        Vector3 travelOrigin;
        bool hacking = false;
        if (peelBuilder != null && peelBuilder.fibers.Count>2)
        {
            travelOrigin = hit.transform.InverseTransformPoint(peelBuilder.root.TransformPoint(peelBuilder.lastFiber_2.basePoint - peelBuilder.lastFiber_2.upOffset.normalized * standardHalfPeelWidth*100));
            travelVec = hit.transform.InverseTransformDirection(peelBuilder.root.TransformDirection(peelBuilder.lastFiber_1.basePoint - peelBuilder.lastFiber_2.basePoint).normalized).normalized;

            //Debug.LogFormat("B- {0} - {1}",travelOrigin.magnitude,travelVec.magnitude);
        }
        else
        {
            travelVec = hit.transform.InverseTransformDirection( Vector3.Cross(hit.normal, Vector3.up)).normalized;
            Vector3 offsetDir = hit.transform.InverseTransformDirection(Vector3.Cross(hit.normal, travelVec)).normalized;
            travelOrigin = hit.transform.InverseTransformPoint(hit.point) - offsetDir * standardHalfPeelWidth*1000;
            //Debug.LogFormat("A- {0} - {1}", travelOrigin.magnitude, travelVec.magnitude);
            //Debug.LogFormat("C- {0} ", Vector3.Dot(travelVec,offsetDir));
            hacking = true;
        }
        closestSqrDist = float.MaxValue;
        farthestSqrDist = 0;
        uvUp = Vector2.zero;
        uvDown = Vector2.zero;

        switch (opMode)
        {
            case VertexOpMode.SPHERE:
                if(PeelableController.instance.useDepth)
                    VertexOperation_Sphere_Thick(rchPoint, travelVec, travelOrigin);
                else
                    VertexOperation_Sphere_Thin(rchPoint, travelVec, travelOrigin);
                break;
            case VertexOpMode.TUNNEL:
                if (PeelableController.instance.useDepth)
                    VertexOperation_Tunnel_Thick(hit, ray, travelVec, travelOrigin);
                else
                    VertexOperation_Tunnel_Thin(hit, ray, travelVec, travelOrigin);
                break;
        }



        mesh.colors = vcolors;
        mesh.vertices = vertices;
        //currentCount += dirty;
        float dirtyNess = (float)dirty / (float)total;
        float customHalfPeelWidth =    hit.transform.TransformDirection( Vector3.up*(Mathf.Sqrt( farthestSqrDist) - Mathf.Sqrt(closestSqrDist))).magnitude*0.55f;// Mathf.Sqrt(dirtyNess) * standardPeelWidth;
        //Debug.LogFormat("peelWidth {0}, standard {1}",customHalfPeelWidth,standardHalfPeelWidth);
        if (customHalfPeelWidth > standardHalfPeelWidth) customHalfPeelWidth = standardHalfPeelWidth;


        float baseDist = Mathf.Sqrt(Vector3.Cross(travelVec, rchPoint - travelOrigin).sqrMagnitude);
        float rmsDist = dirty>0? Mathf.Sqrt( distCumulitive /dirty) : baseDist;

        float centreOffset = rmsDist - baseDist;
        if (peelBuilder != null)
        {
            customHalfPeelWidth = Mathf.Lerp(lastWidth, customHalfPeelWidth, widthSmoothingLerpRate * Time.deltaTime);
            centreOffset = Mathf.Lerp(runningLastCentreOffset, centreOffset, positionSmoothingLerpRate * Time.deltaTime);

            //ReportPeelState?.Invoke ( peelBuilder.GetTotalFiberLength () );
            //if ( PeelLengthListener.instance != null )
            //{
            //    PeelLengthListener.instance.listener ( peelBuilder.GetTotalFiberLength () +residuePeelLength);
            //}
        }

        if (dirty > 0) runningLastCentreOffset = centreOffset;
        if (dirty > 0) lastWidth = customHalfPeelWidth;
       // Debug.Log(centreOffset);
        //Debug.LogFormat("A- {0} - {1}", hit.point, Vector3.Cross(hit.normal, travelVec).normalized * centreOffset);
        //Vector3 oldpoint = hit.point;
        Vector3 rootOffset = - Vector3.Cross(hit.normal, hit.transform.TransformDirection(travelVec)).normalized*centreOffset;
        if (hacking) rootOffset = -rootOffset;
        //Debug.Log(rootOffset)

        if (peelBuilder == null)
        {
            //Debug.Log("==========================A");
            if (dirtyNess > dirtyThreshold_new)
            {
                peelBuilder = new PeelBuilder(PeelableController.instance, hit, rotationPerTravelUnit,rotationReductionMax, peelDivisionLength,arcVertices);
                peelBuilder.AddFiber(hit, customHalfPeelWidth,rootOffset,uvUp,uvDown);
                peelBuilder.AddFiber(hit, customHalfPeelWidth, rootOffset, uvUp, uvDown);
                //Debug.Log("A"+ centreOffset * 1000);
            }
            else
            {
                //play particles
            }
        }
        else
        {
            //Debug.Log("==========================B");
            bool angleBreak = false;
            if (peelBuilder.fibers.Count > 2)
            {
                if (peelBuilder.GetTravelVec().sqrMagnitude > 0)
                {
                    Vector3 point = peelBuilder.root.InverseTransformPoint(hit.point);
                    float angle1 = Vector3.Angle(peelBuilder.GetTravelVec(), peelBuilder.GetPotentialTravelVec(point));
                    float angle2 = Vector3.Angle(peelBuilder.GetTravelVec_1(), peelBuilder.GetPotentialTravelVec(point));
                    if (angle1 > angleCutOffLimit)
                    {
                        angleBreak = true;
                        //Debug.LogFormat("Broke by angle 1 {0}", angle1);


                        //Debug.LogFormat("last {0}", peelBuilder.GetTravelVec().normalized*100);
                        //Debug.LogFormat("current {0}", peelBuilder.GetPotentialTravelVec(peelBuilder.root.InverseTransformPoint(hit.point)).normalized*100);
                    }
                    else if (angle2 > angleCutOffLimit)
                    {
                        angleBreak = true;
                        //Debug.LogFormat("Broke by angle 2 {0}", angle2);
                    }
                }


            }
            bool fiberCountBreak = peelBuilder.fibers.Count >= fiberCountCutOffLimit;
            bool jumpLimitBreak = peelBuilder.WillExceedJumpLimit(hit.point);
            if (jumpLimitBreak || angleBreak || fiberCountBreak)
            {
                Eject(string.Format("Jump break {0}, angle Break {1}, fiber count Break {2}", jumpLimitBreak, angleBreak, fiberCountBreak), jumpLimitBreak||angleBreak);
                peelBuilder = new PeelBuilder(PeelableController.instance, hit, rotationPerTravelUnit, rotationReductionMax, peelDivisionLength,arcVertices);
                peelBuilder.AddFiber(hit, customHalfPeelWidth, rootOffset, uvUp, uvDown);
                peelBuilder.AddFiber(hit, customHalfPeelWidth, rootOffset, uvUp, uvDown);
            }
            else if (peelBuilder.WillExceedTravelLimit(hit.point))
            {
                if (dirtyNess > dirtyThreshold_old)
                {
                    //Debug.Log("==========================C");
                    //Debug.Log("B" + centreOffset * 1000);
                    peelBuilder.AddFiber(hit, customHalfPeelWidth, rootOffset, uvUp, uvDown);
                    peelBuilder.UpdateLastFibers(hit, uvUp, uvDown);
                }
                else
                {
                    Eject("Not enough vertices for new fiber",true);
                }
            }
            else
            {

                //Debug.Log("==========================D");
                peelBuilder.UpdateLastFibers(hit, uvUp, uvDown);
            }

            if (peelBuilder != null && peelBuilder.GetTotalFiberLength() > nextCutOffLength)
            {
                Eject("Peel too long",false);
            }
        }
        if (peelBuilder != null) peelBuilder.UpdateFiberMesh();


    }


    private float nextCutOffLength;

    public Vector3 GetTravelDirectionFromPeelBuilder()
    {
        if (peelBuilder != null && peelBuilder.fibers.Count>2)
            return peelBuilder.root.TransformDirection(peelBuilder.lastFiber_1.basePoint - peelBuilder.lastFiber_2.basePoint).normalized;
        else return Vector3.down;
    }
    private void DecideNextCutOffLength()
    {
        nextCutOffLength = Handy.Deviate(lengthCutOffLimit, lengthCutOff_deviation);
    }

    PeelBuilder peelBuilder;
    public bool IsGeneratingPeelNow()
    {
        return peelBuilder != null;
    }


    //public delegate void PeelTermination(float previousLength, float freshLength);
    //public event PeelTermination onPeelTerminate;
    public void Eject(string ejectReason, bool terminate)
    {
        
        //HERE
        if (peelBuilder == null) return;
        
        float prog = MeshPeeler.instance.currentProgress / PeelableController.instance.targetCount;
        if (prog > RECORD_ENDPOINT_PROGRESS)
        {
            CompleteRecording();
        }

        float peelLength = peelBuilder.GetTotalFiberLength();


        residuePeelLength += peelLength;
        if (terminate)//player fault = actual termination of peel so should evaluate
        {

            if(PeelLengthDataGenerator.instance) PeelLengthDataGenerator.instance.OnPeelCompleted(_highestPeelLength, residuePeelLength);
            if (residuePeelLength > _highestPeelLength) _highestPeelLength = residuePeelLength;
            residuePeelLength = 0;

            //if (PeelLengthListener.instance != null)
            //{
            //    PeelLengthListener.instance.listener(0);
            //}
        }

        TrashManager.instance.AddPeelToTrash(peelBuilder);
        peelBuilder = null;

        DecideNextCutOffLength();
        if (MainGameManager.settings.showEjectLog) Debug.LogFormat("Ejected: {0}",ejectReason);

        
    }

    public static System.Action<float> ReportPeelState;

    private float _highestPeelLength;
    internal float finalHighestPeelLength
    {
        get
        {
            if (LevelPrefabManager.currentLevel.cumulitiveLevelNo == 1)
            {
                return 0.8f * ToolProfileManager.GetProfile(1).peelCost;
            }
            return _highestPeelLength;
        }
        set
        {
            _highestPeelLength = value;
        }
    }
    float residuePeelLength = 0;
    Theme IThemeScript.GetTheme()
    {
        return Theme.BLUE;
    }
}

public enum VertexOpMode
{
    SPHERE,
    TUNNEL,
}

