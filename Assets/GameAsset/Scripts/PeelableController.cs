﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PeelableController : MonoBehaviour
{
    //public float targetTime = 35;
    public const string EDITABLE_MESH_LAYER_NAME = "EditableMesh";
    public static int editableLayer { get { return LayerMask.NameToLayer(EDITABLE_MESH_LAYER_NAME); } }
    public static LayerMask editableLayerMask { get { return LayerMask.GetMask(EDITABLE_MESH_LAYER_NAME); } }


    public static PeelableController instance;
    public static float unitLength;
    //public static Vector3 weightCentre;
    public static Vector3[] gravityPoints;

    public string customLevelTitle;
    [Range(0, 1)]
    public float focusSpeedRatio = 1;
    public Vector3 baseRotationSpeed = new Vector3(0, 135, 0);
    internal bool focusNow;
    internal Vector3 GetFinalBaseRotationSpeed()
    {
        if (focusNow && false)// ABManager.GetValue(ABtype.FOCUS_SPEED) == 1)
        {
            return baseRotationSpeed * focusSpeedRatio;
        }

        return baseRotationSpeed;
    }
    public static void SetFocus(bool focus)
    {
        if (instance) instance.focusNow = focus;
    }
    
    public float targetCount = 0;
    public MeshCollider meshCollider;
    public MeshFilter meshFilter;
    public Material skinMaterial0;
    public Material skinMaterial1;
    //public Material skinMaterial2;

    //public Transform weightCentreTransform;
    public List<Transform> gravityPointTransforms;

    public float half_thickness = 0.0005f;
    [Range(0,1)]
    public float thicknessRatio = 0.5f;

    public bool useDepth = false;
    public MeshRenderer outerMesh;
    public List<MeshRenderer> allMeshes;





    [Header("Prepare Settings")]
    public MeshFilter loadColliderFrom;


    public void EditorInit(bool recalculateTargetCount = true)
    {
        allMeshes = new List<MeshRenderer>();
        int highestVCount = 0;
        foreach (Transform item in transform)
        {
            if (!item.gameObject.activeSelf) continue;
            MeshRenderer rend = item.GetComponent<MeshRenderer>();
            if (rend)
            {
                allMeshes.Add(rend);




                MeshFilter filter = rend.GetComponent<MeshFilter>();
                int vCount = filter.sharedMesh.vertexCount;
                if (vCount > highestVCount && meshFilter == null)
                {

                    outerMesh = rend;
                    highestVCount = vCount;

                }
            }
        }
        if (meshFilter)
        {
            outerMesh = meshFilter.GetComponent<MeshRenderer>();
        }
        else
        {
            meshFilter = outerMesh.GetComponent<MeshFilter>();
        }
        foreach (MeshRenderer rend in allMeshes)
        {
            rend.lightProbeUsage = UnityEngine.Rendering.LightProbeUsage.Off;
            rend.reflectionProbeUsage = UnityEngine.Rendering.ReflectionProbeUsage.Off;
            rend.receiveShadows = false;
            rend.motionVectorGenerationMode = MotionVectorGenerationMode.ForceNoMotion;

            if (rend == outerMesh)
            {
                rend.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
            }
            else
            {
                rend.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.On;
            }

            rend.gameObject.SetLayer(0, includeChildren: false);
            if (rend.gameObject.GetComponent<MeshCollider>() != null) DestroyImmediate(rend.gameObject.GetComponent<MeshCollider>());


        }
        if (!meshCollider)
        {

            GameObject mcObj = new GameObject("Mesh Collider");
            mcObj.SetLayer(editableLayer, includeChildren: false);
            mcObj.transform.SetParent(transform);
            mcObj.transform.localRotation = Quaternion.identity;
            mcObj.transform.localScale = Vector3.one;
            meshCollider = mcObj.AddComponent<MeshCollider>();
            if (loadColliderFrom)
            {
                meshCollider.sharedMesh = loadColliderFrom.sharedMesh;
                mcObj.transform.position = loadColliderFrom.transform.position;
            }
            else
            {
                meshCollider.sharedMesh = meshFilter.sharedMesh;
                mcObj.transform.position = meshFilter.transform.position;
            }

        }
        float targetShouldBeAround = (RegisterVertices()*0.96f);

        float diff = Mathf.Abs(targetCount - targetShouldBeAround);

        if ((diff / targetShouldBeAround) > 0.15f)
        {
            targetCount = targetShouldBeAround;
            Debug.LogWarning(string.Format("Target was reset to {0}",targetShouldBeAround));
        }
    }









    void Awake()
    {
        //Debug.Log(editableLayer);
        instance = this;
        unitLength = gridDivisionUnit;
        if (vertexGroupList.Count == 0) throw new System.Exception("PeelableControllerNotInitialized");

        Shader shader = outerMesh.sharedMaterial.shader;
        useDepth = !string.Equals(shader.name, "UniBliss/VertexColorControl/AlphaBasedTransparent");
        if (!useDepth) Debug.LogWarningFormat("Vertex operation thin not available! - {0}",gameObject.name);
        if (shader.name == "Standard") Debug.Log("Using standard shader!");
        
        if (targetCount == 0)
        {
             targetCount = meshFilter.mesh.vertexCount*0.965f;
        }

        if (!skinMaterial1) skinMaterial1 = skinMaterial0;
        //if (!skinMaterial2) skinMaterial2 = skinMaterial1;

        //if (weightCentreTransform == null) weightCentreTransform = outerMesh.transform;
        //weightCentre = outerMesh.transform.InverseTransformPoint(weightCentreTransform.position);

        if (gravityPointTransforms.Count ==0) gravityPointTransforms.Add(outerMesh.transform);
        List<Vector3> gravityVecPos = new List<Vector3>();
        foreach (var item in gravityPointTransforms)
        {
           gravityVecPos.Add( item.position);
          //  Debug.Log(item.position);
        }
        gravityPoints = gravityVecPos.ToArray();
      //  Debug.Log("A");
    }




    public int RegisterVertices()
    {
        int maxX = 0;
        int maxY = 0;
        int maxZ = 0;
        int minX = 0;
        int minY = 0;
        int minZ = 0;

        Mesh mesh = meshFilter.sharedMesh;
        Vector3[] vx = mesh.vertices;
        for (int i = 0; i < vx.Length; i++)
        {
            int X = Mathf.RoundToInt(vx[i].x / gridDivisionUnit);
            int Y = Mathf.RoundToInt(vx[i].y / gridDivisionUnit);
            int Z = Mathf.RoundToInt(vx[i].z / gridDivisionUnit);

            if (X > maxX) maxX = X;
            if (Y > maxY) maxY = Y;
            if (Z > maxZ) maxZ = Z;

            if (X < minX) minX = X;
            if (Y < minY) minY = Y;
            if (Z < minZ) minZ = Z;
        }
        //Debug.LogFormat("{0} {1} ", minX, maxX);
        //Debug.LogFormat("{0} {1} ", minY, maxY);
        //Debug.LogFormat("{0} {1} ", minZ, maxZ);

        vertexGroups = new TriList<VertexGroup>(minX, maxX, minY, maxY, minZ, maxZ);




        for (int i = minX; i <= maxX; i++)
        {
            for (int j = minY; j <= maxY; j++)
            {
                for (int k = minZ; k <= maxZ; k++)
                {
                    vertexGroups[i, j, k] = new VertexGroup(i,j, k,gridDivisionUnit);
                }
            }
        }

        for (int i = 0; i < vx.Length; i++)
        {
            int X = Mathf.RoundToInt(vx[i].x / gridDivisionUnit);
            int Y = Mathf.RoundToInt(vx[i].y / gridDivisionUnit);
            int Z = Mathf.RoundToInt(vx[i].z / gridDivisionUnit);
            vertexGroups[X, Y, Z].Add(i);
        }

        vertexGroupList = new List<VertexGroup>();
        for (int i = 0; i < vertexGroups.length; i++)
        {
            vertexGroups[i].Consolidate();
            if (vertexGroups[i].vertexIndices.Length>0)
            {

                vertexGroupList.Add(vertexGroups[i]);
            }

        }
        Debug.LogFormat("List Size = {0}", vertexGroupList.Count);
        return vx.Length;
    }



    //const int MAX_DIM = 6;
    //const int OFFSET = -3;
    public float gridDivisionUnit = 0.15f;
    [HideInInspector]public List<VertexGroup> vertexGroupList;
    private TriList<VertexGroup> vertexGroups;
}

[System.Serializable]
public class VertexGroup
{
    public Vector3 groupPostion;
    public int[] vertexIndices;

    [System.NonSerialized] List<int> tempVertexIndices = new List<int>();

    public VertexGroup(float x, float y, float z, float scale)
    {
        groupPostion = new Vector3(x*scale,y * scale, z * scale);
    }
   

    public void Add(int i)
    {
        tempVertexIndices.Add(i);
    }
    public void Consolidate()
    {
        //Debug.Log(tempVertexIndices.Count);
        vertexIndices = tempVertexIndices.ToArray();
        tempVertexIndices = null;
    }
}

public class TriList<T>
{
    private T[] arr;
    public int maxX { get; private set; }
    public int maxY { get; private set; }
    public int maxZ { get; private set; }
    public int minX { get; private set; }
    public int minY { get; private set; }
    public int minZ { get; private set; }


    public int length;
    int X_Length;
    int Y_Length;
    int Z_Length;

    int X_Offset;
    int Y_Offset;
    int Z_Offset;

    //int N;
    //int P;
    public TriList(int minX, int maxX, int minY, int maxY, int minZ,  int maxZ)
    {
        this.minX = minX;
        this.maxX = maxX;
        this.minY = minY;
        this.maxY = maxY;
        this.minZ = minZ;
        this.maxZ = maxZ;

        X_Length = maxX - minX + 1;
        Y_Length = maxY - minY + 1;
        Z_Length = maxZ - minZ + 1;

        X_Offset = minX;
        Y_Offset = minY;
        Z_Offset = minZ;
        //N = maxDim;
        //P = indexOffset;
        length = X_Length * Y_Length * Z_Length;
        //Debug.LogFormat("GroupCount = {0}", length);
        arr = new T[length];
        //Debug.LogFormat("GroupCount = {0}", arr.Length);
    }

    // Define the indexer to allow client code to use [] notation.
    public T this[int i,int j, int k]
    {
        get
        {
            i -= X_Offset;
            j -= Y_Offset;
            k -= Z_Offset;
            if (i >= X_Length || j >= Y_Length || k >= Z_Length)
            {
                throw new System.Exception("index is out of limit");
            }
            return arr[i * Y_Length * Z_Length + j * Z_Length + k];
        }
        set
        {
            i -= X_Offset;
            j -= Y_Offset;
            k -= Z_Offset;
            if (i >= X_Length || j >= Y_Length || k >= Z_Length)
            {
                throw new System.Exception("index is out of limit");
            }
            arr[i * Y_Length * Z_Length + j * Z_Length + k] = value;
        }
    }
    public T this[int i]
    {
        get
        {
            return arr[i];
        }
        set
        {
            arr[i] = value;
        }
    }
}
