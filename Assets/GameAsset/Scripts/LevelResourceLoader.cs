﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Portbliss.DLC;

public class LevelResourceLoader : MonoBehaviour
{
    LevelPrefabManager levelPrefabMan
    { get {return MainGameManager.instance.runningLevelManager; } }
    public GameObject Load(GameObject prefab)
    {
        //GameObject pref = (GameObject)DlcManager.LoadA_Level(levelPrefabMan.prefab_areaIndex, levelPrefabMan.prefab_levelIndex);
        if (prefab != null)
        {
            return Instantiate(prefab, Vector3.zero, Quaternion.identity, this.transform);
        }
        else
        {
            return null;
        }
    }
}
