﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IdleChecker : MonoBehaviour
{
    public static IdleChecker instance;

    public float idletime = 60;
    public float tm = 0;
    public float swipefeedbacktime = 1;
    //public float tapfeedbacktime = 1;

    //public GameObject mainidle;
    public GameObject gameidle;
    public GameObject text;

    public bool allowPeriodicHand = true;
    // Start is called before the first frame update
    void Start ()
    {
        if ( instance == null )
        {
            instance = this;
            DontDestroyOnLoad ( gameObject );
        }
        else
        {
            DestroyImmediate ( this );
        }

        if ( LevelLoader.Last_li == 0 && LevelLoader.Last_ai == 0 )
        {
            Invoke ( "showhandnow", 1 );
        }
    }

    // Update is called once per frame
    void Update ()
    {
        if ( Input.GetMouseButton ( 0 ) || !PeelGameManager.GameInProgress )
        {
            tm = 0;
            OnOff ( false );
        }
        else
        {
            tm += Time.deltaTime;
        }

        if ( tm > idletime )
        {
            tm = 0;

            if ( PeelGameManager.instance)// && PeelGameManager.instance.starCount < 3 )
            {
                //if ( !MainGameManager.instance.loaderObjects.activeSelf )
                //{
                //    Debug.Log ( "Running ingame idle feedback" );
                //    StartCoroutine ( runidlefeedback ( true ) );
                //}
                //else
                //{
                //    Debug.Log ( "Running menu idle feedback" );
                //    StartCoroutine ( runidlefeedback ( false ) );
                //}
                if(allowPeriodicHand)StartCoroutine ( runidlefeedback () );
            }

            
        } 
    }

    //IEnumerator runidlefeedback ( bool ingame )
    //{
    //    ( ingame ? gameidle : mainidle ).SetActive ( true );

    //    yield return new WaitForSeconds ( ingame? swipefeedbacktime :tapfeedbacktime);

    //    ( ingame ? gameidle : mainidle ).SetActive ( false );
    //}

    IEnumerator runidlefeedback ( )
    {
        OnOff ( true );


        yield return new WaitForSeconds (  swipefeedbacktime );

        OnOff ( false );
    }

    public void resetEverything ()
    {
        tm = 0;
        OnOff ( false );
        allowPeriodicHand = true;
    }

    public void showhandnow (  )
    {
        StartCoroutine ( runidlefeedback (  ) );
    }

    void OnOff ( bool on )
    {
        gameidle.SetActive ( on );
        text.SetActive ( on );
        // PeelLengthListener.instance.turnon ( !on );
    }

}
