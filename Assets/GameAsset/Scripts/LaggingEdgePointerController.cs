﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LaggingEdgePointerController : MonoBehaviour
{
    public RectTransform knifeEdgeTrans;
    public CameraAndToolController  knifeModelControllerRef;


    public float maxSpeedLimit;
    //float functionalSpeedLimit;

    RectTransform rectTrans;

    Vector2 scaledExtraOffsetForCuttingEdge;
    Vector2 canvasCentreDifference;

    public CanvasScaler canvScaler;

    //Vector2 reverseScaleVec;
    float reverseScaleFactor;
    //public RectTransform testrect;

    
    void Start()
    {
        rectTrans = this.transform.GetComponent<RectTransform>();

        reverseScaleFactor = canvScaler.referenceResolution.y/ Screen.height;

        //reverseScaleVec = Vector2.Scale(canvScaler.referenceResolution, new Vector2(1.0f / Screen.width, 1.0f / Screen.height));
        canvasCentreDifference =  new Vector2(Screen.width,Screen.height)*reverseScaleFactor / 2;

        scaledExtraOffsetForCuttingEdge = new Vector2(0, canvScaler.referenceResolution.y/ 8.0f);
        knifeEdgeTrans.anchoredPosition =scaledExtraOffsetForCuttingEdge;

        knifeModelControllerRef.savedMousePos = canvasCentreDifference / reverseScaleFactor;
        //Debug.Log(testrect.);
    }

    // Update is called once per frame
    void Update()
    {
        if (LevelPrefabManager.levelStartedFramesToSkip>0)
        {
            LevelPrefabManager.levelStartedFramesToSkip--;
            return;
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            //SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
        if (Input.GetKeyDown(KeyCode.P))
        {
            Debug.Break();
        }
        if (Input.GetMouseButtonDown(0) && TriggerAreaDetector.pressedOn)
        {
            if(AppLovinCrossPromo.Instance().isActiveAndEnabled) AppLovinCrossPromo.Instance().HideMRec();
            UpdateKnifePosTarget();
            WalkTheKnife();
        }
        else if (Input.GetMouseButton(0) && TriggerAreaDetector.pressedOn)
        {
            UpdateKnifePosTarget();
            WalkTheKnife(maxSpeedLimit);
        }

        else
        {
            knifeEdgeTrans.gameObject.SetActive(false);
            knifeModelControllerRef.NoKnifeUpdate();
        }



    }

    Vector2 targetPos;
    Vector2 targetMousePos;
    void UpdateKnifePosTarget()
    {
        targetPos = (Vector2)Input.mousePosition*reverseScaleFactor -canvasCentreDifference;// Vector2.Scale((Vector2)Input.mousePosition, reverseScaleVec) - canvasCentreDifference;
        //targetMousePos = (Vector2)Input.mousePosition + scaledExtraOffsetForCuttingEdge;
        //Debug.LogFormat("IMP: {0} , RESULT: {1}",Input.mousePosition,  targetPos );
    }


    void WalkTheKnife(float maxSpeed = float.MaxValue)
    {
        //HERE
        knifeEdgeTrans.gameObject.SetActive(true);
        float maxDist = maxSpeed * Time.deltaTime;

        Vector2 walkVec = targetPos - rectTrans.anchoredPosition;

        if (walkVec.magnitude > maxDist)
        {
            rectTrans.anchoredPosition = rectTrans.anchoredPosition + walkVec.normalized * maxDist;
        }
        else
        {
            rectTrans.anchoredPosition = rectTrans.anchoredPosition + walkVec;
        }

        //targetMousePos = Vector2.Scale(rectTrans.anchoredPosition + knifeEdgeTrans.anchoredPosition + canvasCentreDifference, new Vector2(1 / reverseScaleVec.x, 1 / reverseScaleVec.y));
        targetMousePos = (rectTrans.anchoredPosition + knifeEdgeTrans.anchoredPosition + canvasCentreDifference)/reverseScaleFactor;

        knifeModelControllerRef.KnifeMoveUpdate(targetMousePos);// rectTrans.anchoredPosition + extraOffsetForCuttingEdge+canvasCentreDifference);
    }
}
