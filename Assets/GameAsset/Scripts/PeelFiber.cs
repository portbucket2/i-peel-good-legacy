﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PeelFiber
{
    public float half_thickness;

    public float lastRecordedLength = 0;
    public Vector3 basePoint;
    public Vector3 normal;
    public Vector2 uv_U;
    public Vector2 uv_D;

    public float halfPeelWidth;
    public Vector3 rootOffset;
    public Vector3 upOffset;

    public Vector3 ShiftedPoint;// { get { return basePoint + rootOffset;  } }
    public Vector3 UpPosFront;// { get { return basePoint + rootOffset + upOffset + normal * half_thickness; } }
    public Vector3 DownPosFront;// { get { return basePoint +rootOffset - upOffset + normal * half_thickness; } }
    public Vector3 UpPosBack;// { get { return basePoint +rootOffset + upOffset - normal * half_thickness; } }
    public Vector3 DownPosBack;// { get { return basePoint +rootOffset - upOffset - normal * half_thickness; } }

    public PeelFiber(RaycastHit hit, Transform root, float halfPeelWidth, Vector3 rootOffset, float half_thickness)
    {
        this.half_thickness = half_thickness;
        this.rootOffset = rootOffset;
        this.halfPeelWidth = halfPeelWidth;
        this.normal = root.InverseTransformDirection(hit.normal);
        this.basePoint = root.InverseTransformPoint(hit.point + this.normal * PeelBuilder.EXTRUDE_CONST);
        SetDefaultUpOffset();
        UpdateFiberExtendedElements();


    }


    public void UpdateFiberExtendedElements()
    {
        ShiftedPoint = basePoint + rootOffset;
        UpPosFront = ShiftedPoint + upOffset + normal * half_thickness;
        DownPosFront = ShiftedPoint - upOffset + normal * half_thickness;
        UpPosBack = ShiftedPoint + upOffset - normal * half_thickness;
        DownPosBack = ShiftedPoint - upOffset - normal * half_thickness;
    }


    void SetDefaultUpOffset()
    {
        this.upOffset = Vector3.Cross(Vector3.up, this.normal).normalized * halfPeelWidth;

        if (upOffset.magnitude < halfPeelWidth)
        {
            this.upOffset = Vector3.Cross(Vector3.forward, this.normal).normalized * halfPeelWidth;
        }
    }
    public void UpdateUV(Vector2 uvup, Vector2 uvdown)
    {
        this.uv_U = uvup;
        this.uv_D = uvdown;
    }

}
public struct PeelFiberStruct
{
    public float halfPeelWidth;
    public float lastRecordedLength;

    public Vector3 basePoint;
    public Vector3 normal;
    public Vector2 uv_U;
    public Vector2 uv_D;

    public Vector3 rootOffset;
    public Vector3 upOffset;

    public Vector3 ShiftedPoint;
    public Vector3 UpPosFront;
    public Vector3 DownPosFront;
    public Vector3 UpPosBack;
    public Vector3 DownPosBack;

}