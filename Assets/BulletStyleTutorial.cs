﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UniBliss;

public class BulletStyleTutorial : MonoBehaviour
{
    public GameObject root;
    public GameObject prompt;
    public Button promptedButton;
    public Button intereactionDetectionButton;

    public HardData<bool> data;

    private void Awake()
    {
        data = new HardData<bool>("BulletStyleTutShown",false);

        if (data.value)
        {
            this.gameObject.SetActive(false);
        }
        else
        {
            root.SetActive(true);

            intereactionDetectionButton.onClick.RemoveAllListeners();
            intereactionDetectionButton.onClick.AddListener(() =>
            {
                prompt.SetActive(true);

                promptedButton.onClick.RemoveAllListeners();
                promptedButton.onClick.AddListener(() =>
                {
                    this.gameObject.SetActive(false);
                    data.value = true;
                });

            });


        }
    }


}
