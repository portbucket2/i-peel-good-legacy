﻿using UnityEngine;

#if UNITY_EDITOR

using UnityEditor;

public class ModelPreProc : AssetPostprocessor
{
    void OnPreprocessAsset()
    {
        if (assetPath.Contains("Assets/GameAsset/AllLevelFruits"))  // @-sign in the name triggers this step
        {
            ModelImporter modelImporter = assetImporter as ModelImporter;
            if (modelImporter != null)
            {
                Debug.LogFormat("PreProcessing {0}", modelImporter.name);
                modelImporter.optimizeMeshVertices = false;
            }
        }
    }
}

#endif

