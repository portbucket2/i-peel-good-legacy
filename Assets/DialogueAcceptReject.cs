﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class DialogueAcceptReject : MonoBehaviour
{

    public static DialogueAcceptReject instance;
    public Button acceptButton; 
    public Button okayButton;

    public GameObject root;

    public Text title;
    public Text body;


    public Button outerButton;
    public Button cancelButton;


    public GameObject choice2_obj;
    public GameObject choice1_obj;
    private void Awake()
    {

        if (!instance)
        {
            instance = this;
            outerButton.onClick.AddListener(Close);
            cancelButton.onClick.AddListener(Close);
        }
    }

    public void Load_2Choice(string titleText, string bodyText, Action onAccept)
    {
        root.SetActive(true);
        choice2_obj.SetActive(true);
        choice1_obj.SetActive(false);
        title.text = titleText;
        body.text = bodyText;
        acceptButton.onClick.RemoveAllListeners();
        acceptButton.onClick.AddListener(()=> { Close(); onAccept?.Invoke(); });
    }
    public void Load_1Choice(string titleText, string bodyText, Action onAccept)
    {
        root.SetActive(true); 
        choice1_obj.SetActive(true);
        choice2_obj.SetActive(false);
        title.text = titleText;
        body.text = bodyText;
        okayButton.onClick.RemoveAllListeners();
        okayButton.onClick.AddListener(() => { Close(); onAccept?.Invoke(); });
    }

    void Close()
    {
        root.SetActive(false);
    }

}
