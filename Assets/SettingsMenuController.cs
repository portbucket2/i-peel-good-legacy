﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Portbliss.Ad;

public class SettingsMenuController : MonoBehaviour
{
    public Toggle toggle;
    public Button GDPRsettings;
    // Start is called before the first frame update
    void Start()
    {
        toggle.isOn = CameraAndToolController.VibrationStatus.value;
        toggle.onValueChanged.AddListener((value) =>
        {
            CameraAndToolController.VibrationStatus.value = value;
            
        });
        StartCoroutine(CheckGDPR());
    }

    IEnumerator CheckGDPR()
    {
        yield return null;
        while (AdController.gdpr_done == false)
        {
            yield return null;
        }
        if (AdController.instance != null)
        {
            if (GDPRController.instance.ShowAnnoyance.value == 0 || GDPRController.instance.ShowAnnoyance.value == 1)
            {
                GDPRsettings.gameObject.SetActive(true);
                GDPRsettings.onClick.AddListener(() =>
                {
                    GDPRController.instance.StartGDPRFromSettings();
                });
            }
        }
    }

    // Update is called once per frame
    public void OnClose ()//attached on easy button con
    {
        
    }
}
