﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;



public class LevelUIItemLoader : MonoBehaviour
{
    public GameObject mainObject;
    public Button mainButton;
    public GameObject lockImage;
    public GameObject nowPlayingImage;
    public GameObject playImage;
    public GameObject starModule;
    public List<GameObject> stars;
    public Text levelName;


    public Image bgImage;
    public Color mainColor;
    public Color nowColor;

    LevelDefinition levelDef;
    public void LoadNormal(int areaIndex, int levelIndex)
    {
        LevelArea levelArea = LevelLoader.instance.levelAreas[areaIndex];
        levelDef = levelArea.levelDefinition[levelIndex];

        mainObject.SetActive(true);

        bool isUnlocked = LevelLoader.instance.allLevelsUnlocked || LevelLoader.instance.levelAreas[areaIndex].lastUnlockedLevelIndex.value >= levelIndex;
        bool isNowPlaying = (LevelPrefabManager.currentLevel && LevelPrefabManager.currentLevel.levelDefinition == levelDef && LevelPrefabManager.currentLevel.loadType == LoadType.NORMAL);

        mainButton.interactable = isUnlocked;
        int starEarned = levelArea.stars[levelIndex].value;
        lockImage.SetActive(!isUnlocked && !isNowPlaying);
        starModule.SetActive(isUnlocked && !isNowPlaying);
        playImage.SetActive(isUnlocked && starEarned <= 0 && !isNowPlaying);
        nowPlayingImage.SetActive(isNowPlaying);
        bgImage.color = isNowPlaying ? nowColor : mainColor;
        if (isUnlocked)
        {
            for (int i = 0; i < stars.Count; i++)
            {
                stars[i].SetActive(starEarned > i);
            }
        }
        if (LevelLoader.instance.allLevelsUnlocked)
        {
            levelName.text = string.Format("{0}. {1}", levelIndex + 1, levelDef.title);
        }
        else if (starEarned >= 1)
        {
            levelName.text =  levelDef.title;
        }
        else
        {
            levelName.text = string.Format("{0}", levelIndex + 1);
        }

        mainButton.onClick.RemoveAllListeners();
        mainButton.onClick.AddListener(()=> {
            if (isNowPlaying)
                MainGameManager.instance.ChangeLevelObjectState(GameState.GAME);
            else
            {
                LevelLoader.LoadA_Level(levelDef, (GameObject resource) =>
                {
                    if (resource) MainGameManager.instance.LoadLevelFromSelection(LoadType.NORMAL, areaIndex, levelIndex, levelDef, resource);
                });
            }
        });
    }
    public void LoadDaily(int levelIndex, LevelDefinition levelDef, bool isUnlocked, int starEarned)
    {
        this.levelDef = levelDef;

        mainObject.SetActive(true);

        bool isNowPlaying = (LevelPrefabManager.currentLevel && LevelPrefabManager.currentLevel.levelDefinition == levelDef && LevelPrefabManager.currentLevel.loadType == LoadType.DAILY);

        mainButton.interactable = isUnlocked;
        lockImage.SetActive(!isUnlocked && !isNowPlaying);
        starModule.SetActive(isUnlocked && !isNowPlaying);
        playImage.SetActive(isUnlocked && starEarned <= 0 && !isNowPlaying);
        nowPlayingImage.SetActive(isNowPlaying);
        bgImage.color = isNowPlaying ? nowColor : mainColor;
        if (isUnlocked)
        {
            for (int i = 0; i < stars.Count; i++)
            {
                stars[i].SetActive(starEarned > i);
            }
        }
        if (LevelLoader.instance.allLevelsUnlocked)
        {
            levelName.text = string.Format("{0}. {1}", levelIndex + 1, levelDef.title);
        }
        else if (starEarned >= 1)
        {
            levelName.text = levelDef.title;
        }
        else
        {
            levelName.text = string.Format("{0}", levelIndex + 1);
        }

        mainButton.onClick.RemoveAllListeners();
        mainButton.onClick.AddListener(() => {
            if (isNowPlaying)
                MainGameManager.instance.ChangeLevelObjectState(GameState.GAME);
            else
            {
                LevelLoader.LoadA_Level(levelDef, (GameObject resource) =>
                {
                    if (resource) MainGameManager.instance.LoadLevelFromSelection(LoadType.DAILY, 0, levelIndex, levelDef, resource);
                });
            }
        });
    }
    public void LoadAsDummy()
    {
        mainObject.SetActive(false);
    }

   
}