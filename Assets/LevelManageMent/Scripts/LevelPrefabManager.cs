﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Portbliss.Ad;

public class LevelPrefabManager : MonoBehaviour
{
    public Theme theme;
    public bool suspendedExternally;
    public static event System.Action<Theme> newPrefabLoading;
    public static LevelPrefabManager currentLevel;
    int areaI { get; set; }
    public int levelI { get; private set; }
    int sequence_areaIndex { get { return levelDefinition.sequence_AI; } }
    int sequence_levelIndex { get { return levelDefinition.sequence_LI; } }

    public LevelDefinition levelDefinition;

    int stars = 0;
    System.Action onEnd;


    public InLevelCanvasManager canvMan
    {
        get
        {
            return MainGameManager.instance.levelCanvas;
        }
    }

    public void AnimateNextLevelButton()
    {
        if (canvMan.successUIController.nextButtonAnim)
        {
            canvMan.successUIController.nextButtonAnim.SetTrigger("go");
        }
        if ( canvMan.successUIController.nextButtonAnim_2step )
        {
            canvMan.successUIController.nextButtonAnim_2step.SetTrigger ( "go" );
        }
    }
    public int cumulitiveLevelNo;
    public LoadType loadType = LoadType.NORMAL;

    public string LevelTypeInfo
    {
        get
        {
            switch (loadType)
            {
                default:
                case LoadType.NORMAL:

                    if (sequence_areaIndex != areaI || sequence_levelIndex != levelI)
                        return "random";
                    else
                        return "normal";

                case LoadType.DAILY:
                    return "daily";
            }
        }
    }


    public bool rewardedVideoAdPlayed;
    public void LoadAs(LoadType loadType, int areaIndex, int levelIndex, LevelDefinition deifinition, GameObject prefab,  System.Action onEnd)
    {
        this.loadType = loadType;
        levelDefinition = deifinition;
        cumulitiveLevelNo = LevelDefinition.Query_CumulativeNumber(areaIndex,levelIndex, loadType);
        currentLevel = this;
        areaI = areaIndex;
        levelI = levelIndex;

        rewardedVideoAdPlayed = false;
        this.onEnd = onEnd;

        LevelResourceLoader levelResourceLoader = GetComponent<LevelResourceLoader>();
        GameObject innerResource = null;
        if (levelResourceLoader) innerResource = levelResourceLoader.Load(prefab);

        theme = Theme.BLUE;
        IThemeScript themescript = null;
        if (innerResource != null)
        {
            themescript = innerResource.GetComponent<IThemeScript>();
        }
        
        if (themescript != null)
        {
            theme = themescript.GetTheme();
           // Debug.Log(themescript);
        }

        canvMan.LevelStart(this);
        //Debug.Log(theme);
        newPrefabLoading?.Invoke(theme);


        AnalyticsAssistant.LevelStarted(cumulitiveLevelNo, levelDefinition.title, LevelTypeInfo);
    }

    public void UnLoad()
    {
        //onPreDestroy?.Invoke();
        currentLevel = null;
        Destroy(gameObject);
        Resources.UnloadUnusedAssets();
        onEnd?.Invoke();
    }
    public int GetStarData()
    {
        switch (loadType)
        {
            case LoadType.NORMAL:
                return LevelLoader.instance.levelAreas[sequence_areaIndex].stars[sequence_levelIndex].value;
            case LoadType.DAILY:
                return DailyLevelManagement.instance.GetStarCount(levelI);
            default:
                return -1;
        }
    }
    public void SetStarData(int star)
    {
        switch (loadType)
        {
            case LoadType.NORMAL:
                LevelLoader.instance.levelAreas[sequence_areaIndex].stars[sequence_levelIndex].value = star;
                break;
            case LoadType.DAILY:
                DailyLevelManagement.instance.SetStarCount(levelI, star);
                break;
        }
    }

    public void AddStar ()
    {
        stars++;
        //canvMan.successUIController.SetStars ( stars );

        int starC = GetStarData ();
        if ( starC < stars )
        {
            SetStarData ( stars );
        }
    }

    public void ReportEarlyCompletion(int reportedStars)
    {
        int starC = GetStarData();
        if (starC < reportedStars)
        {
            SetStarData(reportedStars);
        }
        if (loadType == LoadType.NORMAL)
        {
            AnalyticsAssistant.LevelCompletedAppsFlyer(LevelPrefabManager.currentLevel.cumulitiveLevelNo);
            LevelRandomizer();
        }
    }


    private static UniBliss.HardData<int> totalCompletedLevelNoHD;
    public static int TotalCompletedLevelCount
    {
        get
        {
            if (totalCompletedLevelNoHD == null)
            {
                totalCompletedLevelNoHD = new UniBliss.HardData<int>("TOTAL_COMPLETED_LEVEL_COUNT",
                    LevelDefinition.Query_CumulativeNumber(LevelLoader.Last_ai, LevelLoader.Last_li, LoadType.NORMAL));
            }
            return totalCompletedLevelNoHD.value;
        }
        set
        {
            if (totalCompletedLevelNoHD == null)
            {
                totalCompletedLevelNoHD = new UniBliss.HardData<int>("TOTAL_COMPLETED_LEVEL_COUNT",
                    LevelDefinition.Query_CumulativeNumber(LevelLoader.Last_ai, LevelLoader.Last_li, LoadType.NORMAL));
            }
            totalCompletedLevelNoHD.value = value;
        }
    }
    public void OnComplete(bool success,int timingStars)
    {
        if (success)
        {
            TotalCompletedLevelCount++;
            int starC = GetStarData();
            if (starC >= stars)
            {
                stars = starC;
            }
            else
            {
                SetStarData(stars);
            }
            if (loadType == LoadType.NORMAL)
            {
                AnalyticsAssistant.LevelCompletedAppsFlyer(LevelPrefabManager.currentLevel.cumulitiveLevelNo);
                LevelRandomizer();
            }
            canvMan.LoadSucces();

        }
        else
        {
            canvMan.LoadFail();
        }
        canvMan.successUIController.SetStars(timingStars);
    }

    void LevelRandomizer ()
    {
        if ( !checkcontainment ( sequence_areaIndex, sequence_levelIndex ) )
        {
            LevelLoader.instance.AddtoRandomException ( sequence_areaIndex, sequence_levelIndex );
            LevelLoader.instance.changerandomindex ();
        }
        if (levelI >= LevelLoader.instance.levelAreas[areaI].lastUnlockedLevelIndex.value  )
        {
            LevelLoader.instance.levelAreas[areaI].lastUnlockedLevelIndex.value = levelI + 1 ;

            int arI = UnityEngine.Random.Range ( 0, LevelLoader.instance.levelAreas.Count );
            int lvlI = UnityEngine.Random.Range ( 0, LevelLoader.instance.levelAreas[arI].levelDefinition.Count );

            int j = 0;
            while ( checkcontainment ( arI, lvlI ) && j < 100 )
            {
                arI = UnityEngine.Random.Range ( 0, LevelLoader.instance.levelAreas.Count );
                lvlI = UnityEngine.Random.Range ( 0, LevelLoader.instance.levelAreas[arI].levelDefinition.Count );
                j++;
            }

            LevelLoader.instance.randomAreaIndex.value = arI;
            LevelLoader.instance.randomLevelIndex.value = lvlI;
        }


    }
    bool checkcontainment (int aI, int lI)
    {
        for ( int i = 0; i < LevelLoader.instance.numberofexceptions; i++ )
        {
            if ( LevelLoader.instance.randomlevelcollection[i * 2].value == aI && LevelLoader.instance.randomlevelcollection[i * 2 +1].value == lI )
            {
                return true;
            }
        }

        return false;
    }

    public void OnReset()
    {
        if (suspendedExternally) return;

        ShowADNow(()=>
        {
            //Debug.Log("<color='red'>matro reset e restart flag on hoilo folafol:"+isItRestart+" </color>");
            int ai = currentLevel.areaI;
            int li = currentLevel.levelI;
            LoadType lt = loadType;

            UnLoad();

            MainGameManager.instance.StartLevel(lt, ai, li, levelDefinition, MainGameManager.lastLoadedLevelResource);

        }, isRestart: false);

    }


    public void OnNext()
    {
        if (suspendedExternally) return;
        ShowADNow(() => {
            int ai = 0;
            int li = 0;
            LevelDefinition levelDef = null;
            LoadType lt = loadType;
            switch (loadType)
            {
                case LoadType.NORMAL:
                    {
                        ai = LevelLoader.GetNextLevel_ai(areaI, levelI);
                        li = LevelLoader.GetNextLevel_li(areaI, levelI);
                        levelDef = LevelLoader.FetchAppropriateDefinition(ai, li);
                    }
                    break;
                case LoadType.DAILY:
                    {
                        ai = areaI;
                        li = levelI + 1;
                        levelDef = DailyLevelManagement.instance.GetLevel(li);
                        if (levelDef == null)
                        {

                            ai = LevelLoader.Last_ai;
                            li = LevelLoader.Last_li;
                            levelDef = LevelLoader.FetchAppropriateDefinition(ai, li);
                            lt = LoadType.NORMAL;
                        }
                    }
                    break;
                default:
                    break;
            }


            LevelLoader.LoadA_Level(levelDef, (GameObject resource) =>
            {
                if (resource)
                {
                    UnLoad();
                    MainGameManager.instance.StartLevel(lt, ai, li, levelDef, resource);
                }
            });

        }, isRestart:false);

    }

    private void ShowADNow(System.Action onAdComplete, bool isRestart)
    {
        if ( AdController.hasNoAdIAP_PurchasedHD != null && GameConfig.hasIAP_NoAdPurchasedHD != null )
        {
            if ( AdController.hasNoAdIAP_PurchasedHD.value || rewardedVideoAdPlayed || GameConfig.hasIAP_NoAdPurchasedHD.value )
            {
                onAdComplete?.Invoke ();
            }
            else
            {
                try
                {
                    TimedAd.AdIteration ( ( success ) =>
                    {
                        Debug.Log ( "<color='magenta'>try catch er try te, ad called</color>" );
                        onAdComplete?.Invoke ();
                    }, isRestart );
                }
                catch ( System.Exception e )
                {
                    Debug.Log ( "<color='magenta'>try catch er exception e</color>" );
                    Debug.LogError ( e.Message );
                    onAdComplete?.Invoke ();
                }
            }
        }
        else
        {
            onAdComplete?.Invoke ();
        }
    }

    public static int levelStartedFramesToSkip = 0;
 
    //public static bool isItRestart = false;
    public void OnLevels()
    {
        if (suspendedExternally) return;
        int focusIndex = -1;
        LoadType lt = LoadType.NORMAL;
        if (currentLevel)
        {
            lt = loadType;
            focusIndex = currentLevel.levelI;
        }
        MainGameManager.instance.areaLoader.LoadLevelList(lt, focusIndex);
        //UnLoad();
    }

}
public enum Theme
{
    BLUE = 0,
    RED = 1,
    GREEN = 2,
    YELLOW =3,
}
public interface IThemeScript
{
    Theme GetTheme();
}