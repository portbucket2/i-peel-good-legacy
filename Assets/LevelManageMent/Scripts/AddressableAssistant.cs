﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;

public class AddressableAssistant : MonoBehaviour
{
    public const string remoteBaseAddress = "Assets/Resources_Remote";





    public const bool testing = true;
    public Text statusText;
    public Text requestText;
    public Text localLogText;
    public Text remoteLogText;






    #region loading

    public bool IsReadyToLoad(LevelDefinition def)
    {
        if (string.IsNullOrEmpty(def.remoteSubPath)) return true;
        if (!packsInProcess.ContainsKey(def.remoteSubPath)) return false;
        return packsInProcess[def.remoteSubPath].Status == PackDataStatus.SUCCEEDED;
    }

    public void RequestLoad(LevelDefinition def, System.Action<GameObject> onReadyToLoad)
    {
        int areaI_prefab = def.prefab_AI;
        int levelI_Prefab = def.prefab_LI;
        string variant = def.variant;
        string subPath = def.remoteSubPath;


        if (string.IsNullOrEmpty(subPath))
        {
            if (testing) localLogText.text += string.Format("L{0},", def.sequence_LI);
            //if (testing) localLogText.text += string.Format("L-{0},", levelI_Prefab);
            GameObject lvl;
            if (string.IsNullOrEmpty(variant))
            {
                lvl = (GameObject)Resources.Load(string.Format("Area {0}/Level {1}", areaI_prefab + 1, levelI_Prefab + 1));
            }
            else
            {
                lvl = (GameObject)Resources.Load(string.Format("Area {0}/Level {1}_{2}", areaI_prefab + 1, levelI_Prefab + 1, variant));
            }

            onReadyToLoad?.Invoke(lvl);
        }
        else if (packsInProcess.ContainsKey(subPath))
        {

            PackData pack = packsInProcess[subPath];
            switch (pack.Status)
            {
                case PackDataStatus.QUEUED:
                    {
                        if (testing) remoteLogText.text += string.Format("RQ{0},", def.sequence_LI);
                        string address;
                        if (string.IsNullOrEmpty(variant))
                            address = string.Format("{0}/{1}/Level {2}.prefab", remoteBaseAddress, subPath, levelI_Prefab + 1);
                        else
                            address = string.Format("{0}/{1}/Level {2}_{3}.prefab", remoteBaseAddress, subPath, levelI_Prefab + 1, variant);

                        pack.downloadHandle = Addressables.LoadAssetsAsync<GameObject>(address,null);
                        pack.downloadStartTime = Time.realtimeSinceStartup;
                        pack.SetStatus(PackDataStatus.PROCESSING);
                        pack.downloadHandle.Completed += (AsyncOperationHandle handle) =>
                        {
                            if (testing) remoteLogText.text += string.Format("RQ{0}=<color=#FFFF00>{1}</color>,", def.sequence_LI,handle.Status.ToString());
                            if (handle.Status == AsyncOperationStatus.Succeeded)
                            {
                                pack.SetStatus(PackDataStatus.SUCCEEDED);
                            }
                            else
                            {
                                pack.SetStatus(PackDataStatus.FAILED);
                            }
                        };

                        DLCDialogueMan.instance.LoadProcessing(pack.downloadHandle, () => { RequestLoad(def, onReadyToLoad); });
                    }
                    break;
                case PackDataStatus.PROCESSING:
                    {
                        if (testing) remoteLogText.text += string.Format("RP{0},", def.sequence_LI);
                        DLCDialogueMan.instance.LoadProcessing(pack.downloadHandle, () => { RequestLoad(def, onReadyToLoad); });
                    }
                    break;
                case PackDataStatus.FAILED:
                    {
                        if (testing) remoteLogText.text += string.Format("RF{0},", def.sequence_LI);
                        DLCDialogueMan.instance.LoadFailed();
                    }
                    break;
                case PackDataStatus.SUCCEEDED:
                    {
                        if (testing) remoteLogText.text += string.Format("RS{0},", def.sequence_LI);
                        string address;
                        if (string.IsNullOrEmpty(variant))
                            address = string.Format("{0}/{1}/Level {2}.prefab", remoteBaseAddress, subPath, levelI_Prefab + 1);
                        else
                            address = string.Format("{0}/{1}/Level {2}_{3}.prefab", remoteBaseAddress, subPath, levelI_Prefab + 1, variant);

                        Addressables.LoadAssetsAsync<GameObject>(address, onReadyToLoad);
                    }
                    break;
            }
        }
        else
        {
            Debug.LogError("Requested item is never queued!!!");
        }
    }
    #endregion



    #region queing 
    public void Init(LevelDefinition initLevel, System.Action onInitDone)
    {
        if (testing)
        {
            statusText.text = "init\n";
            requestText.text = "";
            localLogText.text = "";
            remoteLogText.text = "";
        }
        //PrepareLevel_ReturnReady(initLevel);

        RequestQueue(initLevel, 1, () =>
        {
            onInitDone?.Invoke();
        });
        UniBliss.Centralizer.Add_DelayedAct(() => { QueLevelsToLoad(initLevel.sequence_LI); }, 1f);
    }
    int currentLevelSequencePosition;
    List<int> dailyLevelIndexes;


    public bool isMassLevelQueingDone = false;
    public LevelDefinition lastAvailableLocalItem;
    public void QueLevelsToLoad(int currentLevelSequencePosition)
    {
        if (isMassLevelQueingDone) return;

        this.currentLevelSequencePosition = currentLevelSequencePosition;
        dailyLevelIndexes = new List<int>();
        foreach (var item in DailyLevelManagement.instance.definitions)
        {
            dailyLevelIndexes.Add(item.sequence_LI);
        }


        Dictionary<string, LevelDefinition> requests = new Dictionary<string, LevelDefinition>();

        for (int i = 0; i < LevelLoader.instance.levelAreas[0].levelDefinition.Count; i++)
        {
            LevelDefinition def = LevelLoader.instance.levelAreas[0].levelDefinition[i];

            if (!string.IsNullOrEmpty(def.remoteSubPath))
            {

                if (!requests.ContainsKey(def.remoteSubPath))
                {
                    requests.Add(def.remoteSubPath, def);
                }
                else
                {
                    float oldPriority = GetPriority(requests[def.remoteSubPath]);
                    float newPriority = GetPriority(def);
                    if (newPriority > oldPriority)
                    {
                        requests[def.remoteSubPath] = def;
                    }
                }
            }
            else 
            {
                lastAvailableLocalItem = def;
            }
        }

        foreach (KeyValuePair<string,LevelDefinition> item in requests)
        {
            RequestQueue(item.Value, GetPriority(item.Value));
        }
        isMassLevelQueingDone = true;
    }


    private float GetPriority(LevelDefinition def)
    {
        float posDiff = Mathf.Abs(def.sequence_LI - currentLevelSequencePosition);

        float priority = (1 / posDiff);

        if (dailyLevelIndexes.Contains(def.sequence_LI))
        {

            float offset = dailyLevelIndexes.IndexOf(def.sequence_LI)/ ((float) dailyLevelIndexes.Count-1);
            float min_priority = 1 /( 3+offset);
            if (priority < min_priority) priority = min_priority;
        }
        return priority;
    }
    #endregion


    #region fetch cycle
    private void RequestQueue(LevelDefinition def, float priority, System.Action onAvailable=null)
    {
        string subpath = def.remoteSubPath;

        if (string.IsNullOrEmpty(subpath))
        {
            onAvailable?.Invoke();
        }
        else if (packsInProcess.ContainsKey(subpath))
        {
            if (packsInProcess[subpath].IsReady)
            {
                onAvailable?.Invoke();
            }
            else
            {
                packsInProcess[subpath].onAvailable += onAvailable;
            }
        }
        else
        {
            packsInProcess.Add(subpath, new PackData(onAvailable));

            if (testing) requestText.text += string.Format("<color=#FFFF00>{0}_Priority:</color> {1}\n", subpath,Mathf.RoundToInt(priority*1000));

            DownloadQueueItem qItem = new DownloadQueueItem();
            qItem.callerLevelDefinition = def;
            qItem.priority = priority;

            packsInProcess[subpath].qitem = qItem;
            qItem.associatedPack = packsInProcess[subpath];
            for (int i = 0; i < queue.Count + 1; i++)
            {
                if (i == queue.Count) 
                {
                    queue.Add(qItem);
                    break;
                }
                else
                {
                    if (queue[i].priority < qItem.priority)
                    {
                        queue.Insert(i, qItem);
                        break;
                    }
                }
            }
        }

    }

    private Dictionary<string, PackData> packsInProcess = new Dictionary<string, PackData>();

    private List<DownloadQueueItem> queue = new List<DownloadQueueItem>();

    private IEnumerator Start()
    {
        while (true)
        {
            if (queue.Count > 0)
            {
                DownloadQueueItem qitem = queue[0];
                switch (qitem.associatedPack.Status)
                {
                    case PackDataStatus.PROCESSING:
                        break;
                    case PackDataStatus.FAILED:
                    case PackDataStatus.QUEUED:
                        {
                            int areaI_prefab = qitem.callerLevelDefinition.prefab_AI;
                            int levelI_Prefab = qitem.callerLevelDefinition.prefab_LI;
                            string variant = qitem.callerLevelDefinition.variant;
                            string subpath = qitem.callerLevelDefinition.remoteSubPath;

                            string address;
                            if (string.IsNullOrEmpty(variant))
                                address = string.Format("{0}/{1}/Level {2}.prefab", remoteBaseAddress, subpath, levelI_Prefab + 1);
                            else
                                address = string.Format("{0}/{1}/Level {2}_{3}.prefab", remoteBaseAddress, subpath, levelI_Prefab + 1, variant);

                            float requestAcceptTime = Time.realtimeSinceStartup;
                            packsInProcess[subpath].downloadStartTime = Time.realtimeSinceStartup;

                            AsyncOperationHandle asophandle = Addressables.LoadAssetsAsync<GameObject>(address, null);

                            packsInProcess[subpath].downloadHandle = asophandle;
                            packsInProcess[subpath].SetStatus(PackDataStatus.PROCESSING);

                            while (asophandle.IsValid() && !asophandle.IsDone)
                            {
                                if (testing) statusText.text = string.Format("Downloading {0}: {1}, called by level {2}", subpath, asophandle.PercentComplete, qitem.callerLevelDefinition.sequence_LI + 1);
                                yield return null;
                            }
                            if (testing) statusText.text = string.Format("\nWaiting for handle");
                            yield return asophandle;
                            if (testing) statusText.text = string.Format("\nHandle recovered");

                            if (asophandle.Status == AsyncOperationStatus.Succeeded)
                            {
                                packsInProcess[subpath].SetStatus(PackDataStatus.SUCCEEDED);

                                if (testing) requestText.text += string.Format("<color=#00FF00>{0}_Done:</color> {1} sec\n", subpath, Mathf.RoundToInt(
                                    Time.realtimeSinceStartup - packsInProcess[subpath].downloadStartTime));
                                if (testing) Debug.LogFormat("Completed Downloading {0}", subpath);

                                queue.Remove(qitem);
                            }
                            else
                            {
                                packsInProcess[subpath].SetStatus(PackDataStatus.FAILED);
                                packsInProcess[subpath].downloadStartTime = -2;
                                yield return new WaitForSeconds(1f);
                            }
                        }
                        break;
                    case PackDataStatus.SUCCEEDED:
                        {
                            queue.Remove(qitem);
                        }
                        break;
                }
            }
            yield return null;
        }
    }



    public class DownloadQueueItem
    {
        public float priority;
        public LevelDefinition callerLevelDefinition;
        public PackData associatedPack;
    }

    public class PackData
    {
        public DownloadQueueItem qitem;

        public float downloadStartTime;

        public AsyncOperationHandle downloadHandle;

        private PackDataStatus status = PackDataStatus.QUEUED;

        public PackDataStatus Status { get { return status; } }
        public event System.Action onAvailable = null;

        public PackData(System.Action onAvailable)
        {
            downloadStartTime = -1;
            this.onAvailable = onAvailable;
        }
        public void SetStatus(PackDataStatus newStatus)
        {
            if (IsReady) Debug.LogError("Data called ready more then once");
            else
            {
                status = newStatus;
                if (IsReady)
                {
                    onAvailable?.Invoke();
                    onAvailable = null;
                }
            }
        }

        public bool IsReady { get { return status == PackDataStatus.SUCCEEDED; } }
    }
    #endregion


}
public enum PackDataStatus
{
    QUEUED = 0,
    PROCESSING = 1,
    FAILED = 2,
    SUCCEEDED = 3,
}