﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Portbliss.Ad;
using Portbliss.DLC;
using UniBliss;
using Portbliss.IAP;
using System;

public class MainGameManager : MonoBehaviour
{
    public static QuickSettings settings;
    public static MainGameManager instance;
    public InLevelCanvasManager levelCanvas;
    //public GameObject loaderObjects;
    //public GameObject levelObjects;

    public AutoAreaLoader areaLoader;
    public QuickSettings settingsRef;

    public HardData<bool> ratingShown;
    

    // Start is called before the first frame update

    //public GameObject singularPlayButton;
    //public GameObject multiplePlayArray;
    public static float firstBootTime { get; private set; }

    void Awake()
    {
        Application.targetFrameRate = 300;
        instance = this;
        //levelObjects.SetActive(false);
        settings = settingsRef;
        firstBootTime = Time.time;
        //LevelPrefabManager.isItRestart = false;
        ratingShown = new HardData<bool> ( "RATING_ASKED", false );
        
    }

    private void Start()
    {
        HardData<bool> checkedFromIAP = new HardData<bool>("_IS_NO_AD_PURCHASED_CHECKED_", false);
        if (checkedFromIAP.value == false)
        {
            checkedFromIAP.value = true;
            if (IAP_Controller.instance != null)
            {
                IAP_Controller.instance.IsNoAdPurchased((success) =>
                {
                    AdController.hasNoAdIAP_PurchasedHD.value = success;
                });
            }
        }


        ChangeLevelObjectState(GameState.INITIAL);



        int ai = LevelLoader.Last_ai;
        int li = LevelLoader.Last_li;
        LevelDefinition levelDef = LevelLoader.FetchAppropriateDefinition(ai, li);
        if (LevelLoader.assistant.IsReadyToLoad(levelDef))
        {
            LevelLoader.LoadA_Level(levelDef, (GameObject resource) =>
            {
                if (resource) MainGameManager.instance.StartLevel(LoadType.NORMAL, ai, li, levelDef, resource);
            });
        }
        else
        {

            if (!LevelLoader.assistant.isMassLevelQueingDone)
            {
                LevelLoader.assistant.QueLevelsToLoad(LevelLoader.Last_li);
            }

            LevelLoader.LoadA_Level(LevelLoader.assistant.lastAvailableLocalItem, (GameObject resource) =>
            {
                if (resource) MainGameManager.instance.StartLevel(LoadType.NORMAL, ai, li, levelDef, resource);
            });
        }




    }





    public void DisableObjects(List<GameObject> gameObjects)
    {
        foreach (var item in gameObjects)
        {
            item.SetActive(false);
        }
    }
    public void EnableObjects(List<GameObject> gameObjects)
    {
        foreach (var item in gameObjects)
        {
            item.SetActive(true);
        }
    }

    public List<GameObject> inGameItems = new List<GameObject>();
    public List<GameObject> initialItems = new List<GameObject>();
    public List<GameObject> levelSelectItems = new List<GameObject>();
    public static GameState state;
    public void ChangeLevelObjectState (GameState state)
    {
        MainGameManager.state = state;
        //Debug.LogFormat("State set to {0}",state);
        switch (state)
        {
            case GameState.INITIAL:
                DisableObjects(inGameItems);
                DisableObjects(levelSelectItems);
                EnableObjects(initialItems);
                break;
            case GameState.GAME:
                DisableObjects(initialItems);
                DisableObjects(levelSelectItems);
                EnableObjects(inGameItems);
                break;
            case GameState.LEVELS:
                DisableObjects(initialItems);
                DisableObjects(inGameItems);
                EnableObjects(levelSelectItems );
                break;
            default:
                break;
        }

        //Debug.LogFormat("Doubt X: {0}",on);

    }

    public LevelPrefabManager runningLevelManager
    {
        get
        {
            return LevelPrefabManager.currentLevel;
        }
    }


    public static GameObject lastLoadedLevelResource;

    public void StartLevel(LoadType loadType, int areaIndex, int levelIndex, LevelDefinition levelDefinition, GameObject resourcePrefab)
    {
        LevelPrefabManager.levelStartedFramesToSkip = 3;
        AppLovinCrossPromo.Instance().HideMRec();
        lastLoadedLevelResource = resourcePrefab;
        DlcManager.OnLoadALevel();
        //Portbliss.Social.GifShareControl.EndRecord();
        //Portbliss.Social.GifShareControl.BeginRecord();

        int cumlevelnum = LevelDefinition.Query_CumulativeNumber(areaIndex, levelIndex,loadType);

        Instantiate(LevelLoader.instance.commonPrefab, Vector3.zero, Quaternion.identity).GetComponent<LevelPrefabManager>().LoadAs(loadType,areaIndex, levelIndex, levelDefinition, resourcePrefab, null);

        ShowStartPrompts(loadType,cumlevelnum);

        if ( ConfettiManager.instance )
        {
            ConfettiManager.instance.ClearParticles ();
        }

#if (UNITY_ANDROID)
        if (cumlevelnum == 3 && !ratingShown.value)
        {
            Debug.Log("===============================");
            levelCanvas.LoadRatingWindow();
            ratingShown.value = true;
        }
#endif
    }

    private void ShowStartPrompts(LoadType loadType, int cumlevelnum)
    {
        if (loadType == LoadType.NORMAL)
            TryReviewRequestWindow_IOS(cumlevelnum);
        if (loadType == LoadType.DAILY)
            TryPushRequest(cumlevelnum);
    }

    void TryReviewRequestWindow_IOS(int lvNum, string debugMsg = "")
    {
#if (UNITY_IOS)
                if (lvNum == 3 && !ratingShown.value)
                {
                    Debug.Log("<color='magenta'>"+debugMsg+"</color>");
                    UnityEngine.iOS.Device.RequestStoreReview();
                    ratingShown.value = true;
                }
#endif
    }

    void TryPushRequest (int cumulativeIndex)
    {
        if ((cumulativeIndex ==0 || cumulativeIndex == 4) && !DailyLevelManagement.instance.pushpermision.value)
        {
            levelCanvas.LoadPushWindow ();
        }
    }

    public void LoadLevelFromSelection(LoadType loadType, int areaI, int levelI, LevelDefinition definition, GameObject resource)
    {
        if (runningLevelManager) runningLevelManager.UnLoad();
        StartLevel(loadType, areaI, levelI, definition, resource);
        ChangeLevelObjectState(GameState.GAME);
    }

}
public enum GameState
{
    INITIAL,
    GAME,
    LEVELS,
    LEVELS_DAILY,
}
public enum LoadType
{
    NORMAL,
    DAILY,
}