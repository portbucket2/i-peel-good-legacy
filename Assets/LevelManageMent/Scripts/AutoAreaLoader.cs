﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AutoAreaLoader : MonoBehaviour
{
    public int areaIndex;

    [Header("Main")]
    public AreaLoader mainLoaderRef;
    public ScrollRect mainScrollRect;
    public GridLayoutGroup mainContentGroup;

    [Header("Daily")]
    public DailyLoader dailyLoaderRef;
    public ScrollRect dailyScrollRect;
    public GridLayoutGroup dailyContentGroup;

    [Header("Nav")]
    public Button prevAreaButton;
    public Button nextAReaButton;
    public Button closeButton;
    //public Button resetButton;
    [Header("TabControl")]
    public GameObject mainSection;
    public GameObject dailySection;
    public Button mainTabSelectButton;
    public Button dailyTabSelectButton;

    public int colCount = 3;
    void Start()
    {
        closeButton.onClick.RemoveAllListeners();
        closeButton.onClick.AddListener(() => {
            MainGameManager.instance.ChangeLevelObjectState(GameState.GAME);
        });

        prevAreaButton.onClick.RemoveAllListeners();
        prevAreaButton.onClick.AddListener(OnPrev);

        nextAReaButton.onClick.RemoveAllListeners();
        nextAReaButton.onClick.AddListener(OnNext);

        RestartLoader(LoadType.NORMAL);
        RestartLoader(LoadType.DAILY);

        mainTabSelectButton.onClick.RemoveAllListeners();
        mainTabSelectButton.onClick.AddListener(()=>SelectTab(LoadType.NORMAL));

        dailyTabSelectButton.onClick.RemoveAllListeners();
        dailyTabSelectButton.onClick.AddListener(() => SelectTab(LoadType.DAILY));
    }


    void SelectTab(LoadType loadType)
    {
        mainSection.SetActive(loadType == LoadType.NORMAL);
        dailySection.SetActive(loadType == LoadType.DAILY);
        mainTabSelectButton.interactable = loadType == LoadType.DAILY;
        dailyTabSelectButton.interactable = loadType == LoadType.NORMAL;
        RestartLoader(loadType);
    }

    public void LoadLevelList(LoadType loadType, int focusIndex = -1)
    {
        RestartLoader(loadType,focusIndex);
        SelectTab(loadType);
        MainGameManager.instance.ChangeLevelObjectState(GameState.LEVELS);
    }
    public void RestartLoader(LoadType loadType, int focusIndex = -1)
    {
        areaIndex = LevelLoader.Last_ai;
        RefreshLoader(loadType);
        if (focusIndex < 0) chosenFocusIndex = LevelLoader.Last_li;
        else chosenFocusIndex = focusIndex;

        UniBliss.Centralizer.Add_DelayedMonoAct(this, ()=> { FocusNow(loadType); }, 0, true);
    }
    int chosenFocusIndex = -1;
    private void FocusNow(LoadType loadType)
    {

        if (chosenFocusIndex>=0)
        {
            GridLayoutGroup contentGroup = null;
            ScrollRect scrollRect = null;


            switch (loadType)
            {
                case LoadType.NORMAL:
                    {
                        contentGroup = mainContentGroup;
                        scrollRect = mainScrollRect;
                    }
                    break;
                case LoadType.DAILY:
                    {
                        contentGroup = dailyContentGroup;
                        scrollRect = dailyScrollRect;
                    }
                    break;
            }



            //int N = Mathf.CeilToInt( mainLoaderRef.ItemCount /(float) colCount);
            float h = contentGroup.cellSize.y;
            float hs = contentGroup.spacing.y;
            float ptop = contentGroup.padding.top;
            float pbot = contentGroup.padding.top;
            //Debug.LogFormat("Total {0} ,  actual {1}", N*h + (N-1)*hs + ptop + pbot , );
            int Rn = Mathf.CeilToInt(chosenFocusIndex / (float)colCount);

            float Vh = (scrollRect.transform as RectTransform).rect.height;
            float Ch = (contentGroup.transform as RectTransform).rect.height;

            float minFocPixDepth = Vh / 2;
            float maxFocPixDepth = Ch- (Vh/2);

            float focPixDepth = (ptop+ (Rn-1)*(h+hs) + (h/2));
            float clampedFocPixDepth = Mathf.Clamp(focPixDepth,minFocPixDepth,maxFocPixDepth);

            float fractionVal = 1-(clampedFocPixDepth - minFocPixDepth) / (maxFocPixDepth - minFocPixDepth);


            //Debug.LogFormat("R{0}-Target Pixel Depth: {1}({2}-{3}), f-{4}", Rn, focPixDepth, minFocPixDepth,maxFocPixDepth,fractionVal);
            //Debug.LogFormat("ViewPort height: {0}, Content Height{1}", Vh, Ch);

           
            scrollRect.verticalNormalizedPosition = fractionVal;

            chosenFocusIndex = -1;
        }
    }

    private void OnNext()
    {
        areaIndex++;
        RefreshLoader(LoadType.NORMAL);
    }
    private void OnPrev()
    {
        areaIndex--;
        RefreshLoader(LoadType.NORMAL);
    }
    private void RefreshLoader(LoadType loadType)
    {
        switch (loadType)
        {
            case LoadType.NORMAL:
                {
                    mainLoaderRef.Load(areaIndex);
                    if (areaIndex - 1 >= 0)
                    {

                        prevAreaButton.gameObject.SetActive(true);
                        prevAreaButton.interactable = LevelLoader.instance.IsAreaUnlocked(areaIndex - 1);
                    }
                    else
                    {
                        prevAreaButton.gameObject.SetActive(false);
                    }

                    if (areaIndex + 1 < LevelLoader.instance.levelAreas.Count)
                    {
                        nextAReaButton.gameObject.SetActive(true);
                        nextAReaButton.interactable = LevelLoader.instance.IsAreaUnlocked(areaIndex + 1);
                    }
                    else
                    {
                        nextAReaButton.gameObject.SetActive(false);
                    }
                }
                break;
            case LoadType.DAILY:
                {
                    dailyLoaderRef.Load();
                }
                break;
        }
       

    }

}
