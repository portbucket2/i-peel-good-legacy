﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using UniBliss;

public class DailyLoader : MonoBehaviour
{
    public int dummyAreaIndexValue =0;

    //public Text areaTitle;
    public GameObject levelUIPrefab;
    public RectTransform contentPanel;
    public Text coolDownInfoText;

    public event System.Action onRefresh;

    public bool centreObjectIfNecessaryFor3ItemColumn = true;

    private List<LevelUIItemLoader> selectionItems = new List<LevelUIItemLoader>();

    public LevelUIItemLoader GetClosestUIItem(int levelIndex)
    {
        if (levelIndex >= selectionItems.Count) return selectionItems[selectionItems.Count - 1];
        else return selectionItems[levelIndex];
    }
    public int ItemCount { get { return selectionItems.Count; } }

    LevelDefinition[] levelDefinitions;
    public void Load()
    {
        Debug.Log(DailyLevelManagement.instance);
        levelDefinitions= DailyLevelManagement.instance.GetDailyLevels();

        //areaTitle.text = string.Format("Area {0}",dummyAreaIndexValue+1);


        for (int i = selectionItems.Count-1; i >=0 ; i--)
        {
            Pool.Destroy(selectionItems[i].gameObject);
            selectionItems.RemoveAt(i);
        }
        int N = levelDefinitions.Length;
        int m = N % 3;
        bool addOne = centreObjectIfNecessaryFor3ItemColumn && m == 1;
        //Debug.Log(addOne);

        for (int i = 0; i < N + (addOne?1:0); i++)
        {
            Transform tr = Pool.Instantiate(levelUIPrefab).transform;
            tr.SetParent(contentPanel);
            tr.localScale = Vector3.one;
            tr.rotation = Quaternion.identity;

            selectionItems.Add(tr.GetComponent<LevelUIItemLoader>());
        }
        Refresh(addOne);

        Centralizer.Add_Update(this, () => {
            coolDownInfoText.text = string.Format("Daily levels will be refreshed in {0} seconds",DailyLevelManagement.instance.RefreshTimeLeft());
        });
    }

    public void Refresh(bool hasExtra)
    {
        onRefresh?.Invoke();





        if (hasExtra)

        {
            //Debug.Log("B");
            for (int i = 0; i < selectionItems.Count; i++)
            {
                if (i == selectionItems.Count - 2)
                {
                    selectionItems[i].LoadAsDummy();
                }
                else if (i == selectionItems.Count - 1)
                {
                    LoadItem(i, i - 1);
                }
                else
                {
                    LoadItem(i, i);
                }
            }
        }
        else
        {
            //Debug.Log("A");
            for (int i = 0; i < selectionItems.Count; i++)
            {
                LoadItem(i, i);
            }
        }
    }
    void LoadItem(int itemIndex,int levelIndex)
    {
        selectionItems[itemIndex].LoadDaily(
            levelIndex:levelIndex, 
            levelDef: levelDefinitions[levelIndex],
            isUnlocked: DailyLevelManagement.instance.IsIndexUnLocked(levelIndex),
            starEarned: DailyLevelManagement.instance.starsHD[levelIndex]
            );
    }
}