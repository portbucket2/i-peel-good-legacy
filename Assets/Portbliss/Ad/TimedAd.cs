﻿using System;
using System.Collections;
using System.Collections.Generic;
using UniBliss;
using UnityEngine;

namespace Portbliss.Ad
{
    public class TimedAd
    {
        public static TimedAd instance;
        public float lastTime = 0;
        //public const float interval = 10;

        static void AdCore(Action<bool> OnComplete)
        {
            instance.lastTime = Time.time;
            IdleChecker.instance.allowPeriodicHand = false;
            AdController.ShowInterstitialAd((success) =>
            {
                instance.lastTime = Time.time;
                IdleChecker.instance.resetEverything();
                AdController.adWatchCoutHD.value = AdController.adWatchCoutHD.value + 1;
                if (AdController.adWatchCoutHD.value == 10 && AdController.tenTimesAdShownHD.value == false)
                {
                    AnalyticsAssistant.AdWatched10Times();
                    Debug.Log("<color='magenta'>ad watched 10 times.</color>");
                    AdController.tenTimesAdShownHD.value = true;
                }
                else if (AdController.adWatchCoutHD.value == 20 && AdController.twentyTimesAdShownHD.value == false)
                {
                    AnalyticsAssistant.AdWatched20Times();
                    Debug.Log("<color='magenta'>ad watched 20 times.</color>");
                    AdController.twentyTimesAdShownHD.value = true;
                }
                OnComplete?.Invoke(success);
            });
        }

        public static void InitTimedAd()
        {
            if (instance == null)
            {
                instance = new TimedAd();
                instance.lastTime = 0;
            }
        }

        public static void AdIteration(Action<bool> OnComplete, bool isRestart)
        {
            //Debug.LogWarning("we called ad iteration.");
            if (instance == null)
            {
                InitTimedAd();
            }

            bool willPlayAd = false;
            int lvNumAfter = ABManager.GetValue(ABtype.First_Ad);
            if (AdController.AdMode == AdIntensityType.Normal)
            {
                if (Time.time > instance.lastTime + 60 && LevelPrefabManager.TotalCompletedLevelCount > lvNumAfter
                    && !isRestart && Mathf.Abs(MainGameManager.firstBootTime - Time.time) > 60)
                {
                    willPlayAd = true;
                }
            }
            else if (AdController.AdMode == AdIntensityType.Medium)
            {
                if (Time.time > instance.lastTime + 30 && LevelPrefabManager.TotalCompletedLevelCount > lvNumAfter
                    && Mathf.Abs(MainGameManager.firstBootTime - Time.time) > 60)
                {
                    willPlayAd = true;
                }
            }
            else if (AdController.AdMode == AdIntensityType.Aggresive)
            {
                if (Time.time > instance.lastTime + 0 && LevelPrefabManager.TotalCompletedLevelCount > lvNumAfter
                    && Mathf.Abs(MainGameManager.firstBootTime - Time.time) > 0)
                {
                    willPlayAd = true;
                }
            }

            if (willPlayAd)
            {
                AdCore(OnComplete);
            }
            else
            {
                OnComplete?.Invoke(false); 
            }
        }
    }
}
