﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UniBliss;

namespace Portbliss.Ad
{
    public delegate void OnCompleteAdFuc(bool success);
    public enum AdIntensityType { Normal, Medium, Aggresive, Invalid }
    public partial class AdController : MonoBehaviour
    {
        const bool logEnabled = false;
        public static AdController instance;
        const string sdkKey = "Lzi5VR_J50y55PM5ctwAwALT5d9g1CKMhT1TF0naOa4fSUn98Vd6rXsvAp4I3A-5LaPvNk4RSvKe5fesxKhRzh";
        static bool isSDKready = false;
        public static bool IsSDK_Ready { get { return isSDKready; } }
        public static bool AllowSkipFromMax = true;
#if UNITY_ANDROID
        const string interstitialAdUnitId = "b5545cb34adb7356";
#elif UNITY_IOS
        const string interstitialAdUnitId = "afd11738cad7d2b3";
#else
        const string interstitialAdUnitId = "b5545cb34adb7356";
#endif

#if UNITY_ANDROID
        const string rewardedAdUnitId = "360030fe306658a6";
#elif UNITY_IOS
        const string rewardedAdUnitId = "dc378fb233d1836d";
#else
        const string rewardedAdUnitId = "360030fe306658a6";
#endif

#if UNITY_ANDROID
        const string bannerAdUnitId = "2b4531cc16983291"; // Retrieve the id from your account
#elif UNITY_IOS
        const string bannerAdUnitId = "64085c599c3513f0"; // Retrieve the id from your account
#else
        const string bannerAdUnitId = "2b4531cc16983291"; // Retrieve the id from your account
#endif
        [SerializeField] Color bannerColor = Color.white;
        [SerializeField] bool useInterstitial = true, useRewardedVideoAd = false, useBanner = true;
        [SerializeField] MaxSdk.BannerPosition bannerPosition = MaxSdkBase.BannerPosition.BottomCenter;
        [SerializeField] bool testModeShowMediationDebugger = false;
        [SerializeField] bool ForceLocationEU_Test = false;
        [SerializeField] GDPRController GDPR_Script;
        public static AdIntensityType AdMode { get; private set; }
        OnCompleteAdFuc intersitialDel, rewardedDel;
        static bool hasShownVideoAd = false, hasDismissedVideoAd = false;
        static bool hasShownInterstitial = false, hasDismissedIntersitial = false;
        static HardData<bool> consentTaskDoneFlagHD;
        public static HardData<bool> isEUCountry { get; private set; }
        static bool isShowingBanner = false;
        public static HardData<bool> hasNoAdIAP_PurchasedHD { get; private set; }
        public static HardData<int> adWatchCoutHD { get; private set; }
        public static HardData<bool> tenTimesAdShownHD { get; private set; }
        public static HardData<bool> twentyTimesAdShownHD { get; private set; }
        public static bool gdpr_done
        {
            get; private set;
        }
        
        private void Awake()
        {
            if (instance == null)
            {
                instance = this;
                StartAdSystem ();
                DontDestroyOnLoad(this);
            }
            else
            {
                if (gameObject != null)
                {
                    DestroyImmediate(gameObject);
                }
                else
                {
                    DestroyImmediate(this);
                }
            }
        }

        public static void CompleteConsentTask(bool consentGiven)
        {
            MaxSdk.SetHasUserConsent(consentGiven);
            consentTaskDoneFlagHD.value = true;
            gdpr_done = true;
        }

        private void OnDisable ()
        {
            GDPR_Script.OnGDPR_UI_Completion -= CompleteConsentTask;
        }

        string GetEmulatedVariableValueForTesting(string varName)
        {
            return "18";
        }


        public static bool non_ios_14_5plus = true;
        void StartAdSystem()
        {
            isEUCountry = new HardData<bool>("_IS_EU_COUNTRIES_SD", false);
            hasNoAdIAP_PurchasedHD = new HardData<bool>("_NO_AD_IAP_PURCHASED", false);
            consentTaskDoneFlagHD = new HardData<bool>("CONSENT_TASK_DONE_FLAG", false);
            adWatchCoutHD = new HardData<int>("INTERSTITIAL_AD_SHOW_COUNT", 0);
            tenTimesAdShownHD = new HardData<bool>("INTERSITTIAL_AD_SHOWN_TEN_TIMES", false);
            twentyTimesAdShownHD = new HardData<bool>("INTERSITTIAL_AD_SHOWN_TWENTY_TIMES", false);
#if UNITY_IOS || UNITY_ANDROID
            gdpr_done = false;
            GDPR_Script.OnGDPR_UI_Completion += CompleteConsentTask;
            isShowingBanner = false;
            AdMode = AdIntensityType.Aggresive;
            isSDKready = false;
            MaxSdk.SetSdkKey(sdkKey);
            MaxSdk.InitializeSdk();
            TimedAd.InitTimedAd();

            
            //
            MaxSdkCallbacks.OnSdkInitializedEvent += (MaxSdkBase.SdkConfiguration sdkConfiguration) =>
            {
#if UNITY_IOS || UNITY_IPHONE || UNITY_EDITOR
                if (MaxSdkUtils.CompareVersions(UnityEngine.iOS.Device.systemVersion, "14.5") != MaxSdkUtils.VersionComparisonResult.Lesser)
                {
                    non_ios_14_5plus = false;
                    AudienceNetwork.AdSettings.SetAdvertiserTrackingEnabled(true);
                    Debug.LogFormat("<color='magenta'>ios 14.5+ detected!! Initiate WTF protocol</color>");
                }
                else
                {
                    Debug.LogFormat("<color='magenta'>normal mode running_ not ios 14.5+</color>");
                }
#endif



                InitializeAndPreloadSelectedAdTypes();
                // AppLovin SDK is initialized, start loading ads
                isSDKready = true;
                
                if ( consentTaskDoneFlagHD.value == false )
                {
                    if ( (sdkConfiguration.ConsentDialogState == MaxSdkBase.ConsentDialogState.Applies || ForceLocationEU_Test) && non_ios_14_5plus )
                    {
                        isEUCountry.value = true;
                        if (GDPR_Script == null)
                        {
                            consentTaskDoneFlagHD.value = true;
                            gdpr_done = true;
                        }
                        else
                        {
                            // Show user consent dialog
                            GDPR_Script.StartGDPRFromWelcome();
                        }
                    }
                    else if ( sdkConfiguration.ConsentDialogState == MaxSdkBase.ConsentDialogState.DoesNotApply )
                    {
                        isEUCountry.value = false;
                        //consentIsRequired = false;
                        // No need to show consent dialog, proceed with initialization
                        consentTaskDoneFlagHD.value = true;
                        gdpr_done = true;
                    }
                    else
                    {
                        // Consent dialog state is unknown. Proceed with initialization, but check if the consent
                        // dialog should be shown on the next application initialization
                        consentTaskDoneFlagHD.value = false;
                        gdpr_done = true;
                    }
                }
                else
                {
                    gdpr_done = true;
                }
                
                
                var adIntensityVar = MaxSdk.VariableService.GetString("peel_ad_intensity_type");
                foreach (var s in ABManager.allSettings)
                {
                    string varName = s.Value.GetID();
                    string varValue = MaxSdk.VariableService.GetString(varName);
                    Debug.Log("<color='magenta'>variable name: " + varName + " and variable value: " + varValue + "</color>");
                    int result = -1;
                    int.TryParse(varValue, out result);
                    s.Value.Assign_IfUnassigned(result);
                }

                //Debug.Log("<color='green'>the variable type from max sdk is:" + variableValue + "</color>");
                //var SkipVariableValue = MaxSdk.VariableService.GetString("Allow_Skip");

                //if(SkipVariableValue== "1")
                //{
                //    AllowSkipFromMax = true;
                //    //InLevelCanvasManager.instance.AllowSkip = true;
                //    FacebookManager.LogSkipMode(SkipVariableValue);
                //    Debug.Log("_______ALLOWING SKIP!!!!!");
                //}

                //if(SkipVariableValue=="0")
                //{
                //    AllowSkipFromMax = false;
                //    //InLevelCanvasManager.instance.AllowSkip = false;
                //    FacebookManager.LogSkipMode(SkipVariableValue);
                //    Debug.Log("_______NOT ALLOWING SKIP!!!!!");
                //}

                //ev peel_ad_intensity_type ,,,,value 
                if (adIntensityVar == "1")
                {
                    AdMode = AdIntensityType.Normal;
                    FacebookManager.LogAdMode("1");
                }
                else if (adIntensityVar == "2")
                {
                    AdMode = AdIntensityType.Medium;
                    FacebookManager.LogAdMode("2");
                }
                else if (adIntensityVar == "3")
                {
                    AdMode = AdIntensityType.Aggresive;
                    FacebookManager.LogAdMode("3");
                }
                else
                {
                    AdMode = AdIntensityType.Aggresive;
                    FacebookManager.LogAdMode("3");
                }

                if (testModeShowMediationDebugger)
                {
                    MaxSdk.ShowMediationDebugger();
                }
            };
#endif
        }

        void InitializeAndPreloadSelectedAdTypes()
        {
#if UNITY_IOS || UNITY_ANDROID
            if (useInterstitial)
            {
                InitializeInterstitialAds();
            }

            if (useRewardedVideoAd)
            {
                InitializeRewardedAds();
            }

            if (useBanner)
            {
                InitializeBannerAds();
            }
#endif
        }

        public static void ShowInterstitialAd(OnCompleteAdFuc OnComplete)
        {
#if UNITY_IOS || UNITY_ANDROID
            hasShownInterstitial = hasDismissedIntersitial = false;
            if (MaxSdk.IsInterstitialReady(interstitialAdUnitId) && isSDKready)
            {
                MaxSdk.ShowInterstitial(interstitialAdUnitId);
                instance.intersitialDel = OnComplete;
            }
            else
            {
                instance.intersitialDel = null;
                OnComplete?.Invoke(false);
            }
#else
            OnComplete?.Invoke(true);
#endif
        }

        public static void ShowRewardedVideoAd(OnCompleteAdFuc OnComplete)
        {
#if UNITY_IOS || UNITY_ANDROID
            hasShownVideoAd = hasDismissedVideoAd = false;
            if (MaxSdk.IsRewardedAdReady(rewardedAdUnitId) && isSDKready)
            {
                MaxSdk.ShowRewardedAd(rewardedAdUnitId);
                instance.rewardedDel = OnComplete;
            }
            else
            {
                instance.rewardedDel = null;
                OnComplete?.Invoke(false);
            }
#else
            OnComplete?.Invoke(true);
#endif
        }

        public static void ShowBanner()
        {
#if UNITY_IOS || UNITY_ANDROID
            if (isSDKready && isShowingBanner == false)
            {
                isShowingBanner = true;
                MaxSdk.ShowBanner(bannerAdUnitId);
            }
#endif
        }

        public static void HideBanner()
        {
#if UNITY_IOS || UNITY_ANDROID
            if (isSDKready && isShowingBanner == true)
            {
                isShowingBanner = false;
                MaxSdk.HideBanner(bannerAdUnitId);
            }
#endif
        }
    }
}