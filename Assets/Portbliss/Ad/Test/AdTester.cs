﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Portbliss.Ad
{
    public class AdTester : MonoBehaviour
    {
        [SerializeField] Button showBanner, hideBanner, showInterstitial, showVideoad, clearLog;
        [SerializeField] Text logText;
        // Start is called before the first frame update
        void Start()
        {
            showBanner.onClick.RemoveAllListeners();
            showBanner.onClick.AddListener(() =>
            {
                AdController.ShowBanner();
            });

            hideBanner.onClick.RemoveAllListeners();
            hideBanner.onClick.AddListener(() =>
            {
                AdController.HideBanner();
            });

            showInterstitial.onClick.RemoveAllListeners();
            showInterstitial.onClick.AddListener(() =>
            {
                AdController.ShowInterstitialAd((success) =>
                {
                    LogIt("Interstitial has been shown? " + success);
                });
            });

            showVideoad.onClick.RemoveAllListeners();
            showVideoad.onClick.AddListener(() =>
            {
                AdController.ShowRewardedVideoAd((success) =>
                {
                    LogIt("rewarded video has been shown? " + success);
                });
            });

            clearLog.onClick.RemoveAllListeners();
            clearLog.onClick.AddListener(() =>
            {
                logText.text = "";
            });
        }

        void LogIt(string log)
        {
            logText.text += System.Environment.NewLine + log;
        }

    }
}