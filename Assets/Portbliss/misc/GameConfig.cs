﻿using System.Collections;
using System.Collections.Generic;
using UniBliss;
using UnityEngine;

public class GameConfig : MonoBehaviour
{
    static GameConfig instance;
    public static HardData<bool> hasIAP_NoAdPurchasedHD { get; private set; }
    public static HardData<int> adWatchCountHD { get; private set; }
    public static HardData<bool> IAP_PurchasedOrNot_HasBeenCheckedHD { get; private set; }
    public static HardData<bool> hasDoneConsentRelatedTasksHD { get; private set; }
    public static HardData<bool> hasIAP_Restored_iOS { get; private set; }
    //public static OneTimeEventConfig levelCompletionAppsflyerOneTimeHD { get; private set; }
    //public static OneTimeEventConfig adWatchNTimesCompletedAppsflyerHD { get; private set; }
    public static bool hasRewardedVideoAdBeenShown { get; set; }
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            InitSystem();
            DontDestroyOnLoad(this);
        }
        else
        {
            if (instance.gameObject != gameObject)
            {
                DestroyImmediate(this);
            }
        }
    }
    
    void InitSystem()
    {
        hasIAP_NoAdPurchasedHD = new HardData<bool>("_NO_AD_IAP_PURCHASED", false);
        adWatchCountHD = new HardData<int>("INTERSTITIAL_AD_SHOW_COUNT", 0);
        IAP_PurchasedOrNot_HasBeenCheckedHD = new HardData<bool>("_IS_NO_AD_PURCHASED_CHECKED_", false);
        hasDoneConsentRelatedTasksHD = new HardData<bool>("CONSENT_TASK_DONE_FLAG", false);
        hasIAP_Restored_iOS = new HardData<bool>("_APP_IAP_RESTORE_PURCHASE_", false);
        //levelCompletionAppsflyerOneTimeHD = new OneTimeEventConfig(new List<int> { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100 },
        //    "LEVEL_COMPLETED_ONE_TIME_EVENT_FIRED_");
        //adWatchNTimesCompletedAppsflyerHD = new OneTimeEventConfig(new List<int> { 10, 20 }, "ads_watched_HD_");
        hasRewardedVideoAdBeenShown = false;
    }
}
