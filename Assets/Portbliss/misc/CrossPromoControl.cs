﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Portbliss.Ad;
using UnityEngine.UI;
using UniBliss;
using Portbliss.IAP;

public class CrossPromoControl : MonoBehaviour
{
    // Start is called before the first frame update

    // Top-left X position expressed as a percentage of the screen width.
    const float xPercent = 27.5f;

    // Top-left Y position expressed as a percentage of the screen height
    const float yPercent = 64f;

    // Width expressed as a percentage of the minimum value between screen width and height
    const float wPercent = 45.0f;

    // Height expressed as a percentage of the minimum value between screen width and height
    const float hPercent = 45.0f;

    // Clock-wise rotation expressed in degrees
    const float rDegrees = 0.0f;

    [SerializeField] GameObject bannerBackground;
    private void Awake()
    {
        AppLovinCrossPromo.Init();
        var hd = new HardData<bool>("_NO_AD_IAP_PURCHASED", false);
        bannerBackground.SetActive(!hd.value);
    }

    private void Start()
    {
        if ( !( LevelLoader.Last_li == 0 && LevelLoader.Last_ai == 0 ) )
        {
            Invoke ( "ShowPromo", 0.1f );
        }
        StartCoroutine (CheckForSDKReadyAndShowBannerAd());
        StartCoroutine(CheckForNoAdIAP());
    }

    IEnumerator CheckForNoAdIAP()
    {
        while (IAP_Controller.hasAdPurchased == false)
        {
            yield return null;
        }
        AdController.HideBanner();
        bannerBackground.SetActive(false);
    }

    void ShowPromo ()
    {
        AppLovinCrossPromo.Instance ().ShowMRec ( xPercent, yPercent, wPercent, hPercent, rDegrees );
    }

    IEnumerator CheckForSDKReadyAndShowBannerAd()
    {
        while(true)
        {
            if(AdController.IsSDK_Ready 
                && UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex == 1 
                && AdController.gdpr_done)
            {
                break; 
            }
            yield return null;
        }

        if (AdController.hasNoAdIAP_PurchasedHD.value == false)
        {
            AdController.ShowBanner();
        }
    }
}
