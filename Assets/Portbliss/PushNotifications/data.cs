﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Portbliss.Push
{
    public class NotificationData
    {
        public string id, extraData, title, description;
    }
}