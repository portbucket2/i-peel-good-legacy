﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Portbliss.Push
{
    public class PushTester : MonoBehaviour
    {
        [SerializeField] Button scheduleNotificationBtn, requestPermission_IOS_Btn, queryPermissionStatusBtn, appOpenDataQueryBtn;
        [SerializeField] int hoursLater, minuteLater, secondsLater;

        // Start is called before the first frame update
        void Start()
        {
            scheduleNotificationBtn.onClick.RemoveAllListeners();
            scheduleNotificationBtn.onClick.AddListener(() =>
            {
                PushController.ScheduleNotification("How You Peeling Today?", "", "", hoursLater, minuteLater, secondsLater, "magic550");
            });

            requestPermission_IOS_Btn.onClick.RemoveAllListeners();
            requestPermission_IOS_Btn.onClick.AddListener(() =>
            {
                PushController.RequestNotificationPermission_OnlyOnce((permissionGranted) =>
                {
                    Debug.Log("<color='blue'>permission has been granted? " + permissionGranted + "</color>");
                });
            });
            
            queryPermissionStatusBtn.onClick.RemoveAllListeners();
            queryPermissionStatusBtn.onClick.AddListener(() =>
            {
                Debug.Log("<color='blue'>is notification authorized? " + PushController.IsNotificationAuthorized() + "</color>");
            });

            appOpenDataQueryBtn.onClick.RemoveAllListeners();
            appOpenDataQueryBtn.onClick.AddListener(() =>
            {
                if (PushController.appOpenedWithThisData == null)
                {
                    Debug.Log("<color='blue'>app opened normally, so no need to query the appopen data!</color>");
                }
                else
                {
                    Debug.Log("<color='blue'>app open data is not null. so printing... ID: "
                    + PushController.appOpenedWithThisData.id
                    + " and title: " + PushController.appOpenedWithThisData.title
                    + " and description: " + PushController.appOpenedWithThisData.description
                    + " and extra data: " + PushController.appOpenedWithThisData.extraData + "</color>");
                }
            });
        }
    }
}