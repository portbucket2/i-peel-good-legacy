﻿using System;
using System.Collections;
using System.Collections.Generic;
#if UNITY_ANDROID
using Unity.Notifications.Android;
#elif UNITY_IOS
using Unity.Notifications.iOS;
#endif
using UnityEngine;

namespace Portbliss.Push
{
    public class PushController : MonoBehaviour
    {
        static PushController instance;
        const string channelID = "ch_id";
        const string channelName = "i Peel Good";
        const string channelDesc = "I peel good, Do you peel good?";
        const string smallIconName = "small_icon";
        const string bigIconName = "big_icon";
        const string iOS_Identifier = "iPeelGood_ID_IOS";
        const string iOS_CategoryIdentifier = "iPeelGood_Cat_ID_IOS";
        const string iOS_ThreadIdentifier = "iPeelGood_Thread_ID_IOS";
        public static NotificationData appOpenedWithThisData;
        void Awake()
        {
            if (instance == null)
            {
                instance = this;
                InitSystem();
                DontDestroyOnLoad(this);
            }
            else
            {
                if (instance.gameObject != gameObject)
                {
                    DestroyImmediate(this);
                    return;
                }
            }
        }

        void InitSystem()
        {
            appOpenedWithThisData = null;

#if UNITY_ANDROID
            var notifChannel = new AndroidNotificationChannel()
            {
                Id = channelID,
                Name = channelName,
                Importance = Importance.High,
                Description = channelDesc,
            };
            AndroidNotificationCenter.RegisterNotificationChannel(notifChannel);
            var android_data = AndroidNotificationCenter.GetLastNotificationIntent();
            if (android_data != null)
            {
                appOpenedWithThisData = new NotificationData
                {
                    id = android_data.Id + "",
                    description = android_data.Notification.Text,
                    title = android_data.Notification.Title,
                    extraData = android_data.Notification.IntentData
                };
                Debug.Log("<color='blue'>android app opened with notification! Printing...." +
                    " id:" + appOpenedWithThisData.id + " description: " + appOpenedWithThisData.description +
                    " title: " + appOpenedWithThisData.title + " extra data: " + appOpenedWithThisData.extraData + "</color>");
            }
            else
            {
                Debug.Log("<color='blue'>android app was not opened with notification!</color>");
            }

#elif UNITY_IOS
            var ios_data = iOSNotificationCenter.GetLastRespondedNotification();
            if (ios_data != null)
            {
                appOpenedWithThisData = new NotificationData
                {
                    id = ios_data.Identifier,
                    description = ios_data.Body,
                    title = ios_data.Title,
                    extraData = ios_data.Data
                };
                Debug.Log("<color='blue'>ios app opened with notification! Printing...." +
                    " id:" + appOpenedWithThisData.id + " description: " + appOpenedWithThisData.description +
                    " title: " + appOpenedWithThisData.title + " extra data: " + appOpenedWithThisData.extraData + "</color>");
            }
            else
            {
                Debug.Log("<color='blue'>ios app was not opened with notification!</color>");
            }
#endif
        }

        public static void ScheduleNotification(string title, string subtitleForiOS, string description,
            int hoursLater, int minutesLater, int secondsLater, string extraData = "")
        {
#if UNITY_ANDROID
            AndroidNotificationCenter.CancelAllNotifications();
            var androidNotif = new AndroidNotification();
            androidNotif.Title = title;
            androidNotif.Text = description;
            DateTime timeData = DateTime.Now.AddHours(hoursLater);
            timeData = timeData.AddMinutes(minutesLater);
            timeData = timeData.AddSeconds(secondsLater);
            androidNotif.FireTime = timeData;
            androidNotif.SmallIcon = smallIconName;
            androidNotif.LargeIcon = bigIconName;
            androidNotif.IntentData = extraData;
            AndroidNotificationCenter.SendNotification(androidNotif, channelID);
            Debug.Log("<color='blue'>notificatio scheduled for android app</color>");
#elif UNITY_IOS
            iOSNotificationCenter.RemoveAllDeliveredNotifications();
            iOSNotificationCenter.RemoveAllScheduledNotifications();
            var timeTrigger = new iOSNotificationTimeIntervalTrigger()
            {
                TimeInterval = new TimeSpan(hoursLater, minutesLater, secondsLater),
                Repeats = false
            };
            var iosNotif = new iOSNotification()
            {
                Identifier = iOS_Identifier,
                Title = title,
                Body = description,
                Subtitle = subtitleForiOS,
                ShowInForeground = true,
                ForegroundPresentationOption = (PresentationOption.Alert | PresentationOption.Sound),
                CategoryIdentifier = iOS_CategoryIdentifier,
                ThreadIdentifier = iOS_ThreadIdentifier,
                Trigger = timeTrigger,
                Data = extraData
            };
            iOSNotificationCenter.ScheduleNotification(iosNotif);
            Debug.Log("<color='blue'>notificatio scheduled for ios app</color>");
#endif
        }

        public static bool IsNotificationAuthorized()
        {
#if UNITY_IOS
            var settings = iOSNotificationCenter.GetNotificationSettings();
            return settings.AuthorizationStatus == AuthorizationStatus.Authorized;
#elif UNITY_ANDROID
            return true;
#endif
        }

        public static void RequestNotificationPermission_OnlyOnce(Action<bool> OnComplete)
        {
#if UNITY_ANDROID
            OnComplete?.Invoke(true);
#elif UNITY_IOS
            instance.StartCoroutine(instance.RequestAuthorization(OnComplete));
#endif
        }

#if UNITY_IOS
        IEnumerator RequestAuthorization(Action<bool> OnComplete)
        {
            Debug.Log("<color='blue'>will start taking notification permission for iOS</color>");
            using (var req = new AuthorizationRequest(AuthorizationOption.Alert | AuthorizationOption.Badge, true))
            {
                while (!req.IsFinished)
                {
                    yield return null;
                };

                string res = "\n RequestAuthorization: \n";
                res += "\n finished: " + req.IsFinished;
                res += "\n granted :  " + req.Granted;
                res += "\n error:  " + req.Error;
                res += "\n deviceToken:  " + req.DeviceToken;
                Debug.Log("<color='blue'>" + res + "</color>");
                OnComplete?.Invoke(req.Granted);
            }
        }
#endif
    }
}