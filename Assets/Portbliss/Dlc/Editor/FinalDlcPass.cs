﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;
using System.IO;
using Portbliss.DLC;

public class FinalDlcPass : EditorWindow
{
    [MenuItem("Window/Portbliss Dlc Final Pass _d")]
    static void Init()
    {
        EditorWindow window = GetWindow(typeof(FinalDlcPass));
        window.Show();
    }

    public bool balerBt;
    string version = "ver0.1";
    DescriptionOfOriginalCrcData crcData;
    void OnGUI()
    {
        GUILayout.Label("This Utility must be used after" + "" + " building " + Environment.NewLine + "all the assetbundles from assetbundle browser.");
        GUILayout.Space(10);
        GUIStyle style = new GUIStyle();
        style.richText = true;
        GUILayout.Label("<color='yellow'>If you have not installed asset bundle browser, " + Environment.NewLine + "from package manager, please do so!</color>", style);
        
        GUILayout.Space(10);
        GUILayout.Label("<color='yellow'>If you have not built the dlc from" + Environment.NewLine + "asset bundle browser, please do so!</color>", style);

        GUILayout.Space(10);
        GUILayout.Label("<color='yellow'>Dlc must be built in a folder named 'dlc'. " + Environment.NewLine + "Name is case sensitive!</color>", style);
        GUILayout.Space(30);
        GUILayout.Space(10);
        GUILayout.Label("<color='yellow'>You must change the version, otherwise game won't download them! "+Environment.NewLine+"Please check the current version from server!</color>", style);

        
        version = EditorGUILayout.TextField("DLC version: ", version);
        GUILayout.Space(20);
        if (GUILayout.Button("Final Pass"))
        {
            string dlcPath = EditorUtility.OpenFolderPanel("Locate dlc folder", "", "");
            if (string.IsNullOrEmpty(dlcPath))
            {
                return;
            }
            DirectoryInfo dlcFolder = new DirectoryInfo(dlcPath);
            if (dlcFolder.Name != "dlc")
            {
                EditorUtility.DisplayDialog("!Error!", "This is not a dlc folder, Please make sure to build dlc in a folder named 'dlc'!", "I got it.");
                return;
            }
            else
            {
                string[] files = Directory.GetFiles(dlcPath);
                List<FileInfo> fList = new List<FileInfo>();
                foreach (var f in files)
                {
                    FileInfo nfo = new FileInfo(f);
                    fList.Add(nfo);
                }

                var dlcManifestBundleFile = fList.Find((obj) => { return obj.Name == "dlc" || obj.Name == "dlc.gpkg"; });
                if (dlcManifestBundleFile == null)
                {
                    EditorUtility.DisplayDialog("!Error!", "You haven't made dlc into a folder named 'dlc' through asset bundle browser." +
                        " Just renaming parent folder wont work. " + Environment.NewLine +
                        "Plus remember this! DO NOT RENAME FILES INTO 'dlc' to get away! Build properly!", "I got it.");
                    return;
                }

                foreach (var f in fList)
                {
                    Debug.Log("op start---name is: " + f.Name + " and full name is: " + f.FullName +
                       " and extension is: " + f.Extension + " and is extension empty? " + (string.IsNullOrEmpty(f.Extension)));

                    if (f.Name == "version.txt" || f.Name == "crc.txt" || f.Extension == ".manifest")
                    {
                        f.Delete();
                        continue;
                    }

                    if (string.IsNullOrEmpty(f.Extension))
                    {
                        File.Move(f.FullName, Path.ChangeExtension(f.FullName, ".gpkg"));
                    }
                }

                string versionFilePath = Path.Combine(dlcPath, "version.txt");
                FileInfo verF = new FileInfo(versionFilePath);
                //verF.Create();
                File.WriteAllText(verF.FullName, version);

                string crcFilePath = Path.Combine(dlcPath, "crc.txt");
                FileInfo freshDeleteIfAnyInfo = new FileInfo(crcFilePath);
                if (freshDeleteIfAnyInfo.Exists) { freshDeleteIfAnyInfo.Delete(); }



                
                string[] filesNowOnDlcPath = Directory.GetFiles(dlcPath);
                List<FileInfo> allFilesNfosNow = new List<FileInfo>();
                foreach (var f in filesNowOnDlcPath)
                {
                    FileInfo nfo = new FileInfo(f);
                    allFilesNfosNow.Add(nfo);
                }

                DescriptionOfOriginalCrcData descCrc = new DescriptionOfOriginalCrcData();
                descCrc.crcs = new OriginalCrcData[allFilesNfosNow.Count];
                for (int i = 0; i < descCrc.crcs.Length; i++)
                {
                    descCrc.crcs[i] = new OriginalCrcData();
                    descCrc.crcs[i].fileName = allFilesNfosNow[i].Name;
                    Crc32 c = new Crc32();
                    byte[] content = File.ReadAllBytes(allFilesNfosNow[i].FullName);
                    if (content == null || content.Length <= 0)
                    {
                        EditorUtility.DisplayDialog("!Error!", "Could not generate CRC32-B for file: "+allFilesNfosNow[i].FullName, "Ok");
                        return;
                    }
                    uint crcNumber = c.ComputeChecksum(content);
                    descCrc.crcs[i].crc = crcNumber;
                }

                string crcDataJson = JsonUtility.ToJson(descCrc);
                if (string.IsNullOrEmpty(crcDataJson))
                {
                    EditorUtility.DisplayDialog("!Error!", "Generated CRC32-B related data can not be empty, There might be error within.", "Ok");
                    return;
                }

                DescriptionOfOriginalCrcData testData = JsonUtility.FromJson<DescriptionOfOriginalCrcData>(crcDataJson);
                if (testData == null || testData.crcs == null)
                {
                    EditorUtility.DisplayDialog("!Error!", "Generated CRC32-B data could not be read for checking validity," +Environment.NewLine+
                        " this means crc data is corrupt." +
                        " There might be error within." +Environment.NewLine+
                        "If you upload this data to the server and game has 'IsCRCCheckImportant' flag"+Environment.NewLine+" enabled in 'DLC_Manager' script, " +
                        "then you might get errornours behavior(s).", "Ok");
                    return;
                }

                FileInfo cInfo = new FileInfo(crcFilePath);
                //cInfo.Create();
                File.WriteAllText(cInfo.FullName, crcDataJson);

            }
        }

        this.Repaint();
    }
}
