﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FishSpace;
using System;
using System.IO;
using UnityEngine.Networking;

namespace Portbliss.DLC
{
    [System.Serializable]
    public class AllDlcDescription
    {
        [SerializeField]AllProgressDescription progressDescription;
        public AllProgressDescription ProgressDescription { get { return progressDescription; } }
        [SerializeField] DLC_Descriptor[] allDesc;
        public DLC_Descriptor[] AllDescriptors { get { return allDesc; } }
        public bool isDlcBeingDownloaded;
        HardData<bool> allDlcOpCompletionFlagOnHardData;
        public bool HasAllDlcOpBeenDone { get { return allDlcOpCompletionFlagOnHardData.value; } }
        HardData<string> versionDataOnHarddata;
        public string CurrentVersion { get { return versionDataOnHarddata.value; } }
        public string webVersion;
        HardData<DateTime> lastUpdateCheckTime;
        public HardData<bool> WillWeDownloadManifest, WillWeDownloadCrcData, IsVersionDlTimeError, InTheMiddleOfAPatch, HasSharedDlcDownloaded;

        public AllDlcDescription(int dlcLevelStart, int totalNumberOfDlcLevels)
        {
            progressDescription = new AllProgressDescription(dlcLevelStart, totalNumberOfDlcLevels);
            allDlcOpCompletionFlagOnHardData = new HardData<bool>("ALL_DLC_DOWNLOAD_AND_DISK_STORE_OP_COMPLETED", false);
            versionDataOnHarddata = new HardData<string>("DLC_VERSION_NUM", "ZERO_DAY");
            WillWeDownloadManifest = new HardData<bool>("DLC_MANIFEST_NEED_FLAG", true);
            WillWeDownloadCrcData = new HardData<bool>("DLC_CRC_LIST_NEED_FLAG", true);
            IsVersionDlTimeError = new HardData<bool>("DLC_VERSION_DOWNLOAD_TIME_ERROR_FLAG", false);
            InTheMiddleOfAPatch = new HardData<bool>("DLC_IN_THE_MIDDLE_OF_A_PATCH_FLAG", false);
            DateTime now = DateTime.Now;
            DateTime defaultTime = new DateTime(now.Year - 1, now.Month, now.Day, now.Hour, now.Minute, now.Second);

            lastUpdateCheckTime = new HardData<DateTime>("LAST_UPDATE_CHECK_TIME", defaultTime);
            allDesc = new DLC_Descriptor[totalNumberOfDlcLevels];
            int lvNum = dlcLevelStart;
            for (int i = 0; i < totalNumberOfDlcLevels; i++)
            {
                allDesc[i] = new DLC_Descriptor(lvNum);
                lvNum = lvNum + 1;
            }
        }

        public DLC_Descriptor GetDescriptorForLevel(int levelNum)
        {
            DLC_Descriptor result = null;
            foreach (var d in allDesc)
            {
                if (d.LevelNum == levelNum)
                {
                    result = d;
                    break;
                }
            }
            return result;
        }

        /// <summary>
        /// if we need to re-download due to update, only then call this just prior of starting manifest re-download.
        /// </summary>
        public void ResetCertainFlagsAndDeleteDlcFolder()
        {           
            allDlcOpCompletionFlagOnHardData.value = false;

            if (InTheMiddleOfAPatch.value == false)
            {
                WillWeDownloadManifest.value = true;
                WillWeDownloadCrcData.value = true;
                Debug.Log("<color='magenta'>update time(patch came probably)</color>");
                foreach (var d in allDesc)
                {
                    d.DoesExistAsHD_Flag.value = false;
                }
                InTheMiddleOfAPatch.value = true;
            }
        }

        public bool HasTimeBeenPassed(float days = 7, float hours = 0, float minutes = 0, float seconds = 0)
        {
            TimeSpan timeSpan = DateTime.Now - lastUpdateCheckTime.value;
            float secondsToCheck = seconds + minutes * 60 + hours * 3600 + days * 24 * 3600;
            return timeSpan.TotalSeconds > (int)secondsToCheck;
        }

        /// <summary>
        /// must be called after all download
        /// </summary>
        public void SetAsComplete()
        {
            allDlcOpCompletionFlagOnHardData.value = true;
            versionDataOnHarddata.value = webVersion;
            lastUpdateCheckTime.value = DateTime.Now;
            WillWeDownloadManifest.value = false;
            WillWeDownloadCrcData.value = false;
            InTheMiddleOfAPatch.value = false;
        }

        public bool IsThereAnyManifestOnDiskAndItisValid()
        {
            string dataFolderPath = Path.Combine(Application.persistentDataPath, DlcManager.DiskDlcFolderName);
            string manPath = Path.Combine(dataFolderPath, "dlc.gpkg");
            FileInfo manInfo = new FileInfo(manPath);
            return manInfo.Exists && WillWeDownloadManifest.value == false;
        }

        public bool IsThereAnyCrcOnDiskAndIsItValid()
        {
            string dataFolderPath = Path.Combine(Application.persistentDataPath, DlcManager.DiskDlcFolderName);
            string crcPath = Path.Combine(dataFolderPath, "crc.txt");
            FileInfo crcInfo = new FileInfo(crcPath);
            return crcInfo.Exists && WillWeDownloadCrcData.value == false;
        }

        public bool IsThereAnyLevelFiles()
        {
            string dataFolderPath = Path.Combine(Application.persistentDataPath, DlcManager.DiskDlcFolderName);
            DirectoryInfo info = new DirectoryInfo(dataFolderPath);
            bool isThere = false;

            if (info.Exists)
            {
                var files = info.GetFiles();
                foreach (var f in files)
                {
                    if (f.Name.Contains("Level"))
                    {
                        isThere = true;
                        break;
                    }
                }
            }
            return isThere;
        }
    }

    [System.Serializable]
    public class DLC_Descriptor
    {
        [SerializeField] string webUrl, diskUrl;
        public string WebUrl { get { return webUrl; } }
        public string DiskUrl { get { return diskUrl; } }
        public HardData<bool> DoesExistAsHD_Flag;
        [SerializeField] int levelNum;
        public int LevelNum { get { return levelNum; } }
        public bool DoesItExist()
        {
            FileInfo nfo = new FileInfo(diskUrl);
            bool existOnDisk = nfo.Exists;
            bool existOnHardData = DoesExistAsHD_Flag.value;
            return existOnDisk && existOnHardData == true;
        }

        public DLC_Descriptor(int levelNum)
        {
            this.levelNum = levelNum;
            webUrl = DlcManager.BaseSrvUrlWithEndSlash + "level+" + levelNum + ".gpkg";
            string diskDataPath = Path.Combine(Application.persistentDataPath, DlcManager.DiskDlcFolderName);
            string fileName = "level+" + levelNum + ".gpkg";
            string existFlagKey = "level+" + levelNum + "_EXIST_KEY";
            diskUrl = Path.Combine(Application.persistentDataPath, DlcManager.DiskDlcFolderName + "/" + fileName);

            DoesExistAsHD_Flag = new HardData<bool>(existFlagKey, false);
        }
    }
}