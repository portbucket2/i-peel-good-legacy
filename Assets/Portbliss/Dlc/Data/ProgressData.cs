﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

namespace Portbliss.DLC
{
    public enum AsyncTaskType { VersionDownload, ManifestDownload, CrcDownload, CrcLoadFromDisk, DownloadDlc, SharedDlc }
    [System.Serializable]
    public class ProgressDescription
    {
        public UnityWebRequestAsyncOperation asyncOp;
        [SerializeField] AsyncTaskType taskType;
        public AsyncTaskType TaskType { get { return taskType; } }
        public bool IsDone;
        [SerializeField] string taskName;
        public string TaskName { get { return taskName; } }
        int levelNumIfDlcDownload;
        public int LevelNumIfDlcDownload { get { return levelNumIfDlcDownload; } }

        public ProgressDescription(AsyncTaskType asyncTaskType, int levelNumIfDlcType = -1)
        {
            taskType = asyncTaskType;
            if (taskType == AsyncTaskType.VersionDownload)
            {
                taskName = "Downloading version file...";
            }
            else if (taskType == AsyncTaskType.ManifestDownload)
            {
                taskName = "Downloading manifest file...";
            }
            else if (taskType == AsyncTaskType.CrcDownload)
            {
                taskName = "Downloading Crc file...";
            }
            else if (taskType == AsyncTaskType.CrcLoadFromDisk)
            {
                taskName = "Load crc file...";
            }
            else if (taskType == AsyncTaskType.DownloadDlc)
            {
                taskName = "Downloading Level " + levelNumIfDlcType + " dlc file...";
            }
            else if (taskType == AsyncTaskType.SharedDlc)
            {
                taskName = "Downloading shared dlc file...";
            }
            levelNumIfDlcDownload = levelNumIfDlcType;
        }

        public float GetProgress()
        {
            if (asyncOp != null)
            {
                return asyncOp.progress;
            }
            else
            {
                return 0f;
            }
        }
    }

    [System.Serializable]
    public class AllProgressDescription
    {
        public ProgressDescription[] progressDescriptions;
        public ProgressDescription CurrentTaskDescription;
        public bool HasConnectionError;
        public AllProgressDescription(int dlcLevelStart, int totalDlcLevels)
        {
            progressDescriptions = new ProgressDescription[totalDlcLevels + 5];
            progressDescriptions[0] = new ProgressDescription(AsyncTaskType.VersionDownload);
            progressDescriptions[1] = new ProgressDescription(AsyncTaskType.ManifestDownload);
            progressDescriptions[2] = new ProgressDescription(AsyncTaskType.CrcDownload);
            progressDescriptions[3] = new ProgressDescription(AsyncTaskType.CrcLoadFromDisk);
            progressDescriptions[4] = new ProgressDescription(AsyncTaskType.SharedDlc);
            int lvStart = dlcLevelStart;
            for (int i = 4; i < progressDescriptions.Length; i++)
            {
                progressDescriptions[i] = new ProgressDescription(AsyncTaskType.DownloadDlc, lvStart);
                lvStart = lvStart + 1;
            }
        }

        public float GetCurrentProgress()
        {
            if (CurrentTaskDescription == null) { return 0f; }
            else
            {
                return CurrentTaskDescription.GetProgress();
            }
        }

        public float GetOverallProgress()
        {
            if (CurrentTaskDescription == null) { return 0; }
            else
            {
                float progressCur = CurrentTaskDescription.GetProgress();
                int curProgNum = -1;
                for (int i = 0; i < progressDescriptions.Length; i++)
                {
                    if (CurrentTaskDescription == progressDescriptions[i])
                    {
                        curProgNum = i + 1;
                        break;
                    }
                }
                if (curProgNum < 0)
                {
                    return 0f;
                }
                else
                {
                    float progressSofar = (float)(curProgNum - 1) + progressCur;
                    float totalProgress = (float)progressDescriptions.Length;
                    return progressSofar / totalProgress;
                }
            }
        }

        public ProgressDescription GetProgressDescByIdentifier(AsyncTaskType taskType, int levelNumIfDlcDownload = -1)
        {
            ProgressDescription result = null;
            for (int i = 0; i < progressDescriptions.Length; i++)
            {
                if (taskType == progressDescriptions[i].TaskType && taskType == AsyncTaskType.DownloadDlc)
                {
                    if (levelNumIfDlcDownload == progressDescriptions[i].LevelNumIfDlcDownload &&
                        levelNumIfDlcDownload > 0)
                    {
                        result = progressDescriptions[i];
                        break;
                    }
                }
                else if (taskType == progressDescriptions[i].TaskType && taskType != AsyncTaskType.DownloadDlc)
                {
                    result = progressDescriptions[i];
                    break;
                }
            }
            return result;
        }
    }
}