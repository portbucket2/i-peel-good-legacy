﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Portbliss.DLC
{
    [System.Serializable]
    public class DescriptionOfOriginalCrcData
    {
        public OriginalCrcData[] crcs;

        public uint GetOriginalCrc(int levelNo)
        {
            OriginalCrcData data = null;
            foreach (var d in crcs)
            {
                if (d.fileName == "level " + levelNo + ".gpkg")
                {
                    data = d;
                    break;
                }
            }
            return data == null ? 0000 : data.crc;
        }

        public uint GetOriginalCrcForSharedDlc()
        {
            OriginalCrcData data = null;
            foreach (var d in crcs)
            {
                if (d.fileName == "shared.gpkg")
                {
                    data = d;
                    break;
                }
            }
            return data == null ? 0000 : data.crc;
        }
    }

    [System.Serializable]
    public class OriginalCrcData
    {
        public uint crc;
        public string fileName;
    }
}