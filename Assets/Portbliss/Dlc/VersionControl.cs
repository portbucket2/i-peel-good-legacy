﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.Networking;

namespace Portbliss.DLC
{
    public class VersionControl : MonoBehaviour
    {
        //const bool logEnabled = true;
        int attemptVersion = 0;
        Action<bool> savedCallback;
        void NoNeedToUpdate()
        {
            savedCallback?.Invoke(false);
            savedCallback = null;
            StopAllCoroutines();
        }

        void NeedToUpdate()
        {
            savedCallback?.Invoke(true);
            savedCallback = null;
            StopAllCoroutines();
        }

        public void CheckVersion(Action<bool> OnComplete)
        {
            attemptVersion = 0;
            savedCallback = OnComplete;
            StartCoroutine(DownloadVersion());
        }
        
        IEnumerator DownloadVersion()
        {
            string url = DlcManager.BaseSrvUrlWithEndSlash + "version.txt";
            using (UnityWebRequest www = UnityWebRequest.Get(url))
            {
                var op = www.SendWebRequest();
                var prog = DlcManager.AllDlcDescription.ProgressDescription.GetProgressDescByIdentifier(AsyncTaskType.VersionDownload);
                DlcManager.AllDlcDescription.ProgressDescription.CurrentTaskDescription = prog;
                prog.asyncOp = op;
                yield return op;
                prog.IsDone = true;
                if (DlcManager.VersionLogEnabled) { Debug.Log("<color='green'>completed version.txt download from web.</color>"); }
                if (www.isNetworkError || www.isHttpError)
                {
                    DlcManager.AllDlcDescription.IsVersionDlTimeError.value = true;
                    DlcManager.AllDlcDescription.ProgressDescription.HasConnectionError = true;
                    if (DlcManager.VersionLogEnabled) { Debug.Log("<color='red'>version file download error: " + www.error + " and attempt so far: " + attemptVersion + "</color>"); }
                    DlcManager.AllDlcDescription.ProgressDescription.CurrentTaskDescription = null;
                    if (attemptVersion > DlcManager.VersionDownloadAttemptMax)
                    {
                        NoNeedToUpdate();
                    }
                    else
                    {
                        attemptVersion++;
                        yield return StartCoroutine(DownloadVersion());
                    }
                }
                else
                {
                    DlcManager.AllDlcDescription.IsVersionDlTimeError.value = false;
                    DlcManager.AllDlcDescription.ProgressDescription.HasConnectionError = false;
                    DlcManager.AllDlcDescription.webVersion = www.downloadHandler.text;
                    Debug.Log("stored web version is: " + DlcManager.AllDlcDescription.webVersion
                        + " and current game's version is: " + DlcManager.AllDlcDescription.CurrentVersion);
                    if (DlcManager.AllDlcDescription.webVersion == DlcManager.AllDlcDescription.CurrentVersion)
                    {
                        NoNeedToUpdate();
                    }
                    else
                    {
                        //need update
                        DlcManager.AllDlcDescription.ResetCertainFlagsAndDeleteDlcFolder();
                        yield return new WaitForSeconds(0.7f);
                        NeedToUpdate();
                    }
                }
            }
        }
    }
}