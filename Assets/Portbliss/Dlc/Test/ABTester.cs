﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
#if UNITY_IOS
using UnityEngine.iOS;
#endif
using UnityEngine.Networking;
using UnityEngine.UI;

namespace Portbliss.DLC.Test
{
    public class ABTester : MonoBehaviour
    {
        [SerializeField] Button downloadBtn, settingBtn, backSettingBtn, applyBtn, deleteDlcBtn;
        [SerializeField] GameObject mainPanel, settingsPanel;
        [SerializeField] Toggle unloadModeToggle;
        [SerializeField] Text statusText;
        [SerializeField] Image overallImg, currentImg;
        [SerializeField] InputField levelDlcFileNameInp, dlcManifestFileNameInp, levelPrefabNameInp, dataFolderNameInp, baseUrlInp;
        string levelDlcFileName, dlcManifestFileName, prefabNameInsideBundle, dataFolderName, baseUrl;
        public GameObject loadedObj;
        UnityWebRequestAsyncOperation curOp;


        void Start()
        {
            Debug.LogWarning("persistant data path: " + Application.persistentDataPath);
            downloadBtn.onClick.RemoveAllListeners();
            downloadBtn.onClick.AddListener(() =>
            {
                StartCoroutine(DownloadFile());
            });
            DoUI();
            curOp = null;
            currentImg.fillAmount = 0f;
            statusText.text = "0/2";
            overallImg.fillAmount = 0f;

            DateTime d1 = DateTime.Now;
            DateTime d2 = d1; //new DateTime(d1.Year, d1.Month, d1.Day, d1.Hour - 4, d1.Minute, d1.Second);
            d2 = d2.AddHours(-45.456d);
            

            TimeSpan t = d1 - d2;
            Debug.LogWarning("<color='yellow'>d1 is: " + d1 + " and d2 is: " + d2 +
                " and the timespan t is: " + t + " and day difference: " + t.Hours +
                " and day diff total: " + t.TotalHours + "</color>");
        }

        private void Update()
        {
            if (curOp != null)
            {
                currentImg.fillAmount = curOp.progress;
            }
        }

        void DoUI()
        {
            mainPanel.SetActive(true);
            settingsPanel.SetActive(false);

            settingBtn.onClick.RemoveAllListeners();
            settingBtn.onClick.AddListener(() =>
            {
                mainPanel.SetActive(false);
                settingsPanel.SetActive(true);
            });

            backSettingBtn.onClick.RemoveAllListeners();
            backSettingBtn.onClick.AddListener(() =>
            {
                mainPanel.SetActive(true);
                settingsPanel.SetActive(false);
            });

            deleteDlcBtn.onClick.RemoveAllListeners();
            deleteDlcBtn.onClick.AddListener(() =>
            {
                string dPath = Path.Combine(Application.persistentDataPath, dataFolderName + "/");
                DirectoryInfo dataFolderInfo = new DirectoryInfo(dPath);
                if (dataFolderInfo.Exists == true)
                {
                    try
                    {
                        dataFolderInfo.Delete(true);
                        Debug.Log("<color='red'>DLC has been deleted!</color>");
                    }
                    catch (System.Exception ex)
                    {
                        Debug.Log("<color='red'>could not delete folder in the location: " +
                            dPath + " the error: " + ex.Message + "</color>");
                        throw;
                    }
                }
            });

            applyBtn.onClick.RemoveAllListeners();
            applyBtn.onClick.AddListener(() =>
            {
                ApplyUI();
            });

            ApplyUI();
        }

        void ApplyUI()
        {
            levelDlcFileName = levelDlcFileNameInp.text;
            dlcManifestFileName = dlcManifestFileNameInp.text;
            prefabNameInsideBundle = levelPrefabNameInp.text;
            dataFolderName = dataFolderNameInp.text;
            baseUrl = baseUrlInp.text;
        }


        IEnumerator DownloadFile()
        {
            //statusText.text = "0/2";
            //overallImg.fillAmount = 0f;
            //currentImg.fillAmount = 0f;
            //curOp = null;
            //if (loadedObj != null)
            //{
            //    DestroyImmediate(loadedObj);
            //    Debug.Log("<color='yellow'>previously loaded level is destroyed!</color>");
            //}

            //string dPath = Path.Combine(Application.persistentDataPath, dataFolderName + "/");
            //DirectoryInfo dataFolderInfo = new DirectoryInfo(dPath);
            //if (dataFolderInfo.Exists == false)
            //{
            //    try
            //    {
            //        dataFolderInfo.Create();
            //        Debug.Log("<color='green'>data folder did not present! so we created it.</color>");
            //    }
            //    catch (System.Exception ex)
            //    {
            //        Debug.Log("<color='red'>could not create folder in the location: " +
            //            dPath + " the error: " + ex.Message + "</color>");
            //        throw;
            //    }
            //}
            //string manPath_disk = Path.Combine(Application.persistentDataPath, dataFolderName + "/" + dlcManifestFileName);
            string pkgPath_disk = Path.Combine(Application.persistentDataPath, dataFolderName + "/" + levelDlcFileName);
            string pkgUrl = baseUrl + levelDlcFileName;
            //string manUrl = baseUrl + dlcManifestFileName;

//            Debug.Log("<color='green'>Now let us download manifest and write it onto disk.</color>");
//            if (File.Exists(manPath_disk) == false)
//            {
//                using (UnityWebRequest manWWW = UnityWebRequest.Get(manUrl))
//                {
//                    curOp = manWWW.SendWebRequest();
//                    yield return curOp;
//                    if (manWWW.isNetworkError || manWWW.isHttpError)
//                    {
//                        Debug.Log("<color='red'>manifest download error: " + manWWW.error + "</color>");
//                    }
//                    else
//                    {
//                        try
//                        {
//                            File.WriteAllBytes(manPath_disk, manWWW.downloadHandler.data);
//                            Debug.Log("<color='green'>manifest file written.</color>");
//                        }
//                        catch (System.Exception ex)
//                        {
//                            Debug.Log("<color='red'>could not write manifest byte array as file on location: " + manPath_disk
//                                + " and the error: " + ex.Message + "</color>");
//                            throw;
//                        }
//#if UNITY_IOS
//                    Device.SetNoBackupFlag(manPath_disk);
//#endif
//                        Debug.Log("<color='green'>manifest file location has been patched for itune issue.</color>");
//                    }
//                }
//            }

//            statusText.text = "1/2";
//            overallImg.fillAmount = 0.5f;
//            Resources.UnloadUnusedAssets();
//            AssetBundle.UnloadAllAssetBundles(true);
//            Debug.Log("<color='green'>Since manifest has been written, we have just unloaded all assetbundles.</color>");


//            AssetBundle manifestBundle = null;
//            try
//            {
//                manifestBundle = AssetBundle.LoadFromFile(manPath_disk);
//                Debug.Log("<color='green'>manifest assetbundle has been loaded from file location:" + manPath_disk + "</color>");
//            }
//            catch (System.Exception ex)
//            {

//                Debug.Log("<color='red'>could not load assetbundle from the manifest file on location: "
//                    + manPath_disk + " and the error is: " + ex.Message + "</color>");
//                throw;
//            }

//            Debug.Log("<color='green'>manifest file location has been loaded from file location." +
//                " Now we will try to load the manifest data itself from manifest asset bundle data.</color>");
//            AssetBundleManifest manifest = null;
//            try
//            {

//                manifest = manifestBundle.LoadAsset<AssetBundleManifest>("AssetBundleManifest");
//                Debug.Log("<color='green'>Manifest data has been loaded.</color>");
//            }
//            catch (System.Exception ex)
//            {
//                Debug.Log("<color='red'>could not load manifest data from the manifest assetbundle, the error is: "
//                    + ex.Message + "</color>");
//                throw;
//            }

//            Debug.Log("<color='green'>Lets get hash from manifest data</color>");
//            var bundleHash = new Hash128();
//            try
//            {
//                bundleHash = manifest.GetAssetBundleHash(prefabNameInsideBundle);
//                Debug.Log("<color='green'>hash found from manifest data.</color>");
//            }
//            catch (System.Exception ex)
//            {
//                Debug.Log("<color='red'>could not get hash from " + prefabNameInsideBundle + ", error: " + ex.Message + "</color>");
//                throw;
//            }
//            Debug.Log("<color='green'>Hash found so lets use it to get level bundle.</color>");

//            manifestBundle.Unload(true);
//            AssetBundle.UnloadAllAssetBundles(true);
//            Resources.UnloadUnusedAssets();
//            Debug.Log("<color='green'>all asset bundle cleared</color>");

//            downloadBtn.image.color = Color.red;

//            byte[] content = null;
//            using (UnityWebRequest pkgWWW = UnityWebRequest.Get(pkgUrl))
//            {
//                Debug.Log("<color='green'>obtained main level bundle fetch request.</color>");
//                curOp = pkgWWW.SendWebRequest();
//                yield return curOp;
//                if (pkgWWW.isNetworkError || pkgWWW.isHttpError)
//                {
//                    Debug.Log("<color='red'>pkg download error: " + pkgWWW.error + "</color>");
//                }
//                else
//                {
//                    content = pkgWWW.downloadHandler.data;

//                }
//            }

//            statusText.text = "2/2";
//            overallImg.fillAmount = 1f;

//            Debug.Log("before crc time: " + Time.time);
//            UnityEngine.Profiling.Profiler.BeginSample("generate crc data");
//            Crc32 c = new Crc32();
//            UnityEngine.Profiling.Profiler.EndSample();
//            yield return null;
//            UnityEngine.Profiling.Profiler.BeginSample("calculate crc data");
//            uint v = c.ComputeChecksum(content);
//            UnityEngine.Profiling.Profiler.EndSample();
//            Debug.Log("crc32 is: " + v + " for file name: " + levelDlcFileName);
//            Debug.Log("after crc time: " + Time.time);
//            yield return null;
//            Debug.Break();
//            Debug.Log("<color='green'>fully downloaded the level bundle data. lets write it into the disk.</color>");
//            downloadBtn.image.color = Color.yellow;
//            try
//            {

//                File.WriteAllBytes(pkgPath_disk, content);
//                Debug.Log("<color='green'>level bundle data has been written into the disk.</color>");
//            }
//            catch (System.Exception ex)
//            {
//                Debug.Log("<color='red'>could not write main dlc data on device location: "
//                    + pkgPath_disk + " and error:" + ex.Message + "</color>");
//                throw;
//            }

#if UNITY_IOS
        Device.SetNoBackupFlag(pkgPath_disk);
#endif
            AssetBundle.UnloadAllAssetBundles(true);
            Resources.UnloadUnusedAssets();
            Debug.Log("<color='green'>package location has been patched for itune issue and stuffs were unloaded.</color>");

            AssetBundle lvMainBundle = null;
            try
            {
                lvMainBundle = AssetBundle.LoadFromFile(pkgPath_disk);
                Debug.Log("<color='green'>main level bundle has been obtained from local file location.</color>");
            }
            catch (System.Exception ex)
            {
                //Debug.Log("<color='red'>could not load assetbundle from disk location: " +
                //    manPath_disk + " natively and the error: " + ex.Message + "</color>");
                throw;
            }

            GameObject lvObject = null;
            try
            {
                string fName = prefabNameInsideBundle;// + ".prefab";
                Debug.Log("<color='green'> look up name is: " + fName + "</color>");
                lvObject = lvMainBundle.LoadAsset<GameObject>(fName);
                Debug.Log("main pkg bundle name: " + lvMainBundle.name);
                Debug.Log("loaded prefab name: " + lvObject.name);
                Debug.Log("<color='green'>level prefab gameobject has been loaded</color>");
            }
            catch (System.Exception ex)
            {
                Debug.Log("<color='red'>could not load level gameobject from assetbundle and the error: " + ex.Message + "</color>");
                throw;
            }

            downloadBtn.image.color = Color.green;

            if (lvObject != null)
            {
                loadedObj = Instantiate(lvObject);
                Debug.Log("<color='green'>level prefab has been cloned!</color>");
            }
            else
            {
                Debug.Log("<color='red'>but level prefab gameobject is null!</color>");
            }

            yield return new WaitForSeconds(5f);
            //AssetBundle.UnloadAllAssetBundles(unloadModeToggle.isOn);
            //Resources.UnloadUnusedAssets();
            //Debug.Log("<color='yellow'>all cleaned up!</color>");

            yield return null;
        }
    }
}