﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;
using UnityEngine.Networking;
#if UNITY_IOS
using UnityEngine.iOS;
#endif

namespace Portbliss.DLC
{
    public class ManifestControl : MonoBehaviour
    {
        //const bool logEnabled = true;
        int attemptNum = 0;
        Action<AssetBundleManifest> savedCallback;

        void ManifestDone(AssetBundleManifest data)
        {
            if (data == null)
            {
                DlcManager.AllDlcDescription.WillWeDownloadManifest.value = true;
            }
            savedCallback?.Invoke(data);
            savedCallback = null;
            StopAllCoroutines();
        }

        public void DownloadAndStoreIfReq_AndGetManifest(Action<AssetBundleManifest> OnComplete)
        {
            attemptNum = 0;
            savedCallback = OnComplete;
            string dataFolderPath = Path.Combine(Application.persistentDataPath, DlcManager.DiskDlcFolderName);
            string diskUrl = Path.Combine(dataFolderPath, "dlc.gpkg");
            StartCoroutine(StartManifest(diskUrl));
        }

        IEnumerator StartManifest(string diskUrl)
        {
            yield return null;
            if (DlcManager.AllDlcDescription.IsThereAnyManifestOnDiskAndItisValid())
            {
                try
                {
                    var manifest = DlcUtil.LoadManifestFromFile(diskUrl, DlcManager.ManifestLogEnabled);
                    if (DlcManager.ManifestLogEnabled) { Debug.Log("<color='green'>Manifest has been loaded from file.</color>"); }
                    ManifestDone(manifest);
                }
                catch (Exception ex)
                {
                    if (DlcManager.ManifestLogEnabled) { Debug.Log("<color='red'>manifest load from file error: " + ex.Message + "</color>"); }
                    ManifestDone(null);
                }
            }
            else
            {
                //start manifest download coroutine
                yield return StartCoroutine(StartManifestCore());
            }
        }

        IEnumerator StartManifestCore()
        {
            string dataPath = Path.Combine(Application.persistentDataPath, DlcManager.DiskDlcFolderName);
            string diskUrl = Path.Combine(dataPath, "dlc.gpkg");
            string webUrl = Path.Combine(DlcManager.BaseSrvUrlWithEndSlash, "dlc.gpkg");

            if (DlcManager.ManifestLogEnabled) { Debug.Log("<color='green'>Now let us download manifest and write it onto disk.</color>"); }
           
            using (UnityWebRequest www = UnityWebRequest.Get(webUrl))
            {
                var op = www.SendWebRequest();
                var prog = DlcManager.AllDlcDescription.ProgressDescription.GetProgressDescByIdentifier(AsyncTaskType.ManifestDownload);
                DlcManager.AllDlcDescription.ProgressDescription.CurrentTaskDescription = prog;
                prog.asyncOp = op;
                yield return op;
                if (DlcManager.ManifestLogEnabled) { Debug.Log("<color='green'>manifest web operation finished.</color>"); }
                if (www.isNetworkError || www.isHttpError)
                {
                    DlcManager.AllDlcDescription.ProgressDescription.HasConnectionError = true;
                    if (DlcManager.ManifestLogEnabled) { Debug.Log("<color='red'>manifest download error: " + www.error + "</color>"); }
                    DlcManager.AllDlcDescription.ProgressDescription.CurrentTaskDescription = null;
                    if (attemptNum > DlcManager.ManifestDownloadAttemptMax)
                    {
                        if (DlcManager.ManifestLogEnabled) { Debug.Log("<color='red'>could not download manifest file, aborted after N attempt.</color>"); }
                        ManifestDone(null);
                    }
                    else
                    {
                        attemptNum++;
                        if (DlcManager.ManifestLogEnabled) { Debug.Log("<color='yellow'>lets try to download manifest again</color>"); }
                        yield return StartCoroutine(StartManifestCore());
                    }
                }
                else
                {
                    DlcManager.AllDlcDescription.ProgressDescription.HasConnectionError = false;
                    FileInfo file = new FileInfo(diskUrl);
                    file.Directory.Create(); // If the directory already exists, this method does nothing.

                    yield return new WaitForSeconds(5.7f);
                    try
                    {
                        File.WriteAllBytes(diskUrl, www.downloadHandler.data);
                        DlcManager.AllDlcDescription.WillWeDownloadManifest.value = false;
                        if (DlcManager.ManifestLogEnabled) { Debug.Log("<color='green'>Manifest file written onto the disk</color>"); }
                    }
                    catch (Exception ex)
                    {
                        if (DlcManager.ManifestLogEnabled) { Debug.Log("<color='red'>manifest write into file system error: " + ex.Message + "</color>"); }
                        ManifestDone(null);
                    }

                    try
                    {
                        var manifest = DlcUtil.LoadManifestFromFile(diskUrl, DlcManager.ManifestLogEnabled);
                        if (DlcManager.ManifestLogEnabled) { Debug.Log("<color='green'>Manifest file loaded from file</color>"); }
                        ManifestDone(manifest);
                    }
                    catch (Exception ex)
                    {
                        if (DlcManager.ManifestLogEnabled) { Debug.Log("<color='red'>manifest load from file error: " + ex.Message + "</color>"); }
                        ManifestDone(null);
                    }
                    yield return new WaitForSeconds(0.7f);
#if UNITY_IOS
                    Device.SetNoBackupFlag(diskUrl);
#endif
                    if (DlcManager.ManifestLogEnabled) { Debug.Log("<color='green'>manifest file location has been patched for itune issue.</color>"); }
                }
            }
            AssetBundle.UnloadAllAssetBundles(false);
            Resources.UnloadUnusedAssets();
            if (DlcManager.ManifestLogEnabled) { Debug.Log("<color='green'>Since manifest has been written, we have just unloaded all assetbundles.</color>"); }
            yield return null;
        }
    }
}