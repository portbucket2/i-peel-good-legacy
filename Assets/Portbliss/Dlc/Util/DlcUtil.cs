﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Portbliss.DLC
{
    public static class DlcUtil
    {
        public static AssetBundleManifest LoadManifestFromFile(string diskUrl, bool logEnabled)
        {
            AssetBundle manifestBundle = null;
            AssetBundleManifest manifest = null;
            manifestBundle = AssetBundle.LoadFromFile(diskUrl);
            if (logEnabled) { Debug.Log("<color='green'>manifest assetbundle has been loaded from file location:" + diskUrl + "</color>"); }


            if (logEnabled)
            {
                Debug.Log("<color='green'>manifest file location has been loaded from file location." +
  " Now we will try to load the manifest data itself from manifest asset bundle data.</color>");
            }


            manifest = manifestBundle.LoadAsset<AssetBundleManifest>("AssetBundleManifest");
            if (logEnabled) { Debug.Log("<color='green'>Manifest data has been loaded.</color>"); }

            return manifest;
        }

        public static void SetActiveCustom(this GameObject g, bool isActive)
        {
            if (g != null && g.activeInHierarchy == !isActive)
            {
                g.SetActive(isActive);
            }
        }
    }
}