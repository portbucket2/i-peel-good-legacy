﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace Portbliss.DLC
{
    public static class Logger
    {
        public static void LogDownloadCondition(bool mandatoryDlCondition, bool nDayCondition, bool contdContition)
        {
            if (mandatoryDlCondition)
            {
                Debug.Log("<color='yellow'>mandatory check passed, we must download dlc</color>");
            }

            if (nDayCondition)
            {
                Debug.Log("<color='yellow'>N day check passed, we must download dlc</color>");
            }

            if (contdContition)
            {
                Debug.Log("<color='yellow'>contineuous check passed, we must download dlc</color>");
            }
        }

        public static void LogDlcFiles()
        {
            string dataPath = Path.Combine(Application.persistentDataPath, DlcManager.DiskDlcFolderName);
            DirectoryInfo dataFolderNfo = new DirectoryInfo(dataPath);
            if (dataFolderNfo.Exists == false)
            {
                Debug.Log("<color='red'>No DLC folder!</color>");
            }
            else
            {
                Debug.Log("<color='green'>DLC folder found!</color>");
                var allFiles = dataFolderNfo.GetFiles();
                var allFolders = dataFolderNfo.GetDirectories();
                if (allFolders != null && allFolders.Length > 0)
                {
                    Debug.Log("<color='yellow'>We have not created any folder inside data folder, yet it is there! Lets log their names</color>");
                    foreach (var d in allFolders)
                    {
                        Debug.Log("<color='yellow'>folder name inside data folder: " + d.Name + "</color>");
                    }
                    Debug.Log("<color='yellow'>dlc folder's inside possible folder check done</color>");
                }

                if (allFiles != null && allFiles.Length > 0)
                {
                    Debug.Log("<color='green'>let us print all dlc file's names.</color>");
                    foreach (var d in allFiles)
                    {
                        Debug.Log("<color='green'>inside dlc folder there is this file: " + d.Name + "</color>");
                    }
                    Debug.Log("<color='green'>dlc folder's inside all dlc file printing done</color>");
                }
                else
                {
                    Debug.Log("<color='red'>There is no files inside dlc folder!</color>");
                }
            }
        }
    }
}