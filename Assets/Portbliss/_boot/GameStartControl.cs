﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Portbliss.Ad;

public class GameStartControl : MonoBehaviour
{
    static GameStartControl instance;
    [SerializeField] float waitSecondsForGameScene = 5f;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            timer = 0;
            StartCoroutine(HoldForSDKInit());
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            DestroyImmediate(this);
        }
    }

    float timer = 0;
    float holdTimeLast = 0;
    const float splashDuration = 3f;
    IEnumerator HoldForSDKInit()
    {
        while (true)
        {
            timer += Time.deltaTime;
            if ((AdController.IsSDK_Ready && LevelLoader.instance != null && LevelLoader.startLevelReady) || timer > waitSecondsForGameScene)
            {
                holdTimeLast = timer;
                timer = 0;
                break;
            }
            yield return null;
        }

        float wT = 0;
        if (holdTimeLast > splashDuration)
        {
            wT = 0.1f;
        }
        else
        {
            wT = Mathf.Abs(holdTimeLast - splashDuration);
        }
        yield return new WaitForSeconds(wT);
        UnityEngine.SceneManagement.SceneManager.LoadScene(1);
        StopAllCoroutines();
    }
}
