﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using com.adjust.sdk;

public class AdjustSDKWrapper : MonoBehaviour
{
    static AdjustSDKWrapper instance;
    const string iOS_AppToken = "q34c9jplifwg";
    const string android_AppToken = "943zokxna874";
    [SerializeField] AdjustEnvironment environment = AdjustEnvironment.Sandbox;
    [SerializeField] AdjustLogLevel logLevel = AdjustLogLevel.Verbose;
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;

            //#if UNITY_ANDROID
            //            GameObject g = new GameObject();
            //            g.name = "AppsFlyerTrackerCallbacks";
            //            g.AddComponent<AppsFlyerTrackerCallbacks>();
            //#endif

            DontDestroyOnLoad(gameObject);
        }
        else
        {
            if (instance.gameObject != gameObject)
            {
                DestroyImmediate(this);
            }
        }
    }


    //    void Start()
    //    {

    //        /* Mandatory - set your AppsFlyer’s Developer key. */
    //        AppsFlyer.setAppsFlyerKey("zcKrZYJWnrWWctCxcLNnyT");
    //        /* For detailed logging */
    //        /* AppsFlyer.setIsDebug (true); */
    //#if UNITY_IOS
    //  /* Mandatory - set your apple app ID
    //   NOTE: You should enter the number only and not the "ID" prefix */
    //  AppsFlyer.setAppID ("1471374791");
    //  AppsFlyer.trackAppLaunch ();
    //#elif UNITY_ANDROID
    //        /* Mandatory - set your Android package name */
    //        AppsFlyer.setAppID("com.portbliss.ipeelgood");
    //        /* For getting the conversion data in Android, you need to add the "AppsFlyerTrackerCallbacks" listener.*/
    //        AppsFlyer.init("zcKrZYJWnrWWctCxcLNnyT", "AppsFlyerTrackerCallbacks");
    //        AppsFlyer.createValidateInAppListener("AppsFlyerTrackerCallbacks", "onInAppBillingSuccess", "onInAppBillingFailure");
    //#endif
    //    }


    void Start()
    {
#if UNITY_IOS
        /* Mandatory - set your iOS app token here */
        var adjustAppToken = iOS_AppToken;
#elif UNITY_ANDROID
        /* Mandatory - set your Android app token here */
        var adjustAppToken = android_AppToken;
#endif
        var config = new AdjustConfig( adjustAppToken, environment, true);
        config.setLogLevel(logLevel); // AdjustLogLevel.Suppress to disable logs
        config.setSendInBackground(true);
        new GameObject("Adjust").AddComponent<Adjust>(); // do not remove or rename
                                                         // Adjust.addSessionCallbackParameter("foo", "bar"); // if requested to set session - level parameters
        Adjust.start(config);
        
    }
}