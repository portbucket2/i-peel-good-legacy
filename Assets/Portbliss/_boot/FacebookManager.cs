﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Facebook.Unity;

public class FacebookManager : MonoBehaviour
{
    static FacebookManager instance;
    static bool fbInitDone;
    // Start is called before the first frame update
    void Awake()
    {
        fbInitDone = false;
        if (instance == null)
        {
            instance = this;
            
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            if (instance.gameObject != gameObject)
            {
                DestroyImmediate(this);
            }
        }

        if (!FB.IsInitialized)
        {
            // Initialize the Facebook SDK
            FB.Init(()=> {
                fbInitDone = true;
                FB.ActivateApp();
            });
        }
        else
        {
            fbInitDone = true;
            // Already initialized, signal an app activation App Event
            FB.ActivateApp();
        }
    }

    public static void LogLevelCompleted(int levelnumber, string object_name, string toolName, string levelType)
    {
       

        if (fbInitDone == false) { return; }
        var Params = new Dictionary<string, object>();
        Params[AppEventParameterName.Level] = ""+ levelnumber;
        Params[AppEventParameterName.ContentID] = "" + object_name;
        Params["tool_name"] = "" + toolName;
        Params["level_type"] = "" + levelType;

        FB.LogAppEvent(AppEventName.AchievedLevel, null, Params);
        //Debug.Log("loglevelcompleted"+levelnumber + object_name);

    }

    public static void LogLevelSkipOrCompletedFull(int levelnumber, string object_name, string toolName, string completiontype, string levelType)
    {


        if (fbInitDone == false) { return; }
        var Params = new Dictionary<string, object>();
        Params[AppEventParameterName.Level] = "" + levelnumber;
        Params[AppEventParameterName.ContentID] = "" + object_name;
        Params["Completion_Type"] = "" + completiontype;
        Params["tool_name"] = "" + toolName;
        Params["level_type"] = "" + levelType;


        FB.LogAppEvent("Level Completed Common", null, Params);
        Debug.Log("Level Completed Skip+3Stars" + levelnumber + object_name+ toolName);

    }


    public static void LogLevelSkipped(int levelnumber, string object_name, string toolName, string levelType)
    {
        if (fbInitDone == false) { return; }
        var Params = new Dictionary<string, object>();
        Params[AppEventParameterName.Level] = "" + levelnumber;
        Params[AppEventParameterName.ContentID] = "" + object_name;
        Params["tool_name"] = "" + toolName;
        Params["level_type"] = "" + levelType;

        FB.LogAppEvent("Level Skipped", null, Params);
        //Debug.Log("loglevelskipped" + levelnumber + object_name);

    }


    public static void LogLevelStarted(int levelnumber, string object_name, string toolName, string levelType)
    {
        if (fbInitDone == false) { return; }
        var Params = new Dictionary<string, object>();
        Params[AppEventParameterName.Level] = "" + levelnumber;
        Params[AppEventParameterName.ContentID] = "" + object_name;
        Params["tool_name"] = "" + toolName;
        Params["level_type"] = "" + levelType;

        FB.LogAppEvent("Level Started", null, Params);
        //Debug.Log("LogLevelStarted" + levelnumber + object_name);

    }

    public static void LogLevelReStarted(int levelnumber = 0)
    {
        if (fbInitDone == false) { return; }
        var Params = new Dictionary<string, object>();
        Params[AppEventParameterName.Level] = "" + levelnumber;

        FB.LogAppEvent("Level ReStarted", null, Params);

    }

    public static void LogLevelFailed(int levelnumber = 0)
    {
        if (fbInitDone == false) { return; }
        var Params = new Dictionary<string, object>();
        Params[AppEventParameterName.Level] = "" + levelnumber;

        FB.LogAppEvent("Level Failed", null, Params);

    }

    public static void LogToolUnlocked(int tool_index, string tool_name, int levelnumber)
    {
        if (fbInitDone == false) { return; }
        var Params = new Dictionary<string, object>();
        Params[AppEventParameterName.Level] = "" + levelnumber;
        Params[AppEventParameterName.ContentID] = "" + string.Format("{0}_{1}",tool_index, tool_name);

        FB.LogAppEvent("Tool Unlocked", null, Params);
        //Debug.Log("LogLevelStarted" + levelnumber + object_name);

    }
    public static void LogRewardADClicked(int levelnumber, string levelType)
    {
        if (fbInitDone == false) { return; }
        var Params = new Dictionary<string, object>();
        Params[AppEventParameterName.Level] = "" + levelnumber;
        Params["level_type"] = "" + levelType;

        FB.LogAppEvent("Reward AD Click", null, Params);
        //Debug.Log("LogLevelStarted" + levelnumber + object_name);

    }

    public static void LogRewardedVideoAdStart(int levelnumber)
    {
        if (fbInitDone == false) { return; }
        var Params = new Dictionary<string, object>();
        Params[AppEventParameterName.Level] = "" + levelnumber;
        FB.LogAppEvent("Rewarded Video Start", null, Params);
    }
    
    public static void LogRewardedVideoAdComplete(int levelnumber)
    {
        if (fbInitDone == false) { return; }
        var Params = new Dictionary<string, object>();
        Params[AppEventParameterName.Level] = "" + levelnumber;
        FB.LogAppEvent("Rewarded Video Complete", null, Params);
    }

    public static void LogNotificationAccepted(string status)
    {
        if (fbInitDone == false) { return; }
        var Params = new Dictionary<string, object>();
        Params[AppEventParameterName.ContentID] = status;


        FB.LogAppEvent("Notification Accepted", null, Params);

    }

    public static void LogAdMode(string mode)
    {
        if (fbInitDone == false) { return; }
        var Params = new Dictionary<string, object>();
        Params[AppEventParameterName.ContentID] = mode;


        FB.LogAppEvent("peel_ad_intensity_type", null, Params);
    }

    public static void LogSkipMode(string mode)
    {
        if (fbInitDone == false) { return; }
        var Params = new Dictionary<string, object>();
        Params[AppEventParameterName.ContentID] = mode;


        FB.LogAppEvent("Allow_Skip", null, Params);
    }

    public static void LogObjectSelected(string Name)
    {
        if (fbInitDone == false) { return; }
        var Params = new Dictionary<string, object>();
        Params[AppEventParameterName.ContentID] = Name;


        FB.LogAppEvent("Object Selected", null, Params);

    }

    public static void LogSpeedChanged(string speed)
    {
        if (fbInitDone == false) { return; }

        var Params = new Dictionary<string, object>();
        Params[AppEventParameterName.ContentID] = speed;

        FB.LogAppEvent("Speed Changed", null, Params);

    }

    public static void LogABTesting(string abType, int abValue)
    {
        if (fbInitDone == false) { return; }
        var Params = new Dictionary<string, object>();
        Params[AppEventParameterName.Level] = "" + abValue;

        FB.LogAppEvent(abType, null, Params);

    }
}
