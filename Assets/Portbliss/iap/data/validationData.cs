﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Portbliss.IAP
{
    [System.Serializable]
    public class IAP_RecieptObject_Android
    {
        public string Store;
        public string TransactionID;
        [Multiline]
        public string Payload;
    }


    [System.Serializable]
    public class IAP_PayloadObject_Android
    {
        [Multiline]
        public string json;
        [Multiline]
        public string signature;
        [Multiline]
        public string skuDetails;
        public bool isPurchaseHistorySupported;
    }
}