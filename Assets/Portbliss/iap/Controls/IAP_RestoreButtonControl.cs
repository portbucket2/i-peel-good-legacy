﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Portbliss.IAP
{
    public class IAP_RestoreButtonControl : MonoBehaviour
    {
        [SerializeField] Button restoreIAP_Btn;
        [SerializeField] Button[] allIapButtons;
        [SerializeField] GameObject restoreLoadingEffect;
        // Start is called before the first frame update
        void Start()
        {

#if UNITY_ANDROID
            restoreIAP_Btn.gameObject.SetActive(false);
#elif UNITY_IOS
            if (GameConfig.hasIAP_Restored_iOS == null) { return; }
            restoreIAP_Btn.gameObject.SetActive(!(GameConfig.hasIAP_Restored_iOS.value && GameConfig.hasIAP_NoAdPurchasedHD.value));
            restoreIAP_Btn.onClick.RemoveAllListeners();
            restoreIAP_Btn.onClick.AddListener(() =>
            {
                restoreLoadingEffect.SetActive(true);
                IAP_Controller.instance.RestoreIAPForApple((success) =>
                {
                    restoreLoadingEffect.SetActive(false);
                    if (success && GameConfig.hasIAP_NoAdPurchasedHD.value)
                    {
                        restoreIAP_Btn.gameObject.SetActive(false);
                        foreach (var b in allIapButtons)
                        {
                            b.gameObject.SetActive(false);
                        }
                    }
                });
            });
#endif
        }
    }
}