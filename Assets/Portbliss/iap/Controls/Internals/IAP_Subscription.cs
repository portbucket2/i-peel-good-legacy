﻿using UnityEngine.Purchasing;
using Portbliss.IAP.Internal;

namespace Portbliss.IAP
{
    public class IAP_Subscription : IIAP_Buy
    {
        bool logEnabled;
        IAP_Controller IAPController;
        internal IAP_Subscription(IAP_Controller iAPController)
        {
            IAPController = iAPController;
            logEnabled = iAPController.LogEnabled;
        }

        void BuyCore(string productID)
        {
            if (IAPController.IsSubscriptionValid(productID) == false)
            {
                LogUtil.Red("you are trying to buy Subscription product: " + productID +
                    " which are not defined in the controller script. Won't be successful!", logEnabled);
                return;
            }
            LogUtil.Green("lets try to buy Subscription product: " + productID, logEnabled);
            IAP_Core.BuyProductID(productID);
        }

        public void BuyProduct(string productID)
        {
            LogUtil.Green("We will try to buy Subscription product: " + productID + " But first, lets wait " +
                "for any restore purchase operation that is running currently.", logEnabled);
            IAPController.WaitForRestoreIfAny(() =>
            {
                Product product = IAPController.StoreController.products.WithID(productID);
                if (product.hasReceipt == true && string.IsNullOrEmpty(product.transactionID) == false)
                {
                    LogUtil.Green("we have the reciept of Subscription product"
                        + productID + ". So lets exit with success!", logEnabled);
                    IAPController.ExitIAP_WithSuccess(productID);
                }
                else
                {
                    LogUtil.Green("lets try to buy Subscription product: " + productID
                       + " But let us first check if we restored purchases yet or not.", logEnabled);
                    if (IAP_Controller.HasRestoredIAP())
                    {
                        LogUtil.Green("we already restored purchases. " +
                            "So we can gladly initiate purchase process " +
                            "through 'BuyCore()' for Subscription productID: " + productID, logEnabled);
                        BuyCore(productID);
                    }
                    else
                    {
                        LogUtil.Green("we can not buy Subscription product: " + productID
                        + " , before we restored the purchase. we are doing to now.", logEnabled);
                        IAP_Restore.RestorePurchases((restoreSuccess) =>
                        {
                            LogUtil.Green("purchase restored, harddata flag saved. We now will check the product reciept again," +
                                "Since we restored the IAP. " +
                                "If we do not have reciept, only then we send buy request to apple." +
                                "For Subscription product ID:" + productID, logEnabled);
                            if (IAPController.IsProductPurchased(productID))
                            {
                                LogUtil.Green("we have the reciept of Subscription product"
                        + productID + ". So lets exit with success!", logEnabled);
                                IAPController.ExitIAP_WithSuccess(productID);
                            }
                            else
                            {
                                LogUtil.Green("finally let us buy Subscription product: "
                        + productID + "", logEnabled);
                                BuyCore(productID);
                            }
                        });
                    }
                }
            });
        }
    }
}