﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class soundManagement : MonoBehaviour
{
    public bool peeling
    {
        get
        {
            return PeelableController.instance.focusNow;
        }
    }
    
    public bool peelingDummy
    {
        get
        {
            return MeshPeeler.instance.IsGeneratingPeelNow();
        }
    }

    

    public bool peelingDummyDummy;

    //public bool peelingDummyDummyDum;

    public bool peelDetect;

    public AudioSource peelerAudioSource;
    public AudioSource peelerAudioSource2nd;

    public AudioClip[] peelingAudios;
    public AudioClip[] peelerMetalAudio;

    

    float volume;
    public int clipIndex;
    public int maxClipIndex;
    float pitch;
    public Text pitchText;
    public Text clipText;
    public GameObject soundCanvas;

    public bool TESTING;
    public bool soundOn
    {
        get
        {
            return soundToggle.isOn;
        }
    }

    public Toggle soundToggle;

    UniBliss.HardData<bool> soundEnabledHD;
    // Start is called before the first frame update
    void Start()
    {
        pitch = 1;
        pitchText.text = "" + pitch;
        clipText.text = "" + peelerAudioSource.clip.name;
        maxClipIndex = peelingAudios.Length;

        peelerAudioSource.clip = peelingAudios[clipIndex];
        peelerAudioSource.Play();

        peelerAudioSource.pitch = pitch;

        peelerAudioSource.volume = 0;

        peelerAudioSource2nd.volume = 0;
        soundCanvas.SetActive(false);

        soundEnabledHD = new UniBliss.HardData<bool>("SOUNDS_ENABLED",true);
        soundToggle.isOn = soundEnabledHD.value;
        soundToggle.onValueChanged.AddListener(ShawOnToggleChanged);
    }
    void ShawOnToggleChanged(bool newValue)
    {
        soundEnabledHD.value = newValue;
    }

    // Update is called once per frame
    void Update()
    {
        

        if (soundOn)
        {
            PlayAudio();
            DetectContact();

            //peelingDummyDummyDum = peeling;

            if (clipIndex != (int)MeshPeeler.instance.soundType && !TESTING)
            {
                clipIndex = (int)MeshPeeler.instance.soundType;
                peelerAudioSource.clip = peelingAudios[clipIndex];
                pitch = MeshPeeler.instance.soundPitch;
                peelerAudioSource.pitch = pitch;
                peelerAudioSource.Play();
            }

            if (TESTING)
            {
                soundCanvas.SetActive(true);
            }
            else
            {
                soundCanvas.SetActive(false);
            }
        }
        

        //if(MeshPeeler.instance != null)
        //{
        //
        //}

        
        //Debug.Log("PPPPPPPPPP"+MeshPeeler.instance.soundType);



    }

    void PlayAudio()
    {
        if (peelingDummy && Input.GetMouseButton(0))
        {
            volume = Mathf.Lerp(volume, 1 , Time.deltaTime* 6);
            peelDetect = true;
        }
        else
        {
            if (peeling)
            {
                volume = Mathf.Lerp(volume, 0.1f, Time.deltaTime * 10);
            }
            else
            {
                volume = Mathf.Lerp(volume, 0, Time.deltaTime * 10);
            }
            peelDetect = false;
        }
        peelerAudioSource.volume = volume;

        //peelerAudioSource.clip = peelingAudios[clipIndex];
        //peelerAudioSource.pitch = pitch;


        

    }

    void DetectContact()
    {
        if (peelingDummyDummy!= peelDetect)
        {
            if (!peelDetect)
            {
                peelerAudioSource2nd.clip = peelerMetalAudio[1];
                peelerAudioSource2nd.Play();
                
            }
            else
            {
                peelerAudioSource2nd.clip = peelerMetalAudio[0];
                peelerAudioSource2nd.Play();
                //peelerAudioSource.Play();
            }
            

            peelingDummyDummy = peelDetect;
        }
    }

    public void ChangeClip()
    {
        if(clipIndex < maxClipIndex-1)
        {
            clipIndex += 1;
        }
        else
        {
            clipIndex = 0;
        }

        peelerAudioSource.clip = peelingAudios[clipIndex];

        peelerAudioSource.Play();
        clipText.text = "" + peelerAudioSource.clip.name;

    }

    public void PitchPlus()
    {
        pitch += 0.1f;
        pitchText.text = "" + pitch;

        peelerAudioSource.pitch = pitch;
    }

    public void PitchMinus()
    {
        pitch -= 0.1f;
        pitchText.text = "" + pitch;

        peelerAudioSource.pitch = pitch;
    }

    public void MetalSoundOff()
    {
        if (peelerAudioSource2nd.volume == 0 )
        {
            peelerAudioSource2nd.volume = 0.1f;
        }
        else
        {
            peelerAudioSource2nd.volume = 0;
        }
        
    }
}


public enum SoundTrackType
{
    BANANA = 0,
    ORANGE = 1,
    ONION = 2,
    CORN =3,
    CUCUM = 4,
}