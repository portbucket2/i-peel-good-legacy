#if UNITY_ANDROID || UNITY_IPHONE || UNITY_STANDALONE_OSX || UNITY_TVOS
// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class GooglePlayTangle
    {
        private static byte[] data = System.Convert.FromBase64String("xDax/x1xcly5tYrEHeVaROn9mEAcjpR2vlvyqSMFJWWfjKnBhuYqqvdn6TA1mXom8D8By0uw4SiEd//Pix+4XBBg/7E1YeH7DIi9ehTtvjlh01BzYVxXWHvXGdemXFBQUFRRUureb/F6jhjsBwBoiLTrpNin5vh9RSuheIa3VnfO2df8Y/VlGNGGZOEsitRKbLQDclOExbnHNML3l14rgtNQXlFh01BbU9NQUFHc+kfxGksk+2J0Tn+n5w9tNL28mLAzdm9caJn0mkKzNmV9vP1x3ZpayO5IP6VIWaSriqBQJJNwtUW/qkYiangNW/zeiuR6P8IDaQ3L9gD6cZUEs7cMrcV77SYytH7mEsc9a7nS3lGUcHwD5SmX6z96Ps8MFlNSUFFQ");
        private static int[] order = new int[] { 12,3,10,11,12,7,7,9,11,13,12,11,12,13,14 };
        private static int key = 81;

        public static readonly bool IsPopulated = true;

        public static byte[] Data() {
        	if (IsPopulated == false)
        		return null;
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
#endif
