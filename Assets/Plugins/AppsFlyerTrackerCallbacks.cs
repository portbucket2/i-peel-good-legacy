﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class AppsFlyerTrackerCallbacks : MonoBehaviour {

	static AppsFlyerTrackerCallbacks instance;
	static Action<bool> ValidationCallback;

	public static void SendValidationCallback(Action<bool> OnValidate)
	{
		ValidationCallback = OnValidate;
	}
	private void Awake()
	{
		if (instance == null)
		{
			instance = this;
			DontDestroyOnLoad(gameObject);
		}
		else
		{
			if (instance.gameObject.name == "AppsFlyerTrackerCallbacks" && instance.gameObject != gameObject)
			{
				DestroyImmediate(this);
			}
		}
	}

	// Use this for initialization
	void Start () {
		printCallback("AppsFlyerTrackerCallbacks on Start");
	}
	
	public void didReceiveConversionData(string conversionData) {
		printCallback ("AppsFlyerTrackerCallbacks:: got conversion data = " + conversionData);
	}
	
	public void didReceiveConversionDataWithError(string error) {
		printCallback ("AppsFlyerTrackerCallbacks:: got conversion data error = " + error);
	}
	
	public void didFinishValidateReceipt(string validateResult) {
		printCallback ("AppsFlyerTrackerCallbacks:: got didFinishValidateReceipt  = " + validateResult);
		ValidationCallback?.Invoke(true);
		ValidationCallback = null;
	}
	
	public void didFinishValidateReceiptWithError (string error) {
		printCallback ("AppsFlyerTrackerCallbacks:: got idFinishValidateReceiptWithError error = " + error);
		ValidationCallback?.Invoke(false);
		ValidationCallback = null;
	}
	
	public void onAppOpenAttribution(string validateResult) {
		printCallback ("AppsFlyerTrackerCallbacks:: got onAppOpenAttribution  = " + validateResult);
		
	}
	
	public void onAppOpenAttributionFailure (string error) {
		printCallback ("AppsFlyerTrackerCallbacks:: got onAppOpenAttributionFailure error = " + error);
		
	}
	
	public void onInAppBillingSuccess () {
		printCallback ("AppsFlyerTrackerCallbacks:: got onInAppBillingSuccess succcess");
		ValidationCallback?.Invoke(true);
		ValidationCallback = null;
	}
	public void onInAppBillingFailure (string error) {
		printCallback ("AppsFlyerTrackerCallbacks:: got onInAppBillingFailure error = " + error);
		ValidationCallback?.Invoke(false);
		ValidationCallback = null;
	}

	public void onInviteLinkGenerated (string link) {
		printCallback("AppsFlyerTrackerCallbacks:: generated userInviteLink "+link);
	}

	public void onOpenStoreLinkGenerated (string link) {
		printCallback("onOpenStoreLinkGenerated:: generated store link "+link);
		Application.OpenURL(link);
	}

	void printCallback(string str) {
		Debug.Log("<color='magenta'>" + str + "</color>");
	}
}
