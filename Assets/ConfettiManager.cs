﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConfettiManager : MonoBehaviour
{
    public static ConfettiManager instance;
    public List<ParticleSystem> particles;
    // Start is called before the first frame update
    void Start()
    {
        instance = this;
    }

    

    // Update is called once per frame
    public static void Play()
    {
        foreach (var item in instance.particles)
        {
            item.Play();
        }

        
    }

    public void ClearParticles ()
    {
        foreach ( var item in instance.particles )
        {
            item.Stop();
            item.Clear ();
        }
    }
}
