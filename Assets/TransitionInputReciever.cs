﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransitionInputReciever : MonoBehaviour
{
    public static event System.Action initialInputRecieved;
    public GameState fromState = GameState.INITIAL;
    void Update()
    {
        if (MainGameManager.state == fromState)
        {
            if (Input.GetMouseButtonUp(0)||(LevelLoader.Last_ai == 0 && LevelLoader.Last_li==0))
            {
                MainGameManager.instance.ChangeLevelObjectState(GameState.GAME);
                initialInputRecieved?.Invoke();
                AppLovinCrossPromo.Instance().HideMRec();
            }
        }

    }
}
