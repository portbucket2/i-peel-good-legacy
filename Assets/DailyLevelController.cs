﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DailyLevelController : MonoBehaviour
{
    [SerializeField]Text timer;
    [SerializeField]Button dailybutton;
    [SerializeField]RectTransform shakee;

    [SerializeField] Image buttonImage;

    [SerializeField] float mass = 1;
    [SerializeField] float viscousity = 1;
    [SerializeField] float rigidity = 1;

    float exponent;
    float angularvelocity;


    [SerializeField] float period = 2;
    [SerializeField] float initialangle = 30;

    private void Awake ()
    {
        exponent = -1 * viscousity / ( 2 * mass );
        angularvelocity = Mathf.Sqrt ( ( rigidity / mass ) - ( exponent * exponent ) );
        dailybutton.onClick.AddListener(OnButtonClick);
        //() =>
        //{
        //    MainGameManager.instance.areaLoader.LoadLevelList ( LoadType.DAILY, 0 );
        //} );
    }

    public void OnButtonClick()
    {

        if (DailyLevelManagement.instance.AllLevelsPlayed())
        {
            DialogueAcceptReject.instance.Load_2Choice("Daily Levels", "Watch ads to unlock daily levels right now?", () =>
            {
                if (Application.isEditor)
                {
                    OnAdCallback(true);
                }
                else
                {

                    Portbliss.Ad.AdController.ShowRewardedVideoAd(OnAdCallback);
                }
            });
        }
        else
        {
            int di = DailyLevelManagement.instance.GetNextUnlockedIndex();
            LevelDefinition levelDef = DailyLevelManagement.instance.GetLevel(di);
            LevelLoader.LoadA_Level(levelDef, (GameObject resource) =>
            {
                if (resource) MainGameManager.instance.LoadLevelFromSelection(LoadType.DAILY, 0, di, levelDef, resource);
            });
        }
    }

    void OnAdCallback(bool success)
    {
        if (success)
        {
            //Debug.LogError("AAAAAAAAAAAAAAAAAAAA");
            DailyLevelManagement.instance.ForceRefreshDailyLevels();


        }
    }

    // Start is called before the first frame update

    void Start()
    {
        //if(shakee)StartCoroutine ( startoscilation () );
    }

    // Update is called once per frame
    void Update()
    {
        timer.text = DailyLevelManagement.instance.RefreshTimeLeft ();
        transform.GetChild ( 0 ).gameObject.SetActive ( MainGameManager.instance.runningLevelManager.loadType != LoadType.DAILY );
        //dailybutton.interactable = !DailyLevelManagement.instance.AllLevelsPlayed();
        //buttonImage.color = DailyLevelManagement.instance.AllLevelsPlayed() ? new Color(1,1,1,0.5f): Color.white;

        passedtime += Time.deltaTime;

        if ( passedtime > period )
        {
            passedtime = 0;
        }



        if ( passedtime >= 0 )
        {
            float angle = initialangle * Mathf.Exp ( exponent * passedtime ) * Mathf.Cos ( angularvelocity * passedtime );

            if ( DailyLevelManagement.instance && DailyLevelManagement.instance.starsHD.Length > 0 && DailyLevelManagement.instance.starsHD[0] != null && DailyLevelManagement.instance.starsHD[0].value == 0 )
                shakee.eulerAngles = new Vector3 ( 0, 0, angle );
        }

    }

    float passedtime = -1.5f;

    IEnumerator startoscilation ()
    {
        while ( true )
        {
            yield return StartCoroutine ( Oscilate () );
        }
    }

    IEnumerator Oscilate ()
    {
        float passedtime = 0;

        while ( passedtime < period )
        {
            passedtime += Time.deltaTime;

            exponent = -1 * viscousity / ( 2 * mass );
            angularvelocity = Mathf.Sqrt ( ( rigidity / mass ) - ( exponent * exponent ) );

            float angle = initialangle * Mathf.Exp ( exponent * passedtime ) * Mathf.Cos ( angularvelocity * passedtime );

            if( DailyLevelManagement.instance && DailyLevelManagement.instance.starsHD.Length>0 && DailyLevelManagement.instance.starsHD[0]!=null && DailyLevelManagement.instance.starsHD[0].value==0)shakee.eulerAngles = new Vector3 ( 0, 0, angle );

            yield return null;
        }

    }
}
