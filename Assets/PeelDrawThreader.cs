﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading;
using UniBliss;

public static class PeelDrawThreader
{
    const int MAX_LENGTH = 1000;
    static PeelFiberStruct[] fibers;
    static int ARC_N;
    static float half_thickness;
    static float frontThickness;
    static float backThickness;
    static int Count;

    static List<Vector3> front_vertices = new List<Vector3>(5000);
    static List<Vector3> front_normals = new List<Vector3>(5000);
    static List<int> front_triangles = new List<int>(20000);
    static List<Vector2> front_uvs = new List<Vector2>(5000);

    static List<Vector3> back_vertices = new List<Vector3>(5000);
    static List<Vector3> back_normals = new List<Vector3>(5000);
    static List<int> back_triangles = new List<int>(20000);
    static List<Vector2> back_uvs = new List<Vector2>(5000);

    static bool initialzied = false;
    static void InitIfUninitialized()
    {
        if (initialzied) return;
        fibers = new PeelFiberStruct[1000];
        //thread1 = new Thread(ThreadLoop_Front);
        //thread2 = new Thread(ThreadLoop_Back);
        //thread1.Start();
        //thread2.Start();
        initialzied = true;
    }


    static Thread thread1;
    static Thread thread2;





    public static void PrepareForWork(List<PeelFiber> fiberClassList, int ARC_N,float half_thickness,float frontThickness, float backThickness)
    {
        if (fiberClassList.Count > MAX_LENGTH)
        {
            throw new System.Exception("Fiber list too long!");
        }
        InitIfUninitialized();
        PeelFiber pf;
        Count = 0;
        for (int i = 0; i < fiberClassList.Count; i++)
        {
            pf = fiberClassList[i];
            Count++;

            fibers[i].halfPeelWidth = pf.halfPeelWidth;
            fibers[i].lastRecordedLength = pf.lastRecordedLength;

            fibers[i].basePoint = pf.basePoint;
            fibers[i].normal = pf.normal;
            fibers[i].uv_U = pf.uv_U;
            fibers[i].uv_D = pf.uv_D;

            fibers[i].rootOffset = pf.rootOffset;
            fibers[i].upOffset = pf.upOffset;

            fibers[i].ShiftedPoint = pf.ShiftedPoint;
            fibers[i].UpPosFront = pf.UpPosFront;
            fibers[i].DownPosFront = pf.DownPosFront;
            fibers[i].UpPosBack = pf.UpPosBack;
            fibers[i].DownPosBack = pf.DownPosBack;
        }

        PeelDrawThreader.ARC_N = ARC_N;
        PeelDrawThreader.half_thickness = half_thickness;
        PeelDrawThreader.frontThickness = frontThickness;
        PeelDrawThreader.backThickness = backThickness;


    }
    public static void StartWork(MonoBehaviour mono, Mesh fiberFrontMesh, Mesh fiberBackMesh, System.Action callback)
    {
        completeFlag1 = false;
        completeFlag2 = false;
        //threadLoaded_front = true;
        //threadLoaded_back = true;
        thread1 = new Thread(UpdateFrontMesh);
        thread2 = new Thread(UpdateBackMesh);
        thread1.Start();
        thread2.Start();
        mono.StartCoroutine(CheckState(()=> { OnComplete(fiberFrontMesh, fiberBackMesh, callback); }));
    }
    static void OnComplete(Mesh fiberFrontMesh, Mesh fiberBackMesh, System.Action callback)
    {
        //Debug.Log("===============================Reached Thread OnComplete");
        fiberFrontMesh.SetVertices(front_vertices);
        fiberFrontMesh.SetNormals(front_normals);
        fiberFrontMesh.SetTriangles(front_triangles,0);
        fiberFrontMesh.SetUVs(0, front_uvs);

        fiberBackMesh.SetVertices(back_vertices);
        fiberBackMesh.SetNormals(back_normals);
        fiberBackMesh.SetTriangles(back_triangles,0);
        fiberBackMesh.SetUVs(0,back_uvs);

        callback?.Invoke();
    }

    /*static bool threadLoaded_front;
    static void ThreadLoop_Front()
    {
        while (true)
        {
            if (threadLoaded_front)
            {
                UpdateFrontMesh();
                threadLoaded_front = false;
            }
            Thread.Sleep(1);
        }
    }
    static bool threadLoaded_back;
    static void ThreadLoop_Back()
    {
        while (true)
        {
            if (threadLoaded_back)
            {
                UpdateBackMesh();
                threadLoaded_back = false;
                Thread.Sleep(1);
            }
        }
    }*/

    static bool completeFlag1;
    static bool completeFlag2;
    static IEnumerator CheckState(System.Action threadTaskDone)
    {
        yield return null;
        while (!completeFlag1 || !completeFlag2)
        {
            Debug.LogFormat("1 {0}, 2 {1}",completeFlag1,completeFlag2);
            yield return null;
        }
        threadTaskDone?.Invoke();
    }




    static void UpdateFrontMesh()
    {
        front_vertices.Clear();
        front_normals.Clear();
        front_uvs.Clear();
        front_triangles.Clear();
        int used_VXIndex = Count * 4;
        int used_TRIndex = 3 * (Count - 1) * 6;


        for (int i = 0; i < Count; i++)
        {
            front_vertices.Add(fibers[i].DownPosFront);
            front_vertices.Add(fibers[i].UpPosFront);
            front_vertices.Add(fibers[i].DownPosFront - fibers[i].normal * frontThickness);
            front_vertices.Add(fibers[i].UpPosFront - fibers[i].normal * frontThickness);

            front_normals.Add(fibers[i].normal);
            front_normals.Add(fibers[i].normal);
            front_normals.Add(-fibers[i].upOffset);
            front_normals.Add(fibers[i].upOffset);

            front_uvs.Add(fibers[i].uv_D);
            front_uvs.Add(fibers[i].uv_U);
            front_uvs.Add(fibers[i].uv_D);
            front_uvs.Add(fibers[i].uv_U);


            if (i == Count - 1)
            {
                continue;
            }
            else
            {
                //face
                front_triangles.Add(4 * i + 0);
                front_triangles.Add(4 * i + 1);
                front_triangles.Add(4 * i + 4);

                front_triangles.Add(4 * i + 4);
                front_triangles.Add(4 * i + 1);
                front_triangles.Add(4 * i + 5);


                //downBorder
                front_triangles.Add(4 * i + 6);
                front_triangles.Add(4 * i + 2);
                front_triangles.Add(4 * i + 0);

                front_triangles.Add(4 * i + 6);
                front_triangles.Add(4 * i + 0);
                front_triangles.Add(4 * i + 4);

                //upBorder
                front_triangles.Add(4 * i + 5);
                front_triangles.Add(4 * i + 1);
                front_triangles.Add(4 * i + 3);

                front_triangles.Add(4 * i + 5);
                front_triangles.Add(4 * i + 3);
                front_triangles.Add(4 * i + 7);
            }
        }




        int index0, index1, index0x, index1x;




        //start-front
        index0x = 2;
        index0 = 0;
        index1 = 1;
        index1x = 3;

        //Debug.LogFormat("budget {0}, arc {1}",vertices.Length-used_VXIndex, ARC_N);
        for (int i = 0; i <= ARC_N; i++)
        {
            if (i < ARC_N)
            {
                Vector3 offset;
                offset = Quaternion.AngleAxis(-(180.0f * (i + 1)) / (ARC_N + 1), fibers[0].normal) * fibers[0].upOffset;
                Vector3 vert = fibers[0].ShiftedPoint + offset + fibers[0].normal * half_thickness;
                front_vertices.Add(vert);
                front_vertices.Add(vert - fibers[0].normal * frontThickness);
                front_normals.Add(fibers[0].normal);
                front_normals.Add(offset);
                front_uvs.Add(fibers[0].uv_D);
                front_uvs.Add(fibers[0].uv_D);
            }
            int innerIndex = used_VXIndex + 2 * i;
            int outerIndex = used_VXIndex + 2 * i + 1;
            int k = 0;
            if (i == ARC_N)
            {
                innerIndex = index0;
                outerIndex = index0x;
            }
            else
            {
                front_triangles.Add(index1);
                front_triangles.Add(index0);
                front_triangles.Add(innerIndex);
            }


            front_triangles.Add(index1x);
            front_triangles.Add(index1);
            front_triangles.Add(outerIndex);

            front_triangles.Add(index1);
            front_triangles.Add(innerIndex);
            front_triangles.Add(outerIndex);

            index1 = innerIndex;
            index1x = outerIndex;

        }
        used_VXIndex += ARC_N * 2;
        used_TRIndex += (ARC_N * 9 + 6);




        //end-front
        index0x = Count * 4 - 2;
        index0 = Count * 4 - 4;
        index1 = Count * 4 - 3;
        index1x = Count * 4 - 1;

        //Debug.LogFormat("budget {0}, arc {1}",vertices.Length-used_VXIndex, ARC_N);
        for (int i = 0; i <= ARC_N; i++)
        {
            if (i < ARC_N)
            {
                Vector3 offset;
                offset = Quaternion.AngleAxis((180.0f * (i + 1)) / (ARC_N + 1), fibers[Count - 1].normal) * fibers[Count - 1].upOffset;
                Vector3 vert = fibers[Count - 1].ShiftedPoint + offset + fibers[Count - 1].normal * half_thickness;
                front_vertices.Add(vert);
                front_vertices.Add(vert - fibers[Count - 1].normal * frontThickness);
                front_normals.Add(fibers[Count - 1].normal);
                front_normals.Add(offset);
                front_uvs.Add(fibers[Count - 1].uv_D);
                front_uvs.Add(fibers[Count - 1].uv_D);
            }
            int innerIndex = used_VXIndex + 2 * i;
            int outerIndex = used_VXIndex + 2 * i + 1;
            int k = 0;
            if (i == ARC_N)
            {
                innerIndex = index0;
                outerIndex = index0x;
            }
            else
            {
                front_triangles.Add(index0);
                front_triangles.Add(index1);
                front_triangles.Add(innerIndex);
            }


            front_triangles.Add(index1);
            front_triangles.Add(index1x);
            front_triangles.Add(outerIndex);

            front_triangles.Add(index1);
            front_triangles.Add(outerIndex);
            front_triangles.Add(innerIndex);

            index1 = innerIndex;
            index1x = outerIndex;

        }
        used_VXIndex += ARC_N * 2;
        used_TRIndex += (ARC_N * 9 + 6);




        //PeelDrawThreader.front_vertices = front_vertices;
        //PeelDrawThreader.front_normals = front_normals;
        //PeelDrawThreader.front_triangles = front_triangles;
        //PeelDrawThreader.front_uvs = front_uvs;

        completeFlag1 = true;
    }
    static void UpdateBackMesh()
    {
        back_vertices.Clear();
        back_normals.Clear();
        back_uvs.Clear();
        back_triangles.Clear();
        int used_VXIndex = Count * 4;
        int used_TRIndex = 3 * (Count - 1) * 6;



        for (int i = 0; i < Count; i++)
        {
            back_vertices.Add(fibers[i].DownPosBack);
            back_vertices.Add(fibers[i].UpPosBack);
            back_vertices.Add(fibers[i].DownPosBack + fibers[i].normal * backThickness);
            back_vertices.Add(fibers[i].UpPosBack + fibers[i].normal * backThickness);

            back_normals.Add(-fibers[i].normal);
            back_normals.Add(-fibers[i].normal);
            back_normals.Add(-fibers[i].upOffset);
            back_normals.Add(fibers[i].upOffset);

            back_uvs.Add(fibers[i].uv_D);
            back_uvs.Add(fibers[i].uv_U);
            back_uvs.Add(fibers[i].uv_D);
            back_uvs.Add(fibers[i].uv_U);


            if (i == Count - 1)
            {
                continue;
            }
            else
            {
                //face
                back_triangles.Add(4 * i + 0);
                back_triangles.Add(4 * i + 4);
                back_triangles.Add(4 * i + 1);

                back_triangles.Add(4 * i + 1);
                back_triangles.Add(4 * i + 4);
                back_triangles.Add(4 * i + 5);


                //downBorder
                back_triangles.Add(4 * i + 2);
                back_triangles.Add(4 * i + 6);
                back_triangles.Add(4 * i + 0);

                back_triangles.Add(4 * i + 0);
                back_triangles.Add(4 * i + 6);
                back_triangles.Add(4 * i + 4);

                //upBorder
                back_triangles.Add(4 * i + 1);
                back_triangles.Add(4 * i + 5);
                back_triangles.Add(4 * i + 3);

                back_triangles.Add(4 * i + 3);
                back_triangles.Add(4 * i + 5);
                back_triangles.Add(4 * i + 7);
            }
        }


        int index0, index1, index0x, index1x;
        //start-back
        index0x = 2;
        index0 = 0;
        index1 = 1;
        index1x = 3;

        //Debug.LogFormat("budget {0}, arc {1}",vertices.Length-used_VXIndex, ARC_N);
        for (int i = 0; i <= ARC_N; i++)
        {
            if (i < ARC_N)
            {
                Vector3 offset;
                offset = Quaternion.AngleAxis(-(180.0f * (i + 1)) / (ARC_N + 1), fibers[0].normal) * fibers[0].upOffset;
                Vector3 vert = fibers[0].ShiftedPoint + offset - fibers[0].normal * half_thickness;
                back_vertices.Add(vert);
                back_vertices.Add(vert + fibers[0].normal * frontThickness);
                back_normals.Add(-fibers[0].normal);
                back_normals.Add(offset);
                back_uvs.Add(fibers[0].uv_D);
                back_uvs.Add(fibers[0].uv_D);
            }
            int innerIndex = used_VXIndex + 2 * i;
            int outerIndex = used_VXIndex + 2 * i + 1;
            int k = 0;
            if (i == ARC_N)
            {
                innerIndex = index0;
                outerIndex = index0x;
            }
            else
            {
                back_triangles.Add(index0);
                back_triangles.Add(index1);
                back_triangles.Add(innerIndex);
            }


            back_triangles.Add(index1);
            back_triangles.Add(index1x);
            back_triangles.Add(outerIndex);

            back_triangles.Add(index1);
            back_triangles.Add(outerIndex);
            back_triangles.Add(innerIndex);

            index1 = innerIndex;
            index1x = outerIndex;

        }
        used_VXIndex += ARC_N * 2;
        used_TRIndex += (ARC_N * 9 + 6);




        //end-back
        index0x = Count * 4 - 2;
        index0 = Count * 4 - 4;
        index1 = Count * 4 - 3;
        index1x = Count * 4 - 1;

        //Debug.LogFormat("budget {0}, arc {1}",vertices.Length-used_VXIndex, ARC_N);
        for (int i = 0; i <= ARC_N; i++)
        {
            if (i < ARC_N)
            {
                Vector3 offset;
                offset = Quaternion.AngleAxis((180.0f * (i + 1)) / (ARC_N + 1), fibers[Count - 1].normal) * fibers[Count - 1].upOffset;
                Vector3 vert = fibers[Count - 1].ShiftedPoint + offset - fibers[Count - 1].normal * half_thickness;
                back_vertices.Add(vert);
                back_vertices.Add(vert + fibers[Count - 1].normal * frontThickness);
                back_normals.Add(-fibers[Count - 1].normal);
                back_normals.Add(offset);
                back_uvs.Add(fibers[Count - 1].uv_D);
                back_uvs.Add(fibers[Count - 1].uv_D);
            }
            int innerIndex = used_VXIndex + 2 * i;
            int outerIndex = used_VXIndex + 2 * i + 1;
            int k = 0;
            if (i == ARC_N)
            {
                innerIndex = index0;
                outerIndex = index0x;
            }
            else
            {
                back_triangles.Add(index1);
                back_triangles.Add(index0);
                back_triangles.Add(innerIndex);
            }


            back_triangles.Add(index1x);
            back_triangles.Add(index1);
            back_triangles.Add(outerIndex);

            back_triangles.Add(index1);
            back_triangles.Add(innerIndex);
            back_triangles.Add(outerIndex);

            index1 = innerIndex;
            index1x = outerIndex;

        }
        used_VXIndex += ARC_N * 2;
        used_TRIndex += (ARC_N * 9 + 6);





        //PeelDrawThreader.back_vertices = back_vertices;
        //PeelDrawThreader.back_normals = back_normals;
        //PeelDrawThreader.back_triangles= back_triangles;
        //PeelDrawThreader.back_uvs = back_uvs;


        completeFlag2 = true;
    }
}

