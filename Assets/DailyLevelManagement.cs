﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniBliss;

public class DailyLevelManagement : MonoBehaviour
{

    public static DailyLevelManagement instance;
    private const int DAILY_COUNT = 5;
    private const int REFRESH_TIME_HOURS = 24;

    public HardData<System.DateTime> lastRefreshTime;

    public HardData<int>[] indicesHD;
    public HardData<int>[] starsHD;
    public HardData<bool> pushpermision;
    private LevelDefinition[] _definitions;

    bool reloadDefs;
    public LevelDefinition[] definitions
    {
        get
        {
            if ( _definitions == null || reloadDefs)
            {
                _definitions = new LevelDefinition[DAILY_COUNT];
                //Debug.Log(LevelLoader.instance.levelAreas[0].levelDefinition.Count);
                for (int i = 0; i < DAILY_COUNT; i++)
                {
                    //Debug.Log(indicesHD[i].value);
                    _definitions[i] = LevelLoader.instance.levelAreas[0].levelDefinition[indicesHD[i].value];
                }
            }


            return _definitions;
        }
    }


    public bool IsTimeToRefresh
    {
        get
        {
            //Debug.Log("Now " + System.DateTime.Now);
            //Debug.Log("Now " + lastLevelCompleteTime.value);
            //Debug.Log("Now " + System.DateTime.Now);
            if (indicesHD[0].value < 0)
            {
                return true;
            }
            else {
                return System.DateTime.Now - lastRefreshTime.value > new System.TimeSpan(REFRESH_TIME_HOURS, 0, 0) && starsHD[starsHD.Length - 1].value > 0;
            }
        }
    }

    public string RefreshTimeLeft ()
    {
        System.TimeSpan t = lastRefreshTime.value.Add ( new System.TimeSpan ( REFRESH_TIME_HOURS, 0, 0 ) ) - System.DateTime.Now;

        return t.TotalSeconds>0?string.Format("{0}:{1}:{2}", fixtwodigits ( t.Hours ) , fixtwodigits ( t.Minutes ) , fixtwodigits ( t.Seconds ) )  : "";//"00:00:00";
    }

    string fixtwodigits ( int inte )
    {
        if ( inte > 99 )
        {
            return fixtwodigits ( inte % 100 );
        }
        else if ( inte > 9 )
        {
            return inte.ToString ();
        }
        else if ( inte > 0 )
        {
            return string.Format( "0{0}" , inte.ToString ());
        }
        else
            return "00";
    }
    public bool IsAwaitingRefresh
    {
        get
        {
            if (IsTimeToRefresh) return false;
            else return (starsHD[DAILY_COUNT - 1].value > 0) ;
        }
    }
    // Start is called before the first frame update
    void Start ()
    {
        if (!instance)
        {
            instance = this;

            lastRefreshTime = new HardData<System.DateTime>("LAST_LEVEL_COMPLETE_TIME", System.DateTime.MinValue);
            indicesHD = new HardData<int>[DAILY_COUNT];
            starsHD = new HardData<int>[DAILY_COUNT];
            pushpermision = new HardData<bool>("PUSH_PERMISSION", false);
            for (int i = 0; i < DAILY_COUNT; i++)
            {
                indicesHD[i] = new HardData<int>(string.Format("DAILY_LEVEL_INDEX_{0}", i + 1), -1);
                starsHD[i] = new HardData<int>(string.Format("DAILY_LEVEL_STAR_{0}", i + 1), 0);
            }

            //Debug.Log(indicesHD[0].value + " " + IsTimeToRefresh);
            if (indicesHD[0].value < 0 && IsTimeToRefresh)
            {
                Debug.Log("Generating");
                GenerateAndStoreLevels();
            }
            var initData = Portbliss.Push.PushController.appOpenedWithThisData;
            if (initData != null)
            {
                //MainGameManager.instance.areaLoader.LoadLevelList ( LoadType.DAILY, 0 );
            }
        }
    }

    int multfactorhigh = 100;
    int mulfactorlow = 75;
    int[] GenerateNewDailyLevelIndices ()
    {
        int count = LevelLoader.GetCumulativeLevelCap ();
        int currentNumber = LevelDefinition.Query_CumulativeNumber(LevelLoader.Last_ai,LevelLoader.Last_li, LoadType.NORMAL) ;

        ChancedList<int> roll = new ChancedList<int> ();

        for ( int i = 1; i <= count; i++ )
        {
            if ( i < currentNumber )
            {
                roll.Add ( i,mulfactorlow * ( currentNumber - i ) * ( currentNumber - i ) );
            }
            else if ( i > currentNumber )
            {
                roll.Add ( i, multfactorhigh * ( i - currentNumber ) * ( i - currentNumber ) * ( i - currentNumber ) );
            }
        }

        List<int> storedindices = new List<int> ();

        while ( storedindices.Count < DAILY_COUNT )
        {
            int index = roll.Roll ()-1;

            if ( !storedindices.Contains ( index ) )
            {
                storedindices.Add ( index );
            }
        }

        return storedindices.ToArray ();
    }

    void GenerateAndStoreLevels ()
    {
        reloadDefs = true;
        int[] store = GenerateNewDailyLevelIndices ();
        lastRefreshTime.value = System.DateTime.Now;
        string title = "How You Peeling Today?";
        string subtitle = "";
        string description = "";
        Portbliss.Push.PushController.ScheduleNotification ( title, subtitle, description, REFRESH_TIME_HOURS, 0, 0, "extradata" );
        for ( int i = 0; i < store.Length; i++ )
        {
            Debug.Log(store[i]);
            indicesHD[i].value = store[i];
            //Debug.Log(indicesHD[i].value);
            starsHD[i].value = 0;
        }
    }

    public void ForceRefreshDailyLevels()
    {
        GenerateAndStoreLevels();
    }
    public LevelDefinition[] GetDailyLevels ()
    {
        if ( IsTimeToRefresh )
        {
            GenerateAndStoreLevels ();
        }

        return definitions;
    }

    public LevelDefinition GetLevel(int i)
    {
        if (i < 0 || i >= DAILY_COUNT)
            return null;
        else
            return GetDailyLevels()[i];
    }
    public void SetStarCount(int index, int count)
    {
        starsHD[index].value = count;

    }

    public int GetStarCount ( int index )
    {
        return starsHD[index].value;
    }

    public bool IsIndexUnLocked(int index) { return !(index!=0 && starsHD[index].value == 0 && starsHD[index - 1].value == 0); }

    public int GetNextUnlockedIndex()
    {
        int di = 0;
        for (int i = 0; i < definitions.Length; i++)
        {
            if (GetStarCount(i) > 0)
            {
                di = i+1;
            }
        }
        return di;
    }

    public bool AllLevelsPlayed()
    {
        for (int i = 0; i < definitions.Length; i++)
        {
            if (GetStarCount(i) == 0)
            {
                return false;
            }
        }
        return true;
    }


}