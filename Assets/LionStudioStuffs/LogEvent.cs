﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LogEvent : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void LevelCompleted()
    {
        //FacebookManager.LogLevelCompleted();
    }

    public void LevelStarted() 
    {
        //FacebookManager.LogLevelStarted(); 
    }

    public void LevelFailed()
    {
        FacebookManager.LogLevelFailed();
    }

    public void NotificationAccepted() 
    {
        FacebookManager.LogNotificationAccepted("Yes"); 
    }
}
