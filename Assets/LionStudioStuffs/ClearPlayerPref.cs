﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClearPlayerPref : MonoBehaviour
{
    // Start is called before the first frame update

    public bool cleardata;
    void Start()
    {
        if (cleardata)
        {
            PlayerPrefs.DeleteAll();
        }

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
